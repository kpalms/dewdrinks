﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class VideoGallery : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectVideoGallary();
        }
    }

    private void SelectVideoGallary()
    {
        DataTable dt = CRUD.SelectVideoGallary();
        if(dt.Rows.Count > 0)
        {
            rptcourse.DataSource = dt;
            rptcourse.DataBind();
        }
      
       
    }
}