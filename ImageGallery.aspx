﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ImageGallery.aspx.cs" Inherits="ImageGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
.rowa {
  display: -webkit-box;
  display: -webkit-flex;
  display: -ms-flexbox;
  display:         flex;
  flex-wrap: wrap;
}
.cccc {
  display: flex;
  flex-direction: column;
}
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <h3 class="title_two" style="color: #056ee3">Image Gallery</h3>
            </div>
        </div>

        <div class="row rowa">
            <asp:Repeater ID="rptcourse" runat="server">
                <ItemTemplate>

                    <div class="col-sm-6 col-md-4 cccc">

                        <div class="thumbnail">
                            <div class="panel-heading text-uppercase text-center">
                              <b style="color:blue">  <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label></b>
                            </div>
                            
                            <img src="dd/Gallary/<%# Eval("ImageName") %>">
                            <div class="caption">
                                <p>
                                    <cite>
                                        <asp:Label ID="lblDescrption" runat="server" Text='<%# Eval("Descrption") %>'></asp:Label>
                                    </cite>
                                </p>

                            </div>

                        </div>




                    </div>

                </ItemTemplate>
            </asp:Repeater>
        </div>

    </div>
</asp:Content>

