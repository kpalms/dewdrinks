﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="AboutWater.aspx.cs" Inherits="AboutWater" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="courseArchive">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 col-sm-12">
                    <div class="courseArchive_content">
                        <div class="row">
                            <div class="col-lg-12 col-12 col-sm-12">
                                <div class="single_blog_archive wow fadeInUp">
                                    <br />
                                    <br />
                                    <h3 class="title_two" style="color: #056ee3">IT'S NOT BOTTLED...<br />
                                        IT'S BETTER!</h3>
                                    <h3 class="title_two" style="color: #056ee3">FIRST TIME IN PUNE PACKAGED DRINKING WATER DELIVERED TO YOUR DOORSTEP .</h3>
                                    <h4>Never guess what you are drinking again</h4>
                                    <p style="text-align: justify">
                                        <img src="img/dew.png" />Water is processed through 8 stages of rigorous purification which includes Micron filtration, Reverse Osmosis, Ozonation and UV treatment. 
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />Strict online process control system monitors quality at every critical step of the process through identify and aluminate contamination.

                                        <br />
                                        <br />
                                        <img src="img/dew.png" />Samples are also sent to external laboratory on regular basis to check the pesticide residue levels, toxic elements, and pathogenic bacteria.
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />Jars are washed through automatic washing and jars are checked regularly for the washing efficiency for microbiological parameters. Purified water is filled and capped in jars automatically.
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />Every jar is coded with batch number and manufacturing date through inkjet coding machine on the caps for easy traceability.
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />Professional service covering complete Pune delivering safe & quality drinking water
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />ALL 7 DAYS A WEEK
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />12 HOURS A DAY
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />NO DELIVERY CHARGES
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />ANY FLOOR TO YOUR DOORSTEP
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />DELEVERING ANYWHERE IN PUNE OTHER CITIES COMMING SOON....
                                        <br />
                                        <br />
                                        <img src="img/dew.png" />We offer home and offices,Retails,shops,Marriage hall,Parties etc water delivery services
                                        <br />
                                        <br />

                                        <h4 style="color: #056ee3">Quality </h4>
                                        <img src="img/dew.png" />We supply only standard approved products,ISI,BIS
                                        <br />
                                        <br />
                                        <h4 style="color: #056ee3">Misssion </h4>
                                        <img src="img/dew.png" />Our mission is to deliver SAFE water to our customers to their doorstep.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

