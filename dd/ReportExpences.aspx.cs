﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dd_ReportExpences : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txttodate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            txtfromdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }


    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        ReportExpences();
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        ReportExpences();
    }


    private void ReportExpences()
    {
        DataTable dt = CRUD.ReportExpences(txtfromdate.Text,txttodate.Text);
        if(dt.Rows.Count > 0)
        {
            GridView1.DataSource = dt;
            GridView1.DataBind();
            decimal sum = 0;
            for (int i = 0; i< dt.Rows.Count ;i++ )
            {
              sum=sum+ Convert.ToDecimal( dt.Rows[i][4]);
            }
            lbltotcan.Text = sum.ToString();
        }
       
    }
}