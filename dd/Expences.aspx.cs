﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.Services;
using System.Configuration;
using MySql.Data.MySqlClient;

public partial class dd_Expences : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            SelectExpences();
            
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
      
        
         int qty;
            if (txtqty.Text == "")
            {
                qty=0;
            }
            else
            {
                qty = Convert.ToInt32(txtqty.Text);
            }

        if (btnadd.Text == "Save")
        {
            CRUD.InsertExpences(txtdate.Text,txtItemName.Text,qty,Convert.ToDecimal(txtamount.Text),txtnote.Text);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification :  Expences Added Successfully !.');", true);
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateExpences(txtdate.Text, txtItemName.Text, qty, Convert.ToDecimal(txtamount.Text), txtnote.Text,id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification :  Expences Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtItemName.Text = "";
        txtqty.Text = "";
        txtamount.Text = "";
        txtnote.Text = "";
        txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        SelectExpences();
    }

    private void SelectExpences()
    {
        DataTable dt = CRUD.SelectExpences(txtdate.Text);
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }


    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtdate.Text = ((Label)e.Item.FindControl("lblDate")).Text;
            txtItemName.Text = ((Label)e.Item.FindControl("lblItemName")).Text;
            txtqty.Text = ((Label)e.Item.FindControl("lblQty")).Text;
            txtamount.Text = ((Label)e.Item.FindControl("lblAmount")).Text;
            txtnote.Text = ((Label)e.Item.FindControl("lblNotes")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeleteExpences(id);
            SelectExpences();
            btnadd.Text = "Save";
        }
    }
    protected void txtdate_TextChanged(object sender, EventArgs e)
    {
        SelectExpences();
    }
}