﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using MySql.Data.MySqlClient;
public partial class dd_PlaceOrder : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id,subid=0,mb=0;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            SelectPaymentMode();
            SelectItem();
            //lnksubcustmer.Enabled = false;
            lnksubcustmer.CssClass = "btn  btn-block btn-info";
            txtqty.Text = "1";
            txttime.Text = CRUD.ReturnIndianTime();
            txtdate.Text = CRUD.ReturnIndianDate();


            //DataTable dt = CRUD.selectCustomerName();
            //ddlcustomer.DataSource = dt;
            //ddlcustomer.DataTextField = "FullName";
            //ddlcustomer.DataValueField = "CustomerId";
            //ddlcustomer.DataBind();
            //ddlcustomer.Items.Insert(0, new ListItem("-- Select Customer --", "0"));
            //ddlcustomer.SelectedIndex = 0;



            if (Request.QueryString["Id"] != null)
            {
                DataTable dttt = CRUD.selectOrderById(Convert.ToInt32(Request.QueryString["Id"]));
                if (dttt.Rows.Count > 0)
                {
                    txtmobileno.Text = dttt.Rows[0]["MobileNo"].ToString();
                    SelectAddress();
                   // ddlcustomer.SelectedValue = dttt.Rows[0]["CustomerId"].ToString();
                    ddladdress.SelectedValue = dttt.Rows[0]["DeliveryId"].ToString();
                    txtdate.Text = dttt.Rows[0]["DeDate"].ToString();
                    txtnote.Text = dttt.Rows[0]["note"].ToString();
                    txttime.Text = dttt.Rows[0]["Time"].ToString();
                    txtrate.Text = dttt.Rows[0]["Rate"].ToString() + ".00";
                    txtqty.Text = dttt.Rows[0]["qty"].ToString();
                   ddlitem.SelectedValue = dttt.Rows[0]["ItemId"].ToString();
                    ddlpaymentmode.SelectedValue = dttt.Rows[0]["PaymentModeId"].ToString();
                    btnadd.Text = "UpdateOrder";
                }
               
            }

            if (Request.QueryString["SId"] != null)
            {

                txtmobileno.Text = Request.QueryString["MB"].ToString();
                mb = 1;
                SelectAddress();
                subid = Convert.ToInt32(Request.QueryString["SId"]);
                lnksubcustmer.Enabled = true;
            }
            else
            {
                subid=0;
                mb=0;
                
            }
        }
    }


    private void SelectPaymentMode()
    {
        DataTable dt = CRUD.SelectPaymentMode();
        ddlpaymentmode.DataSource = dt;
        ddlpaymentmode.DataTextField = "PaymentMode";
        ddlpaymentmode.DataValueField = "PayModeId";
        ddlpaymentmode.DataBind();
        ddlpaymentmode.Items.Insert(0, new ListItem("-- Select Payment Mode --", "00"));
        ddlpaymentmode.Items.Insert(1, new ListItem("Subscription", "0"));
     ddlpaymentmode.SelectedIndex = 0;
    }
    private void SelectItem()
    {
        DataTable dtt = CRUD.SelectItemName();
        ddlitem.DataSource = dtt;
        ddlitem.DataTextField = "ItemNames";
        ddlitem.DataValueField = "ItemId";
        ddlitem.DataBind();
        ddlitem.Items.Insert(0, new ListItem("-- Select Item --", "0"));
    ddlitem.SelectedIndex = 0;
    }
    protected void txtmobileno_TextChanged(object sender, EventArgs e)
    {
        if (txtmobileno.Text.Length == 10)
        {
            SelectAddress();
        }
        else
        {
            //Getddladdressmsg("Enter Mobile No 10 Digit");
            Getddladdressmsg("-- Select Address --");
        }

    }

    private void SelectAddress()
    {
        DataSet ds = CRUD.selectAddressByMb(txtmobileno.Text);

        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                id = Convert.ToInt32(ds.Tables[0].Rows[0]["CustomerId"].ToString());
            }

            if (ds.Tables[1].Rows.Count > 0)
            {
                ddladdress.DataSource = ds.Tables[1];
                ddladdress.DataTextField = "Address";
                ddladdress.DataValueField = "DeliveryId";
                ddladdress.DataBind();
                ddladdress.Items.Insert(0, new ListItem("-- Select Address --", "0"));
                ddladdress.SelectedIndex = 0;
            }
            else
            {

                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Address Not Found.');", true);
                Getddladdressmsg("Address Not Found");
            }
        }
        else
        {

            Getddladdressmsg("Customer Not Found");
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Customer Not Found.');", true);
            Response.Redirect("~/dd/Customer.aspx?mn="+txtmobileno.Text);


        }
    }
      
    protected void btnadd_Click(object sender, EventArgs e)
    {

            if (btnadd.Text == "PlaceOrder")
            {
                
                if (mb == 0)
                {
                   
                    CRUD.InsertPlaceOrder(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToDouble(txtrate.Text),txtdate.Text, txttime.Text, id, Convert.ToInt32(ddlpaymentmode.SelectedValue), "PlaceOrder", Convert.ToInt32(ddladdress.SelectedValue), txtnote.Text, subid);
                   
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Place Order Successfully .');", true);
                }
                else
                {
                    //added by kundan
                    CRUD.InsertPlaceOrder(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToDouble(txtrate.Text), txtdate.Text, txttime.Text, id, Convert.ToInt32(ddlpaymentmode.SelectedValue), "PlaceOrder", Convert.ToInt32(ddladdress.SelectedValue), txtnote.Text, subid);
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Place Order Successfully .');", true);
                    //end

                    //string RemaingCan = CRUD.ReturnOneValue("select ((select qty from Subscription where SubscriptionId='" + subid + "')-sum(od.Qty)) as RemaingCan  from OrderDetail od, Orders where od.SubscriptionId='" + subid + "' and od.OrderId = Orders.OrderId and Orders.Status in ('Delivered','PlaceOrder');");

                    //if (RemaingCan != "")
                    //{
                    //    if (Convert.ToInt32(txtqty.Text) <= Convert.ToInt32(RemaingCan))
                    //    {
                         
                    //        CRUD.InsertPlaceOrder(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToDouble(00.0), txtdate.Text, txttime.Text, id, Convert.ToInt32(ddlpaymentmode.SelectedValue), "PlaceOrder", Convert.ToInt32(ddladdress.SelectedValue), txtnote.Text, subid);
                    //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Place Order Successfully .');", true);
                    //    }
                    //    else
                    //    {
                    //        lblMessage.Visible = true;
                    //        lblMessage.Text = "Your Remaing Can is '" + RemaingCan + "'";
                    //        ScriptManager.RegisterStartupScript(btnadd, typeof(Button), "scr", "HideLabel();", true);
                    //    }
                    //}
                    //else
                    //{
                    //    CRUD.InsertPlaceOrder(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToDouble(00.0), txtdate.Text, txttime.Text, id, Convert.ToInt32(ddlpaymentmode.SelectedValue), "PlaceOrder", Convert.ToInt32(ddladdress.SelectedValue), txtnote.Text, subid);
                    //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Place Order Successfully .');", true);
                    //}
                }
            }
            else
            {
             
                CRUD.UpdatePlaceOrder(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToDouble(txtrate.Text), txtdate.Text, txttime.Text, id, Convert.ToInt32(ddlpaymentmode.SelectedValue), "PlaceOrder", Convert.ToInt32(ddladdress.SelectedValue), txtnote.Text, Convert.ToInt32(Request.QueryString["Id"]));
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Update Order Successfully .');", true);
            }

       
        if (mb == 0)
        {
            ddlitem.SelectedValue = "0";
            ddlpaymentmode.SelectedValue = "0";
            ddladdress.DataSource = "0";
            Getddladdressmsg("Select Mobile No");
            txtmobileno.Text = "";
            txtqty.Text = "1";
            txtrate.Text = "";
            txttotal.Text = "";
            txtnote.Text = "";
        }
       
        btnadd.Text = "PlaceOrder";
        txttime.Text = CRUD.ReturnIndianTime();
        txtdate.Text = CRUD.ReturnIndianDate();

       
    }
    protected void ddlitem_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtrate.Text = CRUD.ReturnOneValue("select Rate from ItemName where ItemId=" + Convert.ToInt32(ddlitem.SelectedValue) + "");
    }

    private void Getddladdressmsg(string msg)
    {
        List<ListItem> items = new List<ListItem>();
        items.Add(new ListItem("--" + msg + "--", "0"));
        ddladdress.DataSource = items;
        ddladdress.DataTextField = "Text";
        ddladdress.DataValueField = "Value";
        ddladdress.DataBind();
        ddladdress.SelectedIndex = 0;
    }

    //protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlcustomer.SelectedValue !="0")
    //    {

    //      string mobile =  CRUD.ReturnOneValue("select MobileNo from Customer where CustomerId="+ Convert.ToInt32(ddlcustomer.SelectedValue));

    //      if (mobile.Length == 10)
    //    {
    //        txtmobileno.Text = mobile;
    //        SelectAddress();
    //    }
    //    else
    //    {
           
    //        Getddladdressmsg("-- Select Address --");
    //    }
    //    }
    //}
    protected void txtcustomer_TextChanged(object sender, EventArgs e)
    {
        if (txtcustomer.Text != "")
        {
            string dir = txtcustomer.Text;
            string[] parts = dir.Split('@');
            txtmobileno.Text = parts[1];
            SelectAddress();

        }
        else
        {

            Getddladdressmsg("-- Select Address --");
        }
    }



}