﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CustomerAddress.aspx.cs" Inherits="dd_CustomerAddress" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div class="container">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-center text-uppercase ">
                                Customer Address Book
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <asp:GridView ID="dvaddress"  EmptyDataText="No records Found" AutoGenerateColumns="False" class="table table-striped table-bordered table-hover" runat="server" OnRowCommand="dvaddress_RowCommand">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Sr.No">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Company" HeaderText="Company" />
                                                <asp:BoundField DataField="Appartment" HeaderText="Appartment" />
                                                <asp:BoundField DataField="Flatno" HeaderText="Flatno" />
                                                <asp:BoundField DataField="Address" HeaderText="Address" />
                                                <asp:BoundField DataField="AreaName" HeaderText="AreaName" />
                                                <asp:BoundField DataField="CityName" HeaderText="CityName" />
                                                <asp:TemplateField HeaderText="Create">
                                                    <ItemTemplate>
                                                        <a href="Address.aspx?Id=<%#Eval("CustomerId") %>&A=C" class="btn btn-primary btn-xs">Create</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Edit">
                                                    <ItemTemplate>
                                                        <a href="Address.aspx?Id=<%#Eval("CustomerId") %>&A=E" class="btn btn-info btn-xs">Edit</a>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" runat="server" CommandArgument='<%#Eval("DeliveryId") %>' class="btn btn-danger btn-xs" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
