﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dd_CustomerAddress : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectAddress();
        }
    }
    protected void dvaddress_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        CRUD.DeleteAddress(0,Convert.ToInt32(e.CommandArgument.ToString()));
        SelectAddress();
    }

    public void SelectAddress()
    {
        DataTable dtt = CRUD.SelectAddress(Convert.ToInt32(Request.QueryString["Id"]));
        dvaddress.DataSource = dtt;
        dvaddress.DataBind();
    }
}