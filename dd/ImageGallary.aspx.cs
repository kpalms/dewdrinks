﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
public partial class dd_ImageGallary : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    static string str;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            btnupdate.Enabled = false;
            btnupdate.CssClass = "btn  btn-block btn-primary";
            SelectImageGallary();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {

        string str = CRUD.ReturnOneValue("select Title from ImageGallary where Title='" + txttitle.Text + "'");
            if (str != txttitle.Text)
            {
            if (fileimage.HasFile)
            {
                string ext = Path.GetExtension(fileimage.FileName);
                string filename = CRUD.GetImageName() + "" + ext;
                fileimage.PostedFile.SaveAs(Server.MapPath("Gallary/") + filename);
                str = filename;
            }
            else
            {
                str = "NoImage.jpg";
            }
            CRUD.InsertImageGallary(txttitle.Text, txtdescripation.Text, str);
             ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Image Gallary Added Successfully !.');", true);

        }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Image Gallary is Already Exists !.');", true);
            }
       
       // btnadd.Text = "Save";
        txttitle.Text = "";
        txtdescripation.Text = "";
        SelectImageGallary();
    }


    protected void btnupdate_Click(object sender, EventArgs e)
    {
        if (fileimage.HasFile)
        {
            string ext = Path.GetExtension(fileimage.FileName);
            string filename = CRUD.GetImageName() + "" + ext;
            fileimage.PostedFile.SaveAs(Server.MapPath("Gallary/") + filename);
            str = filename;
        }
        CRUD.UpdateImageGallary(txttitle.Text, txtdescripation.Text, str, id);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Image Gallary Updated Successfully !.');", true);

        txttitle.Text = "";
        txtdescripation.Text = "";
        SelectImageGallary();

        btnadd.Enabled = true;
        btnadd.CssClass = "btn  btn-block btn-primary";

        btnupdate.Enabled = false;
        btnupdate.CssClass = "btn  btn-block btn-primary";
    }



    private void SelectImageGallary()
    {
        DataTable dt = CRUD.SelectImageGallary();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        str = ((Label)e.Item.FindControl("lblimagename")).Text;
        if (e.CommandName == "Edit")
        {
            btnadd.Enabled = false;
            btnadd.CssClass = "btn  btn-block btn-primary";

            btnupdate.Enabled = true;
            btnupdate.CssClass = "btn  btn-block btn-primary";
           // btnadd.Text = "Update";
            txttitle.Text = ((Label)e.Item.FindControl("lblTitle")).Text;
            //txturl.Text = ((Label)e.Item.FindControl("lblURL")).Text;
            txtdescripation.Text = ((Label)e.Item.FindControl("lblDescrption")).Text;
        }
        if (e.CommandName == "Delete")
        {
          
            CRUD.DeleteImageGallary(id);
            string imagefilepath = Server.MapPath("Gallary/" + str);
            File.Delete(imagefilepath);
            SelectImageGallary();
            btnadd.Text = "Save";
        }
    }
}