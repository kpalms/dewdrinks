﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="EditCustomer.aspx.cs" Inherits="dd_EditCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <a href="CustomerList.aspx" class="btn  btn-sm btn-primary" title="Back">Back</a>
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Update Customer
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtFirstName" placeholder="Enter First Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtLastName" placeholder="Enter Last Name" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno" onkeypress="return isNumber(event)" MaxLength="10" placeholder="Enter Mobile Number" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtemailid" placeholder="Enter Email Id" TextMode="Email" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:RadioButtonList ID="rdgender" CssClass="RadioButtonWidth" runat="server" RepeatColumns="2">
                                        <asp:ListItem Value="0">Male</asp:ListItem>
                                        <asp:ListItem Value="1">Female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txthindt" placeholder="Enter Hint" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtmobileno" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="ReqiredFieldValidator1" runat="server" ControlToValidate="rdgender" ForeColor="Red" ErrorMessage="Select Gender!" ValidationGroup="g1"> </asp:RequiredFieldValidator>

                        </div>

                        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
                            <div class="form-group">
                                <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" OnClick="btnadd_Click" Text="Update Customer" />

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
            
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

