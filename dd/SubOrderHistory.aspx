﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="SubOrderHistory.aspx.cs" Inherits="dd_SubOrderHistory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Subscripation Order History
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4">
                                <center><h4> <asp:RequiredFieldValidator ID="rfv1" ValidationGroup="g1" runat="server" ControlToValidate="ddlcustomer" InitialValue="0" ForeColor="Red" ErrorMessage="please select Customer" /></h4></center>
                            </div>
                            <div class="col-lg-4">
                                <center><h4> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="g1" runat="server" ControlToValidate="ddlplan" InitialValue="0" ForeColor="Red" ErrorMessage="please select Plan" /></h4></center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlcustomer" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlcustomer_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-lg-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlplan" class="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>

                                </div>
                            </div>
                           
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" Text="Load" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                            <asp:BoundField DataField="DeDate" HeaderText="Delivery Date" />
                                            <asp:BoundField DataField="Time" HeaderText="Time" />
                                             <asp:TemplateField HeaderText="DeliveredCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeliveredCan" runat="server" Text=' <%#Eval("Qty")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CollectedCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCollectedCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Status" HeaderText="Status" />
                                             <asp:BoundField DataField="note" HeaderText="PlaceOrderNote" />
                                             <asp:BoundField DataField="DeliveryNote" HeaderText="DeliveryNote" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 alert alert-success">
                                <b>Total Delivered Can :
                                    <asp:Label ID="lbltotdelivedcan" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-4 alert alert-info">
                                <b>Total Collected Can :
                                    <asp:Label ID="lbltotcollectedcan" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-4 alert alert-warning">
                                <b>Total Pending Can :
                                    <asp:Label ID="lbltotpendingcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


