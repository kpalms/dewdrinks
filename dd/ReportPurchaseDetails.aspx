﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReportPurchaseDetails.aspx.cs" Inherits="dd_ReportPurchaseDetails" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
           <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div class="container">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-center text-uppercase ">
                                Item Details
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="dvaddress" EmptyDataText="No records Found" AutoGenerateColumns="False" class="table table-striped table-bordered table-hover" runat="server">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="ItemNames" HeaderText="ItemNames" />
                                            <asp:BoundField DataField="UnitName" HeaderText="UnitName" />
                                            <asp:BoundField DataField="Qty" HeaderText="Qty" />
                                            <asp:BoundField DataField="Rate" HeaderText="Rate" />
                                            <asp:BoundField DataField="Total" HeaderText="Total" />
                                            <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
