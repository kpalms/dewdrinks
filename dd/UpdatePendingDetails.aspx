﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdatePendingDetails.aspx.cs" Inherits="dd_UpdatePendingDetails" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
     <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script src="bower_components/JavaScript.js" type="text/javascript"></script>
     <script type="text/javascript">
         function refreshAndClose() {
             window.opener.location.reload(true);
             window.close();
         }
</script>

</head>
<body>
    <form id="form1" runat="server">
   <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div class="container-fluid">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-center text-uppercase ">
                               Insert Update Pending Details
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        Enter Collected Can :
                                        <asp:TextBox ID="txtremaingincan" onkeypress="return isNumber(event)"  placeholder="Enter Collected Can" class="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        Enter Collected Amount :
                                        <asp:TextBox ID="txtremaingamt" onkeypress="return isNumber(event)"  placeholder="Enter Pending Amount" class="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                                  <div class="col-lg-3">
                                    <div class="form-group">
                                        Select Date:
                                        <asp:TextBox ID="txtdate" onkeypress="return isNumber(event)"  placeholder="Select Date" class="form-control" runat="server" required></asp:TextBox>
                                        <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txtdate">
                                    </ajaxToolkit:CalendarExtender>
                                    </div>
                                </div>
                                <div class="col-lg-3">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Confirm" OnClick="btnadd_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
