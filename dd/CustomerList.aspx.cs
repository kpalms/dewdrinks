﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_CustomerList : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectCustomer();
        }
    }

    private void SelectCustomer()
    {
        DataTable dt = CRUD.SelectCustomer(1);
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        SelectCustomer();
    }
   
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            CRUD.ActiveCustomer(Convert.ToInt32(e.CommandArgument.ToString()), 0);
            SelectCustomer();
        }
    }

    protected void lnksearmb_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.SelectCustomerByMb(txtmobile.Text);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtmobile.Text = "";
    }
    protected void lnksearcust_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.SelectCustomerByFirstName(txtcustname.Text);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtcustname.Text = "";
    }

    protected void lnklastname_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.SelectCustomerByLastName(txtlastname.Text);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtlastname.Text = "";
    }

    protected void btndefault_Click(object sender, EventArgs e)
    {
        SelectCustomer();
    }
}