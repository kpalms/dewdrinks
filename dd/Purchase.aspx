﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="Purchase.aspx.cs" Inherits="dd_Purchase" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function calculate() {
            var myBox1 = document.getElementById("<%=txtqty.ClientID %>").value;
            var myBox2 = document.getElementById("<%=txtrate.ClientID %>").value;
            if (myBox1 != null && myBox2 != null) {
                document.getElementById("<%=txttot.ClientID %>").value = myBox1 * myBox2;
            }
        }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Purchase
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="rfv1" ValidationGroup="g2" runat="server" ControlToValidate="ddlsupplier" InitialValue="0" ForeColor="Red" ErrorMessage="please select supplier"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="g2" runat="server" ControlToValidate="txtInvoiceNo"  ForeColor="Red" ErrorMessage="please Enter Invocie No"></asp:RequiredFieldValidator>

                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="g1" runat="server" ControlToValidate="ddlitem" InitialValue="0" ForeColor="Red" ErrorMessage="please select Item"></asp:RequiredFieldValidator>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="g1" runat="server" ControlToValidate="ddlunit" InitialValue="0" ForeColor="Red" ErrorMessage="please select Unit"></asp:RequiredFieldValidator>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <asp:CompareValidator ID="CompareValidator1" runat="server"
                                    ControlToValidate="txtqty"
                                    Operator="GreaterThanEqual"
                                    ValueToCompare="1"
                                    Type="Integer"
                                    ForeColor="Red"
                                    ValidationGroup="g1"
                                    ErrorMessage="The integer cannot be less than zero"
                                    Display="Dynamic"
                                    Text="The integer cannot be less than one" />

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtdate" placeholder="Select Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txtdate"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtInvoiceNo" onkeypress="return isNumber(event)" placeholder="Enter Invoice No" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlsupplier" class="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlitem" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlitem_SelectedIndexChanged"></asp:DropDownList>

                                </div>
                            </div>

                             <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlunit" class="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtqty" onkeypress="return isNumber(event)" onchange="calculate()" placeholder="Enter Quantity" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                           

                        </div>


                        <div class="row">
                              <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtrate" onkeypress="return isNumber(event)" onchange="calculate()" placeholder="Enter Rate" class="form-control" runat="server" required></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txttot" ReadOnly="true" onkeypress="return isNumber(event)" placeholder="Enter Total" class="form-control" runat="server" required></asp:TextBox>

                                </div>
                            </div>
                             <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtnotes" placeholder="Enter Notes" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" Text="ADD" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>ItemNames</th>
                                                     <th>UnitName</th>
                                                    <th>Quantity</th>
                                                   
                                                    <th>Rate</th>
                                                    <th>Total</th>
                                                    <th>Notes</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td>
                                                        <asp:Label ID="lblItemNames" runat="server" Text='<%# Eval("ItemNames") %>'></asp:Label>
                                                        <asp:Label ID="lblItemId" Visible="false" runat="server" Text='<%# Eval("ItemId") %>'></asp:Label>
                                                    </td>
                                                  
                                                    <td>
                                                        <asp:Label ID="lblUnitName" runat="server" Text='<%# Eval("UnitName") %>'></asp:Label>
                                                        <asp:Label ID="lblUnitId" Visible="false" runat="server" Text='<%# Eval("UnitId") %>'></asp:Label>
                                                    </td>
                                                      <td>
                                                        <asp:Label ID="lblQuantity" runat="server" Text='<%# Eval("Qty") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRate" runat="server" Text='<%# Eval("Rate") %>'></asp:Label>
                                                    </td>
                                                     <td>
                                                        <asp:Label ID="lblTotal" runat="server" Text='<%# Eval("Total") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("PurchaseDetailsId") %>' CssClass="btn btn-sm btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("PurchaseDetailsId") %>' CssClass="btn btn-sm btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr id="trEmpty" runat="server" visible="false">
                                                    <td colspan="9" align="center">No records found.
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:LinkButton ID="btnsave" runat="server" ValidationGroup="g2" class="btn  btn-block btn-primary" Text="CompleteOrder" OnClick="btnsave_Click"></asp:LinkButton>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Date</th>
                                                    <th>InvoiceNo</th>
                                                    <th>Supplier Name & Total Amount</th>

                                                     <th>Edit</th>
                                               <%-- <th>Delete</th>--%>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                                        <asp:Label ID="lblSupplierId" Visible="false" runat="server" Text='<%# Eval("SupplierId") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblInVoiceNo" runat="server" Text='<%# Eval("InVoiceNo") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFullName" runat="server" Text='<%# Eval("FullName") %>'></asp:Label> &
                                                         <asp:Label ID="lblTotalAmount" runat="server" Text='<%# Eval("TotalAmount") %>'></asp:Label>
                                                    </td>

                                                    <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("PurchaseId") %>' CssClass="btn btn-sm btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                </td>
                                                <%-- <td>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("PurchaseId") %>' CssClass="btn btn-sm btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                </td>--%>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr id="trEmpty" runat="server" visible="false">
                                                    <td colspan="5" align="center">No records found.
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

