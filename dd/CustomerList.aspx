﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="CustomerList.aspx.cs" Inherits="dd_CustomerList" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Customer List
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtmobile" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtmobile" ForeColor="Red" ValidationGroup="g1" runat="server" ErrorMessage="please enter mobiel no"></asp:RequiredFieldValidator>

                            </div>
                             <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtcustname" ForeColor="Red" ValidationGroup="g2" runat="server" ErrorMessage="please enter First name"></asp:RequiredFieldValidator>

                            </div>
                               <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtlastname" ForeColor="Red" ValidationGroup="g3" runat="server" ErrorMessage="please enter Last Name"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtmobile" placeholder="Enter Mobile No" onkeypress="return isNumber(event)" MaxLength="10" class="form-control" runat="server" required></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:LinkButton ID="lnksearmb" class="btn btn-primary" ValidationGroup="g1" runat="server" OnClick="lnksearmb_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                    </span>
                                </div>
                                <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetMobileNo" MinimumPrefixLength="1"
                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtmobile"
                                    ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                           <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtcustname" placeholder="Enter First Name" class="form-control" runat="server" required></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:LinkButton ID="lnksearcust" class="btn btn-primary" ValidationGroup="g2" runat="server" OnClick="lnksearcust_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                    </span>
                                </div>
                                <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetFirstName" MinimumPrefixLength="1"
                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtcustname"
                                    ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>

                             <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtlastname" placeholder="Enter Last Name" class="form-control" runat="server" required></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:LinkButton ID="lnklastname" class="btn btn-primary" ValidationGroup="g3" runat="server" OnClick="lnklastname_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                    </span>
                                </div>
                                <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetLastName" MinimumPrefixLength="1"
                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtlastname"
                                    ID="AutoCompleteExtender3" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                           <div class="col-lg-3 col-md-3">
                                <asp:LinkButton ID="btndefault" class="btn btn-primary btn-block" runat="server" OnClick="btndefault_Click">See All Records</asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="100" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FirstName" HeaderText="FirstName" />
                                            <asp:BoundField DataField="LastName" HeaderText="LastName" />
                                            <asp:BoundField DataField="Gender" HeaderText="Gender" />
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                            <asp:BoundField DataField="Email" HeaderText="Email" />
                                            <asp:BoundField DataField="Hint" HeaderText="Hint" />
                                            <asp:TemplateField HeaderText="View Address">
                                                <ItemTemplate>
                                                    <a href="#" onclick="popup =window.open('CustomerAddress.aspx?Id=<%#Eval("CustomerId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false"  class="btn btn-primary btn-xs">View Address</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Edit">
                                                <ItemTemplate>
                                                    <a href='EditCustomer.aspx?Id=<%#Eval("CustomerId") %>' class="btn btn-info btn-xs">Edit</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Delete">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkdelete" CommandName="DeleteRow" runat="server" CommandArgument='<%#Eval("CustomerId") %>' class="btn btn-danger btn-xs" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


