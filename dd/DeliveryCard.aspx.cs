﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_DeliveryCard : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id=0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            selectDeliveryCard();
        }
    }

    public void selectDeliveryCard()
    {
        DataSet dt = CRUD.selectDeliveryCard("PlaceOrder",CRUD.ReturnIndianDate());
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();
            }

            //if (dt.Tables[1].Rows.Count > 0)
           // {
               // lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
              //  lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
            //}

        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectDeliveryCard();
    }
    
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);
           DataTable dt= CRUD.selectPeningCanAmt( Convert.ToInt32(lblcustomerid.Text));
         lblPendingAmt.Text=dt.Rows[0]["PendingAmt"].ToString();
         lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();

         Label lbltime = (e.Row.FindControl("lbltime") as Label);
         Label lbltimeago = (e.Row.FindControl("lbltimeago") as Label);
         DateTime StartTime = DateTime.Parse(lbltime.Text);
         DateTime EndTime = DateTime.Parse( DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("hh:mm tt"));
     
         TimeSpan ts = EndTime - StartTime;
         lbltimeago.Text = ts.ToString();
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
        CRUD.ChangeStatus(Convert.ToInt32(e.CommandArgument.ToString()), "Cancel");
        selectDeliveryCard();
        }
    }


    protected void lnksearmb_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectDeliveryCardByMb("PlaceOrder", txtmobile.Text, CRUD.ReturnIndianDate());
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtmobile.Text = "";
    }
    protected void lnksearcust_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectDeliveryCardByFName("PlaceOrder", txtcustname.Text, CRUD.ReturnIndianDate());
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtcustname.Text = "";
    }

    protected void lnklastname_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectDeliveryCardByLName("PlaceOrder", txtlastname.Text, CRUD.ReturnIndianDate());
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtlastname.Text = "";
    }

    protected void btndefault_Click(object sender, EventArgs e)
    {
        selectDeliveryCard();
    }
}