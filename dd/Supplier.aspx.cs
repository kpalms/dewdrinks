﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Supplier : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectSupplier();
        }
    }


    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select FullName from supplier where FullName='" + txtfullname.Text + "'");
            if (str != txtfullname.Text)
            {
            CRUD.InsertSupplier(txtfullname.Text, txtcontactperson.Text, txtemail.Text, txtmobileno1.Text, txtmobileno2.Text, txtcity.Text, txtnote.Text, txtaddress.Text);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Supplier Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : supplier  is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateSupplier(txtfullname.Text, txtcontactperson.Text, txtemail.Text, txtmobileno1.Text, txtmobileno2.Text, txtcity.Text, txtnote.Text, txtaddress.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Supplier Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        
        txtfullname.Text = "";
        txtcity.Text = "";
        txtaddress.Text = "";
        txtcontactperson.Text = "";
        txtemail.Text = "";
        txtmobileno1.Text = "";
        txtmobileno2.Text = "";
        txtnote.Text = "";
        SelectSupplier();
    }

    private void SelectSupplier()
    {
        DataTable dt = CRUD.SelectSupplier();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtfullname.Text = ((Label)e.Item.FindControl("lblFullName")).Text;
            txtcontactperson.Text = ((Label)e.Item.FindControl("lblContactPerson")).Text;
            txtemail.Text = ((Label)e.Item.FindControl("lblEmailId")).Text;
            txtmobileno1.Text = ((Label)e.Item.FindControl("lblMobileNo1")).Text;
            txtmobileno2.Text = ((Label)e.Item.FindControl("lblMobileNo2")).Text;
            txtcity.Text = ((Label)e.Item.FindControl("lblCity")).Text;
            txtnote.Text = ((Label)e.Item.FindControl("lblNote")).Text;
            txtaddress.Text = ((Label)e.Item.FindControl("lblAddress")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeletSupplier(0,id);
            SelectSupplier();
            btnadd.Text = "Save";
        }
    }
}