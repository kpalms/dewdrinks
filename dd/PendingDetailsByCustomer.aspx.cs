﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using MySql.Data.MySqlClient;
public partial class dd_PendingDetailsByCustomer : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    decimal TotalPendingAmount = 0;
    int TotalPendingCan = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
           
            DataTable dt = CRUD.selectCustomerName();
            ddlcustomer.DataSource = dt;
            ddlcustomer.DataTextField = "FullName";
            ddlcustomer.DataValueField = "CustomerId";
            ddlcustomer.DataBind();
            ddlcustomer.Items.Insert(0, new ListItem("-- Select Customer --", "0"));
            ddlcustomer.SelectedIndex = 0;
            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            selectPendingDetailsByCustomer();
        }
    }

    private void selectPendingDetailsByCustomer()
    {
        int Id;
        if(Convert.ToInt32(Request.QueryString["Id"])==0)
        {
            Id = Convert.ToInt32(ddlcustomer.SelectedValue);
        }
        else
        {
            Id = Convert.ToInt32(Request.QueryString["Id"]);
        }
        if (Id !=0)
        {
        DataSet dt = CRUD.selectPendingDetailsByCustomer(Id);
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();


                lbltotcan.Text = "0";
                lbltotamt.Text = "0.00";
                GridView1.Columns.Clear();
            }
            //if (dt.Tables[1].Rows.Count > 0)
            //{
            //    lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
            //    lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
            //}
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
          
            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            GridView1.Columns.Clear();
        }
        }

    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectPendingDetailsByCustomer();
    }

   
    protected void btnadd_Click(object sender, EventArgs e)
    {
        selectPendingDetailsByCustomer();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);
            //DataTable dt = CRUD.selectPeningCanAmtByOderId(Convert.ToInt32(lblcustomerid.Text));
            //lblPendingAmt.Text = dt.Rows[0]["PendingAmt"].ToString();
            //lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();

            TotalPendingCan += Convert.ToInt32(lblPendingCan.Text);
            TotalPendingAmount += Convert.ToDecimal(lblPendingAmt.Text);

            lbltotcan.Text = TotalPendingCan.ToString();
            lbltotamt.Text = TotalPendingAmount.ToString();
        }
    }
}