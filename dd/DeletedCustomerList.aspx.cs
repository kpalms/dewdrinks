﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_DeletedCustomerList : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectCustomer();
        }
    }

    private void SelectCustomer()
    {
        DataTable dt = CRUD.SelectCustomer(0);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        
    }
 
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        SelectCustomer();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            CRUD.ActiveCustomer(Convert.ToInt32(e.CommandArgument.ToString()), 1);
            SelectCustomer();
        }
    }
}