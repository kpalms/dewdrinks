﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Purchase : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id,pid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
            DataTable dt = CRUD.SelectSupplierName();
            ddlsupplier.DataSource = dt;
            ddlsupplier.DataTextField = "FullName";
            ddlsupplier.DataValueField = "SupplierId";
            ddlsupplier.DataBind();
            ddlsupplier.Items.Insert(0, new ListItem("-- Select Supplier --", "0"));
            ddlsupplier.SelectedIndex = 0;

            DataTable dtt = CRUD.SelectPItemName();
            ddlitem.DataSource = dtt;
            ddlitem.DataTextField = "ItemNames";
            ddlitem.DataValueField = "ItemId";
            ddlitem.DataBind();
            ddlitem.Items.Insert(0, new ListItem("-- Select Item Names --", "0"));
            ddlitem.SelectedIndex = 0;

            DataTable dttt = CRUD.SelectUnit();
            ddlunit.DataSource = dttt;
            ddlunit.DataTextField = "UnitName";
            ddlunit.DataValueField = "UnitId";
            ddlunit.DataBind();
            ddlunit.Items.Insert(0, new ListItem("-- Select Unit Names --", "0"));
            ddlunit.SelectedIndex = 0;
            SelectPurchaseDetails();
            SelectPurchase();
        }
    }

    protected void ddlitem_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtrate.Text = CRUD.ReturnOneValue("select Rate from pitem where ItemId='" + Convert.ToInt32(ddlitem.SelectedValue) + "'");
    }

   
    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "ADD")
        {
            string str = CRUD.ReturnOneValue("select ItemId from purchasedetails where ItemId='" + Convert.ToInt32(ddlitem.SelectedValue) + "' and Status=1");
            if (str=="")
            {
                CRUD.InsertPurchaseDetails(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToInt32(ddlunit.SelectedValue), Convert.ToDecimal(txtrate.Text), txtnotes.Text);
               // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Item Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Item is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdatePurchaseDetails(Convert.ToInt32(ddlitem.SelectedValue), Convert.ToInt32(txtqty.Text), Convert.ToInt32(ddlunit.SelectedValue), Convert.ToDecimal(txtrate.Text), txtnotes.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Item Updated Successfully !.');", true);
        }

        txtrate.Text = "";
        txtqty.Text = "";
        txtnotes.Text = "";
        ddlitem.SelectedValue = "0";
        ddlunit.SelectedValue = "0";
        btnadd.Text = "ADD";
        SelectPurchaseDetails();
    }

    private void SelectPurchaseDetails()
    {
        DataTable dt = CRUD.SelectPurchaseDetails();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }


    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtnotes.Text = ((Label)e.Item.FindControl("lblNotes")).Text;
            txtrate.Text = ((Label)e.Item.FindControl("lblRate")).Text;
            ddlunit.SelectedValue = ((Label)e.Item.FindControl("lblUnitId")).Text;
            txtqty.Text = ((Label)e.Item.FindControl("lblQuantity")).Text;
            ddlitem.SelectedValue = ((Label)e.Item.FindControl("lblItemId")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeletPurchaseDetails(id);
            SelectPurchaseDetails();
            btnadd.Text = "ADD";
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        if (btnsave.Text == "CompleteOrder")
        {
            CRUD.InsertPurchase(txtdate.Text, Convert.ToInt32(txtInvoiceNo.Text), Convert.ToInt32(ddlsupplier.SelectedValue));
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Order Completed Successfully !.');", true);
        }

        if (btnsave.Text == "UpdateOrder")
        {
            CRUD.UpdatePurchase(txtdate.Text, Convert.ToInt32(txtInvoiceNo.Text), Convert.ToInt32(ddlsupplier.SelectedValue), pid);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : update Order Completed Successfully !.');", true);
        }
        
        txtInvoiceNo.Text = "";
        btnsave.Text = "CompleteOrder";
        ddlsupplier.SelectedValue = "0";
        txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        SelectPurchase();
        SelectPurchaseDetails();
    }

    private void SelectPurchase()
    {
        DataTable dt = CRUD.SelectPurchase();
        Repeater1.DataSource = dt;
        Repeater1.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = Repeater1.Controls[Repeater1.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void Repeater1_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        pid = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnsave.Text = "UpdateOrder";
            txtdate.Text = ((Label)e.Item.FindControl("lblDate")).Text;
            txtInvoiceNo.Text = ((Label)e.Item.FindControl("lblInVoiceNo")).Text;
            ddlsupplier.SelectedValue = ((Label)e.Item.FindControl("lblSupplierId")).Text;
            CRUD.EditPurchase(pid);
            SelectPurchase();
            SelectPurchaseDetails();
        }
    }
}