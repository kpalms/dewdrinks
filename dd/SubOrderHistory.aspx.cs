﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_SubOrderHistory : System.Web.UI.Page
{
    int TotalDeliveredCan = 0, TotalCollectedCan = 0;
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.selectSubscriptionCustomer();
            ddlcustomer.DataSource = dt;
            ddlcustomer.DataTextField = "FullName";
            ddlcustomer.DataValueField = "CustomerId";
            ddlcustomer.DataBind();
            ddlcustomer.Items.Insert(0, new ListItem("-- Select Customer --", "0"));
            ddlcustomer.SelectedIndex = 0;

            lbltotcollectedcan.Text = "0";
            lbltotdelivedcan.Text = "0";
            lbltotpendingcan.Text = "0";
        }
    }

    private void selectSubOrderHistory()
    {
        DataSet dt = CRUD.selectSubOrderHistory(Convert.ToInt32(ddlcustomer.SelectedValue), Convert.ToInt32(ddlplan.SelectedValue));
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
                GridView1.Columns.Clear();
                lbltotcollectedcan.Text = "0";
                lbltotdelivedcan.Text = "0";
                lbltotpendingcan.Text = "0";
            }
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.Columns.Clear();
            lbltotcollectedcan.Text = "0";
            lbltotdelivedcan.Text = "0";
            lbltotpendingcan.Text = "0";
        }
        //if (dt.Tables[1].Rows.Count > 0)
        //{
        //    lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
        //    lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
        //}
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        selectSubOrderHistory();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectSubOrderHistory();
    }
    protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectSubscriptionByCustomer(Convert.ToInt32(ddlcustomer.SelectedValue));
        ddlplan.DataSource = dt;
        ddlplan.DataTextField = "qty";
        ddlplan.DataValueField = "SubscriptionId";
        ddlplan.DataBind();
        ddlplan.Items.Insert(0, new ListItem("-- Select Subscription --", "0"));
        ddlplan.SelectedIndex = 0;
    }


    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblDeliveredCan = (e.Row.FindControl("lblDeliveredCan") as Label);
            Label lblCollectedCan = (e.Row.FindControl("lblCollectedCan") as Label);

            TotalDeliveredCan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "Qty"));
            TotalCollectedCan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CollectedCan"));

            lbltotcollectedcan.Text = TotalCollectedCan.ToString();
            lbltotdelivedcan.Text = TotalDeliveredCan.ToString();
            lbltotpendingcan.Text = (TotalDeliveredCan - TotalCollectedCan).ToString();
           
        }
    }
}