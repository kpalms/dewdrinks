﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="WorkerDeliverOrders.aspx.cs" Inherits="dd_WorkerDeliverOrders" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript">
        function Validate(sender, args) {
            var gridView = document.getElementById("<%=GridView1.ClientID %>");
            var checkBoxes = gridView.getElementsByTagName("input");
            for (var i = 0; i < checkBoxes.length; i++) {
                if (checkBoxes[i].type == "checkbox" && checkBoxes[i].checked) {
                    args.IsValid = true;
                    return;
                }
            }
            args.IsValid = false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Order Assign to Worker
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <asp:CustomValidator ID="CustomValidator1" runat="server" ValidationGroup="g1" ErrorMessage="Please select at least one record."
                                ClientValidationFunction="Validate" ForeColor="Red"></asp:CustomValidator>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <center> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="g1" runat="server" ControlToValidate="ddlworker" InitialValue="0" ForeColor="Red" ErrorMessage="please select Worker Name" />
              </center>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlworker" class="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" Text="Assign Orders" OnClick="btnadd_Click" />

                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <asp:LinkButton ID="btndefault" class="btn btn-info btn-block" runat="server" OnClick="btndefault_Click">Refresh For New Orders</asp:LinkButton>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="OrderId" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblOrderId" runat="server" Text=' <%#Eval("OrderId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                            <asp:TemplateField HeaderText="Qty & ItemName">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblqty" runat="server" Text=' <%#Eval("qty")%>'></asp:Label></b>
                                                    <asp:Label ID="lblItemNames" runat="server" Text=' <%#Eval("ItemNames")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hint & Address">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                    <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                            <asp:TemplateField HeaderText="Delivery DateTime" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                    <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time Ago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltimeago" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="note" HeaderText="PlaceOrderNote" />
                                            <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" />
                                            <asp:TemplateField HeaderText="TotalAmount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalAmount" runat="server" Text=' <%#Eval("TotalAmount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PendingAmt">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="PendingCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Select Data">
                                                <EditItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </EditItemTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <div class="dropdown">
                                                        <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                                            Action
  <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href='PlaceOrder.aspx?Id=<%#Eval("OrderId") %>'>OEdit</a></li>
                                                             <li><a href='EditCustomer.aspx?Id=<%#Eval("CustomerId") %>'>CEdit</a></li>
                                                            <li>
                                                                <asp:LinkButton ID="lnkordercomp" CommandName="DeleteRow" runat="server" OnClientClick="return confirm('are you sure want to Cancel Order?');" CommandArgument='<%#Eval("OrderId") %>'>Cancel</asp:LinkButton>
                                                            </li>

                                                            <li>
                                                            <asp:LinkButton ID="lnkDelete" CommandName="DeleteOrder" runat="server" OnClientClick="return confirm('are you sure want to Delete Order?');" CommandArgument='<%#Eval("OrderId") %>'>Delete</asp:LinkButton>
                                                            </li>



                                                        </ul>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <%-- <asp:TemplateField>
                                                  <ItemTemplate>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("OrderId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm">Assign</asp:LinkButton>
                                          </ItemTemplate>
                                                        </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                             <div class="col-lg-3 alert alert-success">
                                <b>Total Can :
                                    <asp:Label ID="lbltotalcan" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-3 alert alert-info">
                                <b>Total Amount:
                                    <asp:Label ID="lbltotalamt" runat="server" Text="Label"></asp:Label></b>
                            </div>
                            <div class="col-lg-3 alert alert-danger">
                                <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-3 alert alert-warning">
                                <b>Total Pending Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

