﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="PendingDetailsByCustomer.aspx.cs" Inherits="dd_PendingDetailsByCustomer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                           Pending Details By Customer
                        </div>
                    </div>
                    <div class="panel-body">
                         <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlcustomer" class="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>

                                </div>
                            </div>
                              <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                        
                                        <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Load" OnClick="btnadd_Click" />
                                </div>
                            </div>
                              <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                  <a href="WorkerDeliveryStatus.aspx" class="btn  btn-block btn-info">Go to Worker Delivery Status </a>
                                </div>
                            </div>
                             </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" runat="server" EmptyDataText="No records Found" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                           <asp:TemplateField HeaderText="Delivery DateTime">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                    <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                            <asp:TemplateField HeaderText="Hint & Address">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                    <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="PendingAmt">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PendingCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("OrderId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Complete Order">
                                                <ItemTemplate>
                                                    <a href="#" onclick="popup =window.open('UpdatePendingDetails.aspx?Id=<%#Eval("OrderId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false" class="btn btn-info">Save</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 alert col-lg-offset-2 alert-success">
                                <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-4 col-lg-offset-1 alert alert-info">
                                <b>Total Pending Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


         
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

