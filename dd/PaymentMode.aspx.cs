﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_PaymentMode : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectPaymentMode();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select PaymentMode from PaymentMode where PaymentMode='" + txtpaymentmode.Text + "'");
            if (str != txtpaymentmode.Text)
            {
                CRUD.InsertPaymentMode(txtpaymentmode.Text);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Payment Mode Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Payment Mode is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdatePaymentMode(txtpaymentmode.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Payment Mode Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtpaymentmode.Text = "";
        SelectPaymentMode();
    }

    private void  SelectPaymentMode()
    {
        DataTable dt = CRUD.SelectPaymentMode();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtpaymentmode.Text = ((Label)e.Item.FindControl("lblPaymentMode")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeletePaymentMode(0,id);
            SelectPaymentMode();
            btnadd.Text = "Save";
        }
    }
}