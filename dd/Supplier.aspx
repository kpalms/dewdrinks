﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="Supplier.aspx.cs" Inherits="dd_Supplier" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Supplier
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtmobileno1" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                                    ControlToValidate="txtmobileno2" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtfullname" placeholder="Enter Full Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtcontactperson" placeholder="Enter Contact Person" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtemail" placeholder="Enter Email Id"  class="form-control" runat="server" TextMode="Email"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                         <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno1" placeholder="Enter Mobile No 1" onkeypress="return isNumber(event)" MaxLength="10" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno2" placeholder="Enter Mobile No 2" onkeypress="return isNumber(event)" MaxLength="10" class="form-control" runat="server" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtnote" placeholder="Enter Note"  class="form-control" runat="server" ></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtcity" placeholder="Enter City"  class="form-control" runat="server" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtaddress" placeholder="Enter Address" TextMode="MultiLine" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                       
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" Text="Save" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>FullName</th>
                                                    <th>ContactPerson</th>
                                                    <th>EmailId</th>
                                                    <th>MobileNo1</th>
                                                     <th>MobileNo2</th>
                                                    <th>City</th>
                                                    <th>Note</th>
                                                    <th>Address</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td>
                                                        <asp:Label ID="lblFullName" runat="server" Text='<%# Eval("FullName") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblContactPerson" runat="server" Text='<%# Eval("ContactPerson") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblEmailId" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMobileNo1" runat="server" Text='<%# Eval("MobileNo1") %>'></asp:Label>
                                                    </td>
                                                     <td>
                                                        <asp:Label ID="lblMobileNo2" runat="server" Text='<%# Eval("MobileNo2") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNote" runat="server" Text='<%# Eval("Note") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblAddress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("SupplierId") %>' CssClass="btn btn-block btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("SupplierId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr id="trEmpty" runat="server" visible="false">
                                                    <td colspan="11" align="center">No records found.
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

