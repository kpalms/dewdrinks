﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="ImageGallary.aspx.cs" Inherits="dd_ImageGallary" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="text-center text-uppercase ">
                    Image Gallary
                </div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-lg-offset-6 col-md-offset-6">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ErrorMessage="Please Select Image" ControlToValidate="fileimage" ValidationGroup="new"
                            runat="server" Display="Dynamic" ForeColor="Red" />
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.png|.jpg|.gif|.bmp|.jpeg)$"
                            ControlToValidate="fileimage" ValidationGroup="new" Font-Size="12px" runat="server" ForeColor="Red" ErrorMessage="Please select a valid png,jpg,gif,bmp,jpeg file."
                            Display="Dynamic" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-md-offset-2 col-lg-offset-2">
                        <div class="form-group">
                            <asp:TextBox ID="txttitle" placeholder="Enter Title" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <asp:FileUpload ID="fileimage" class="form-control" runat="server" />
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="form-group">
                            <asp:TextBox ID="txtdescripation" placeholder="Enter Descrption" Rows="5" TextMode="MultiLine" class="form-control" runat="server"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4  col-md-4 col-md-offset-2 col-lg-offset-2">
                        <div class="form-group">
                            <asp:Button ID="btnadd" runat="server" ValidationGroup="new" class="btn  btn-block btn-primary" Text="Save" OnClick="btnadd_Click" />
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4">
                        <div class="form-group">
                            <asp:Button ID="btnupdate" runat="server" class="btn  btn-block btn-primary" Text="Update" OnClick="btnupdate_Click" />
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                    <HeaderTemplate>
                                        <tr>
                                            <th>Sr.No</th>
                                            <th>Title</th>
                                            <th>Image</th>
                                            <th>Descrption</th>
                                            <th>Edit</th>
                                            <th>Delete</th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <tr>
                                            <td><%# Container.ItemIndex + 1 %></td>
                                            <td>
                                                <asp:Label ID="lblTitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:Label ID="lblimagename" runat="server" Text='<%# Eval("ImageName") %>' Visible="false"></asp:Label>
                                                <img src="Gallary/<%# Eval("ImageName") %>" width="50px" height="50px" />
                                            </td>
                                            <td>
                                                <asp:Label ID="lblDescrption" runat="server" Text='<%# Eval("Descrption") %>'></asp:Label>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("ImageGallaryId") %>' CssClass="btn btn-block btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                            </td>
                                            <td>
                                                <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("ImageGallaryId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <FooterTemplate>
                                        <tr id="trEmpty" runat="server" visible="false">
                                            <td colspan="6" align="center">No records found.
                                            </td>
                                        </tr>
                                        </table>
                                    </FooterTemplate>
                                </asp:Repeater>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

