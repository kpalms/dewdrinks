﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="Expences.aspx.cs" Inherits="dd_Expences" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Expences
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtdate" placeholder="Select Date" class="form-control" runat="server" AutoPostBack="true" required OnTextChanged="txtdate_TextChanged"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txtdate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtItemName" placeholder="Enter Item Name" class="form-control" runat="server" required></asp:TextBox>
                                    <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetItems" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtItemName"
                                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtqty" placeholder="Enter Quantity" onkeypress="return isNumber(event)" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtamount" placeholder="Enter Amount"  class="form-control" runat="server" required></asp:TextBox>
                                     <asp:RegularExpressionValidator ID="revDecimals" ValidationGroup="g1" runat="server" ErrorMessage="Enter Rate Like 100.00"
    ControlToValidate="txtamount" ValidationExpression="^\d{1,9}\.\d{1,2}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtnote" placeholder="Enter Note" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Save" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Date</th>
                                                    <th>ItemName</th>
                                                    <th>Qty</th>
                                                    <th>Amount</th>
                                                    <th>Notes</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblItemName" runat="server" Text='<%# Eval("ItemName") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblQty" runat="server" Text='<%# Eval("Qty") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblAmount" runat="server" Text='<%# Eval("Amount") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblNotes" runat="server" Text='<%# Eval("Notes") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("ExpencesId") %>' CssClass="btn btn-block btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("ExpencesId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr id="trEmpty" runat="server" visible="false">
                                                    <td colspan="8" align="center">No records found.
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

