﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="OrderHistory.aspx.cs" Inherits="dd_OrderHistory" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Order History
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    From Date :
                                    <asp:TextBox ID="txtfrmdate" placeholder="Select Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy"
                                        TargetControlID="txtfrmdate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    To Date :
                                    <asp:TextBox ID="txttodate" placeholder="Select Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" DefaultView="Days" runat="server" Format="dd/MM/yyyy"
                                        TargetControlID="txttodate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <br />
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlstatus" class="form-control" runat="server">
                                        <asp:ListItem Value="Delivered">Delivered</asp:ListItem>
                                        <asp:ListItem Value="Cancel">Cancel</asp:ListItem>
                                        <asp:ListItem Value="PlaceOrder">PlaceOrder</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Load" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                            <asp:BoundField DataField="DeDate" HeaderText="Delivery Date" Visible="false" />
                                            <asp:BoundField DataField="Time" HeaderText="Time" Visible="false" />
                                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                            <asp:TemplateField HeaderText="Qty & ItemName">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblqty" runat="server" Text=' <%#Eval("qty")%>'></asp:Label></b>
                                                    <asp:Label ID="lblItemNames" runat="server" Text=' <%#Eval("ItemNames")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hint & Address">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                    <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                             <asp:BoundField DataField="note" HeaderText="note" />
                                            <asp:BoundField DataField="DeliveryNote" HeaderText="DeliveryNote" />
                                            <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" />
                                              <asp:TemplateField HeaderText="TotalAmount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalAmount" runat="server" Text=' <%#Eval("TotalAmount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PendingAmt">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PendingCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                             <div class="col-lg-3 alert alert-success">
                                <b>Total Can :
                                    <asp:Label ID="lbltotalcan" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-3 alert alert-info">
                                <b>Total Amount:
                                    <asp:Label ID="lbltotalamt" runat="server" Text="Label"></asp:Label></b>
                            </div>
                            <div class="col-lg-3 alert alert-danger">
                                <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-3 alert alert-warning">
                                <b>Total Pending Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

