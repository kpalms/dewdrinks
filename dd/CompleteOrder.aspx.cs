﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_CompleteOrder : System.Web.UI.Page
{

    int TotalDeliveredCan = 0, TotalCollectedCan = 0;
    decimal TotalAmount = 0, TotalCollectedAmt = 0;
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            selectDeliveryCard();

            //lbltotcollectedcan.Text = "0";
            //lbltotdelivedcan.Text = "0";
            //lbltotpendingcan.Text = "0";

            //lbltotalamt.Text = "0.00";
            //lblcollecamt.Text = "0.00";
            //lblpendingamt.Text = "0.00";
        }
    }

    private void selectDeliveryCard()
    {
        DataSet dt = CRUD.selectDeliveryCard("Delivered", CRUD.ReturnIndianDate());
        if (dt.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = dt.Tables[0];
            GridView1.DataBind();
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            GridView1.Columns.Clear();
            lbltotcollectedcan.Text = "0";
            lbltotdelivedcan.Text = "0";
            lbltotpendingcan.Text = "0";
        }
        //if (dt.Tables[1].Rows.Count > 0)
        //{
           // lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
           // lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
        //}
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectDeliveryCard();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblqty = (Label)row.FindControl("lblqty");

        CRUD.ChangeStatus(Convert.ToInt32(e.CommandArgument.ToString()), "PlaceOrder");
        CRUD.UpdatePenCanAmt(Convert.ToInt32(e.CommandArgument.ToString()), 0, 0, "", Convert.ToInt32(lblqty.Text));
        CRUD.UseExcuteNonQuery("delete from PaymentHistory where OrderId="+Convert.ToInt32(e.CommandArgument.ToString()));
        selectDeliveryCard();
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Undo Order Successfully !.');", true);

        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
           // Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);
           // DataTable dt = CRUD.selectPeningCanAmt(Convert.ToInt32(lblcustomerid.Text));
            //lblPendingAmt.Text = dt.Rows[0]["PendingAmt"].ToString();
            //lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();

            TotalDeliveredCan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "qty"));
            TotalCollectedCan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "CollectedCan"));
            TotalAmount += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "TotalAmount"));
            TotalCollectedAmt += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "PendingAmt"));

            lbltotcollectedcan.Text = TotalCollectedCan.ToString();
            lbltotdelivedcan.Text = TotalDeliveredCan.ToString();
            lbltotpendingcan.Text = (TotalDeliveredCan - TotalCollectedCan).ToString();

            lbltotalamt.Text = TotalAmount.ToString();
            lblcollecamt.Text = TotalCollectedAmt.ToString();
            lblpendingamt.Text = (TotalAmount - TotalCollectedAmt).ToString();
        }
    }


    protected void lnksearmb_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectDeliveryCardByMb("Delivered", txtmobile.Text, CRUD.ReturnIndianDate());
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtmobile.Text = "";
    }
    protected void lnksearcust_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectDeliveryCardByFName("Delivered", txtcustname.Text, CRUD.ReturnIndianDate());
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtcustname.Text = "";
    }

    protected void lnklastname_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.selectDeliveryCardByLName("Delivered", txtlastname.Text, CRUD.ReturnIndianDate());
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtlastname.Text = "";
    }

    protected void btndefault_Click(object sender, EventArgs e)
    {
        selectDeliveryCard();
    }
}