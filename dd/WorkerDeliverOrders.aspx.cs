﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Text;
public partial class dd_WorkerDeliverOrders : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    //static int id, subid = 0, mb = 0;
     decimal TotalPrice=0, TotalPendingAmount=0;
     int TotalPendingCan=0, TotalCan=0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.selectWorkerName();
            ddlworker.DataSource = dt;
            ddlworker.DataTextField = "FullName";
            ddlworker.DataValueField = "WorkerId";
            ddlworker.DataBind();
            ddlworker.Items.Insert(0, new ListItem("-- Select Worker --", "0"));
            ddlworker.SelectedIndex = 0;
            lbltotalcan.Text = "0";
            lbltotalamt.Text = "0.00";

            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            selectNewDeliveryCard();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
       
        foreach (GridViewRow gvrow in GridView1.Rows)
        {
            var checkbox = gvrow.FindControl("CheckBox1") as CheckBox;
            if (checkbox.Checked)
            {
                var lblOrderId = gvrow.FindControl("lblOrderId") as Label;
               CRUD.InsertOrderDeliveredDetail(Convert.ToInt32(ddlworker.SelectedValue),Convert.ToInt32(lblOrderId.Text));
            }  
        }
        
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Orders Assign Successfully !.');", true);
        selectNewDeliveryCard();
    }

    public void selectNewDeliveryCard()
    {
        DataSet dt = CRUD.selectNewDeliveryCard("PlaceOrder", CRUD.ReturnIndianDate());
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();
            }
                 else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            lbltotalcan.Text = "0";
            lbltotalamt.Text = "0.00";

            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            GridView1.Columns.Clear();
        }
            }
            //if (dt.Tables[1].Rows.Count > 0)
            //{
            //    lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
            //    lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
            //}
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            lbltotalcan.Text = "0";
            lbltotalamt.Text = "0.00";

            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            GridView1.Columns.Clear();
        }
    }

   
    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectNewDeliveryCard();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);

           

            DataTable dt = CRUD.selectPeningCanAmt(Convert.ToInt32(lblcustomerid.Text));
            lblPendingAmt.Text = dt.Rows[0]["PendingAmt"].ToString();
            lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();
            TotalPendingCan += Convert.ToInt32(dt.Rows[0]["CollectedCan"]);
            TotalPendingAmount += Convert.ToDecimal(dt.Rows[0]["PendingAmt"].ToString());

            Label lbltime = (e.Row.FindControl("lbltime") as Label);
            Label lbltimeago = (e.Row.FindControl("lbltimeago") as Label);
            DateTime StartTime = DateTime.Parse(lbltime.Text);
            DateTime EndTime = DateTime.Parse(DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("hh:mm tt"));
            TimeSpan ts = EndTime - StartTime;
            lbltimeago.Text = ts.ToString();

            TotalCan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "qty"));
            TotalPrice += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalAmount"));
            lbltotalcan.Text = TotalCan.ToString();
            lbltotalamt.Text = TotalPrice.ToString();
            lbltotcan.Text = TotalPendingCan.ToString();
            lbltotamt.Text = TotalPendingAmount.ToString();
        }
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        //if (e.CommandName == "Delete")
        //{
        //    //CRUD.InsertOrderDeliveredDetail(Convert.ToInt32(ddlworker.SelectedValue), Convert.ToInt32(e.CommandArgument.ToString()));

        //    //selectNewDeliveryCard();


        //}

        if (e.CommandName == "DeleteRow")
        {
            CRUD.ChangeStatus(Convert.ToInt32(e.CommandArgument.ToString()), "Cancel");
            selectNewDeliveryCard();
        }
        if (e.CommandName == "DeleteOrder")
        {
            CRUD.ChangeStatus(Convert.ToInt32(e.CommandArgument.ToString()), "Delete");
            selectNewDeliveryCard();
        }
    }

    protected void btndefault_Click(object sender, EventArgs e)
    {
        selectNewDeliveryCard();
    }
}