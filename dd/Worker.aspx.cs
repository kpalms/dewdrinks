﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Worker : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectWorkers();
            txtcity.Text = "Pune";
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select Mobile from Worker where Mobile='" + txtmobileno.Text + "'");
            if (str != txtmobileno.Text)
            {
                CRUD.InsertWorker(txtFirstName.Text.Trim(), txtMiddleName.Text.Trim(), txtLastName.Text.Trim(), txtphoneno.Text.Trim(), txtmobileno.Text.Trim(), txtemailid.Text.Trim(), txtCountry.Text.Trim(), txtstate.Text.Trim(), txtcity.Text.Trim(), txtaddress.Text.Trim(), txtpassword.Text.Trim(), Convert.ToDecimal(txtsalary.Text.Trim()), ddlrole.SelectedValue);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Worker Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Worker is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateWorker(txtFirstName.Text.Trim(), txtMiddleName.Text.Trim(), txtLastName.Text.Trim(), txtphoneno.Text.Trim(), txtmobileno.Text.Trim(), txtemailid.Text.Trim(), txtCountry.Text.Trim(), txtstate.Text.Trim(), txtcity.Text.Trim(), txtaddress.Text.Trim(), txtpassword.Text.Trim(), Convert.ToDecimal(txtsalary.Text.Trim()),ddlrole.SelectedValue, id);

            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Worker Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        SelectWorkers();
        txtFirstName.Text="";
        txtMiddleName.Text="";
       txtLastName.Text="";
       txtphoneno.Text="";
        txtmobileno.Text="";
        txtemailid.Text="";
        txtCountry.Text = "India";
        txtstate.Text = "Maharastra";
        txtcity.Text = "Pune";
        txtaddress.Text="";
        txtpassword.Text="";
        txtsalary.Text = "00"; 
        ddlrole.SelectedValue="0";
        txtpassword.Attributes["value"] = "";
        txtretypepass.Attributes["value"] = "";
    }

    private void SelectWorkers()
    {
        DataTable dt = CRUD.SelectWorker();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());

        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtFirstName.Text = ((Label)e.Item.FindControl("lblName")).Text;
            txtMiddleName.Text = ((Label)e.Item.FindControl("lblMiddleName")).Text;
            txtLastName.Text = ((Label)e.Item.FindControl("lblLastName")).Text;
            txtphoneno.Text = ((Label)e.Item.FindControl("lblPhoneNo")).Text;
            txtmobileno.Text = ((Label)e.Item.FindControl("lblMobile")).Text;
            txtemailid.Text = ((Label)e.Item.FindControl("lblEmail")).Text;
            txtsalary.Text = ((Label)e.Item.FindControl("lblSalary")).Text;
            txtCountry.Text = ((Label)e.Item.FindControl("lblCountry")).Text;
            txtstate.Text = ((Label)e.Item.FindControl("lblState")).Text;
            txtcity.Text = ((Label)e.Item.FindControl("lblCity")).Text;
            txtpassword.Attributes["value"] = ((Label)e.Item.FindControl("lblPassword")).Text;
            txtretypepass.Attributes["value"] = ((Label)e.Item.FindControl("lblPassword")).Text;
            txtaddress.Text = ((Label)e.Item.FindControl("lbladdress")).Text;
            ddlrole.SelectedValue = ((Label)e.Item.FindControl("lblRole")).Text;
        }


         if (e.CommandName == "Delete")
        {
            CRUD.DeleteWorker(0,id);
            SelectWorkers();
            btnadd.Text = "Save";
        }
    }
}