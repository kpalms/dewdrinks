﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_subcustmer : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectSubCustomer();
        }
    }

    private void SelectSubCustomer()
    {
        DataTable dt = CRUD.SelectSubCustomer();
        GridView1.DataSource = dt;
        GridView1.DataBind();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        SelectSubCustomer();
    }


    protected void lnksearmb_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.SelectSubCustomerByMb(txtmobile.Text);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtmobile.Text = "";
    }
    protected void lnksearcust_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.SelectSubCustomerByFirstName(txtcustname.Text);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtcustname.Text = "";
    }

    protected void lnklastname_Click(object sender, EventArgs e)
    {
        DataTable dt = CRUD.SelectSubCustomerByLastName(txtlastname.Text);
        GridView1.DataSource = dt;
        GridView1.DataBind();
        txtlastname.Text = "";
    }

    protected void btndefault_Click(object sender, EventArgs e)
    {
        SelectSubCustomer();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblSubscriptionId = (e.Row.FindControl("lblSubscriptionId") as Label);
            Label lblQty = (e.Row.FindControl("lblQty") as Label);
            Label lblDeliveredCan = (e.Row.FindControl("lblDeliveredCan") as Label);
            lblDeliveredCan.Text = CRUD.ReturnOneValue(" select sum(Qty)  from OrderDetail od, Orders  where SubscriptionId='" + lblSubscriptionId.Text + "' and od.OrderId = Orders.OrderId and Orders.Status in ('Delivered','PlaceOrder');");


            if (lblDeliveredCan.Text !="")
            {
            if (Convert.ToInt32(lblQty.Text) == Convert.ToInt32(lblDeliveredCan.Text))
            {
               // CRUD.ReturnOneValue("update Subscription set Active=0 where SubscriptionId='" + lblSubscriptionId.Text + "'");
            }
            }


        }
    }
}