﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="Worker.aspx.cs" Inherits="dd_Worker" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Worker
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtmobileno" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:CompareValidator ID="CompareValidator1" ValidationGroup="g1" ForeColor="Red" ControlToValidate="txtretypepass" ControlToCompare="txtpassword" runat="server" ErrorMessage="Both Password Does Not Match"></asp:CompareValidator>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="rfv1" ValidationGroup="g1" runat="server" ControlToValidate="ddlrole" InitialValue="0" ForeColor="Red" ErrorMessage="please select Role" />
                            </div>
                            <div class="col-lg-3 col-md-3">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtFirstName" placeholder="Enter First Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtMiddleName" placeholder="Enter Middle Name" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtLastName" placeholder="Enter Last Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtphoneno" TextMode="Phone" onkeypress="return isNumber(event)" placeholder="Enter Phone Number" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno" onkeypress="return isNumber(event)" MaxLength="10" placeholder="Enter Mobile Number" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtemailid" placeholder="Enter Email Id" TextMode="Email" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtsalary" onkeypress="return isNumber(event)" placeholder="Enter Salary" class="form-control" Text="00" required runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtCountry" ReadOnly="true" placeholder="Enter Country" class="form-control" runat="server" Text="India" required></asp:TextBox>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtstate" ReadOnly="true" placeholder="Enter State" class="form-control" Text="Maharastra" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtcity" placeholder="Enter City" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlrole" runat="server" class="form-control">
                                        <asp:ListItem  Value="0">--Select Role--</asp:ListItem>
                                        <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                        <asp:ListItem Value="Worker">Worker</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtpassword" TextMode="Password" placeholder="Enter Password" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtretypepass" TextMode="Password" placeholder="Enter ReType Password" class="form-control" runat="server" required></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <asp:TextBox ID="txtaddress" TextMode="MultiLine" placeholder="Enter Address" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" Text="Save" OnClick="btnadd_Click" />

                                </div>
                            </div>
                        </div>

                    </div>



                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                        <HeaderTemplate>
                                            <tr>
                                                <th>Sr.No</th>
                                                <th>FullName</th>
                                                <th>PhoneNo</th>
                                                <th>Mobile</th>
                                                <th>Email</th>
                                                <th>Salary</th>
                                                <th>Address</th>
                                                <th>Role</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <tr>
                                                <td><%# Container.ItemIndex + 1 %></td>
                                                <td>
                                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                    <asp:Label ID="lblMiddleName" runat="server" Text='<%# Eval("MiddleName") %>'></asp:Label>
                                                     <asp:Label ID="lblLastName" runat="server" Text='<%# Eval("LastName") %>'></asp:Label>
                                                </td>
                                              
                                                <td>
                                                    <asp:Label ID="lblPhoneNo" runat="server" Text='<%# Eval("PhoneNo") %>'></asp:Label>

                                                </td>
                                                <td>
                                                    <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblEmail" runat="server" Text='<%# Eval("Email") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblSalary" runat="server" Text='<%# Eval("Salary") %>'></asp:Label>
                                                </td>
                 
                                                <td>
                                                    <asp:Label ID="lbladdress" runat="server" Text='<%# Eval("Address") %>'></asp:Label>,
                                                     <asp:Label ID="lblCity" runat="server" Text='<%# Eval("City") %>'></asp:Label>,
                                                    <asp:Label ID="lblState" runat="server" Text='<%# Eval("State") %>'></asp:Label>,
                                                    <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("Country") %>'></asp:Label>
                                                </td>
                                                <td>
                                                    <asp:Label ID="lblRole" runat="server" Text='<%# Eval("Role") %>'></asp:Label>
                                                     <asp:Label ID="lblPassword" Visible="false" runat="server" Text='<%# Eval("Password") %>'></asp:Label>
                                                </td>
                                               
                                                   
                                               
                                               
                                                <td>
                                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("WorkerId") %>' CssClass="btn btn-block btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                </td>
                                                <td>
                                                    <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("WorkerId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <tr id="trEmpty" runat="server" visible="false">
                                                <td colspan="17" align="center">No records found.
                                                </td>
                                            </tr>
                                            </table>
                                        </FooterTemplate>
                                    </asp:Repeater>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

