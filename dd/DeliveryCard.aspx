﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="DeliveryCard.aspx.cs" Inherits="dd_DeliveryCard" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Delivery Card
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtmobile" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtmobile" ForeColor="Red" ValidationGroup="g1" runat="server" ErrorMessage="please enter mobiel no"></asp:RequiredFieldValidator>

                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtcustname" ForeColor="Red" ValidationGroup="g2" runat="server" ErrorMessage="please enter First name"></asp:RequiredFieldValidator>

                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtlastname" ForeColor="Red" ValidationGroup="g3" runat="server" ErrorMessage="please enter Last Name"></asp:RequiredFieldValidator>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtmobile" placeholder="Enter Mobile No" onkeypress="return isNumber(event)" MaxLength="10" class="form-control" runat="server" required></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:LinkButton ID="lnksearmb" class="btn btn-primary" ValidationGroup="g1" runat="server" OnClick="lnksearmb_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                    </span>
                                </div>
                                <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetMobileNo" MinimumPrefixLength="1"
                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtmobile"
                                    ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtcustname" placeholder="Enter First Name" class="form-control" runat="server" required></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:LinkButton ID="lnksearcust" class="btn btn-primary" ValidationGroup="g2" runat="server" OnClick="lnksearcust_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                    </span>
                                </div>
                                <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetFirstName" MinimumPrefixLength="1"
                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtcustname"
                                    ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <div class="form-group input-group">
                                    <asp:TextBox ID="txtlastname" placeholder="Enter Last Name" class="form-control" runat="server" required></asp:TextBox>
                                    <span class="input-group-btn">
                                        <asp:LinkButton ID="lnklastname" class="btn btn-primary" ValidationGroup="g3" runat="server" OnClick="lnklastname_Click"><i class="fa fa-search"></i></asp:LinkButton>
                                    </span>
                                </div>
                                <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetLastName" MinimumPrefixLength="1"
                                    CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtlastname"
                                    ID="AutoCompleteExtender3" runat="server" FirstRowSelected="false">
                                </asp:AutoCompleteExtender>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <asp:LinkButton ID="btndefault" class="btn btn-primary btn-block" runat="server" OnClick="btndefault_Click">Refresh</asp:LinkButton>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="50" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                            <asp:TemplateField HeaderText="Qty & ItemName">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblqty" runat="server" Text=' <%#Eval("qty")%>'></asp:Label></b>
                                                    <asp:Label ID="lblItemNames" runat="server" Text=' <%#Eval("ItemNames")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Hint & Address">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                    <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                            <asp:TemplateField HeaderText="Delivery DateTime" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                    <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time Ago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltimeago" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="note" HeaderText="PlaceOrderNote" />
                                            <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" />
                                              <asp:TemplateField HeaderText="TotalAmount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalAmount" runat="server" Text=' <%#Eval("TotalAmount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PendingAmt">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           
                                            <asp:TemplateField HeaderText="CollectCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <div class="dropdown">
                                                        <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                                            Action
  <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu">
                                                            <li><a href="#" onclick="popup =window.open('OrderComplete.aspx?Id=<%#Eval("OrderId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false">Ok</a></li>
                                                            <li><a href='PlaceOrder.aspx?Id=<%#Eval("OrderId") %>'>Edit</a></li>

                                                            <li>
                                                                <asp:LinkButton ID="lnkordercomp" CommandName="DeleteRow" runat="server" OnClientClick="return confirm('are you sure want to Cancel Order?');" CommandArgument='<%#Eval("OrderId") %>'>Cancel</asp:LinkButton>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 alert col-lg-offset-2 alert-success">
                                <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-4 col-lg-offset-1 alert alert-info">
                                <b>Total Pending Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

