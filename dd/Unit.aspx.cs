﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Unit : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectUnit();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select UnitName from unit where UnitName='" + txtunit.Text + "'");
            if (str != txtunit.Text)
            {
                CRUD.InsertUnit(txtunit.Text);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Unit  Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Unit  is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateUnit(txtunit.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Unit  Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtunit.Text = "";
        SelectUnit();
    }

    private void SelectUnit()
    {
        DataTable dt = CRUD.SelectUnit();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtunit.Text = ((Label)e.Item.FindControl("lblUnitName")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeleteUnit(0, id);
            SelectUnit();
            btnadd.Text = "Save";
        }
    }
}