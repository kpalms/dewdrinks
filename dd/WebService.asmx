﻿<%@ WebService Language="C#" Class="WebService" %>

using System;
using System.Web;
using System.Web.Services;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using MySql.Data.MySqlClient;
[WebService(Namespace = "http://dd.dewdrinks.com/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class WebService  : System.Web.Services.WebService {

    [WebMethod]
    public List<string> GetMobileNo(string prefixText)
    {
        List<string> empResult = new List<string>();
        using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.CommandText = "select DISTINCT MobileNo from Customer where IsDelete=1 and MobileNo  LIKE '" + prefixText + "%'";
                cmd.Connection = con;
                con.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    empResult.Add( dr["MobileNo"].ToString());
                }
                con.Close();
                return empResult;
            }
        }
    }


    [WebMethod]
    public List<string> GetFirstName(string prefixText)
    {
        List<string> empResult = new List<string>();
        using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.CommandText = "select DISTINCT FirstName from Customer where IsDelete=1 and FirstName  LIKE '%" + prefixText + "%'";
                cmd.Connection = con;
                con.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    empResult.Add(dr["FirstName"].ToString());
                }
                con.Close();
                return empResult;
            }
        }
    }


    [WebMethod]
    public List<string> GetLastName(string prefixText)
    {
        List<string> empResult = new List<string>();
        using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.CommandText = "select DISTINCT LastName from Customer where IsDelete=1 and LastName  LIKE '%" + prefixText + "%'";
                cmd.Connection = con;
                con.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    empResult.Add(dr["LastName"].ToString());
                }
                con.Close();
                return empResult;
            }
        }
    }

    
    

     [WebMethod]
    public  List<string> GetItems(string prefixText)
    {
        List<string> empResult = new List<string>();
        using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
        {
            using (MySqlCommand cmd = new MySqlCommand())
            {
                cmd.CommandText = "select DISTINCT ItemName from Expences where  ItemName  LIKE '%" + prefixText + "%'";
                cmd.Connection = con;
                con.Open();
                MySqlDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    empResult.Add(dr["ItemName"].ToString());
                }
                con.Close();
                return empResult;
            }
        }
    }



     [WebMethod]
     public List<string> GetSMobileNo(string prefixText)
     {
         List<string> empResult = new List<string>();
         using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
         {
             using (MySqlCommand cmd = new MySqlCommand())
             {
                 cmd.CommandText = "select DISTINCT MobileNo from Customer c inner join Subscription s on s.CustId=c.CustomerId where IsDelete=1  and MobileNo  LIKE '" + prefixText + "%'";
                 cmd.Connection = con;
                 con.Open();
                 MySqlDataReader dr = cmd.ExecuteReader();
                 while (dr.Read())
                 {
                     empResult.Add(dr["MobileNo"].ToString());
                 }
                 con.Close();
                 return empResult;
             }
         }
     }


     [WebMethod]
     public List<string> GetSFirstName(string prefixText)
     {
         List<string> empResult = new List<string>();
         //string name ;
         using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
         {
             using (MySqlCommand cmd = new MySqlCommand())
             {
                 cmd.CommandText = "select DISTINCT FirstName,Hint from Customer c inner join Subscription s on s.CustId=c.CustomerId where IsDelete=1  and FirstName  LIKE '%" + prefixText + "%'";
                 cmd.Connection = con;
                 con.Open();
                 MySqlDataReader dr = cmd.ExecuteReader();
                 while (dr.Read())
                 {
                  //   name = dr["FirstName"].ToString() + " " + dr["Hint"].ToString();
                     empResult.Add(dr["FirstName"].ToString());
                 }
                 con.Close();
                 return empResult;
             }
         }
     }


     [WebMethod]
     public List<string> GetSLastName(string prefixText)
     {
         List<string> empResult = new List<string>();
         using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
         {
             using (MySqlCommand cmd = new MySqlCommand())
             {
                 cmd.CommandText = "select DISTINCT LastName from Customer c inner join Subscription s on s.CustId=c.CustomerId where IsDelete=1  and LastName  LIKE '%" + prefixText + "%'";
                 cmd.Connection = con;
                 con.Open();
                 MySqlDataReader dr = cmd.ExecuteReader();
                 while (dr.Read())
                 {
                     empResult.Add(dr["LastName"].ToString());
                 }
                 con.Close();
                 return empResult;
             }
         }
     }


    //by kundan
     [WebMethod]
     public List<string> GetCustomer(string prefixText)
     {
         List<string> empResult = new List<string>();
         using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
         {
             using (MySqlCommand cmd = new MySqlCommand())
             {
                 cmd.CommandText = "select CONCAT(FirstName, ' ', LastName,' ',Hint,'@',MobileNo) as FirstName from Customer where IsDelete=1 and FirstName  LIKE '%" + prefixText + "%'";
                 cmd.Connection = con;
                 con.Open();
                 MySqlDataReader dr = cmd.ExecuteReader();
                 while (dr.Read())
                 {
                     empResult.Add(dr["FirstName"].ToString());
                 }
                 con.Close();
                 return empResult;
             }
         }
     }
    
    

   
}