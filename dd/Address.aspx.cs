﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Address : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int cid;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.SelectCity();
            ddlcity.DataSource = dt;
            ddlcity.DataTextField = "CityName";
            ddlcity.DataValueField = "CityId";
            ddlcity.DataBind();
            ddlcity.Items.Insert(0, new ListItem("-- Select City --", "0"));
            ddlcity.SelectedIndex = 0;

            if (Request.QueryString["A"].ToString() == "E")
            {
                DataTable aa = CRUD.SelectAddressById(Convert.ToInt32(Request.QueryString["Id"]));

                if (aa.Rows.Count > 0)
                {
                    txtaddress.Text = aa.Rows[0]["Address"].ToString();
                    txtCompanyName.Text = aa.Rows[0]["Company"].ToString();
                    txtAppartmentName.Text = aa.Rows[0]["Appartment"].ToString();
                    txtFlatNo.Text = aa.Rows[0]["Flatno"].ToString();
                    ddlcity.SelectedValue = aa.Rows[0]["CityId"].ToString();
                    SelectArea();
                    ddlarea.SelectedValue = aa.Rows[0]["AreaId"].ToString();
                }
                btnadd.Text = "Update";
            }
           
        }
    }

    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectArea();
    }

    private void SelectArea()
    {
        DataTable dt = CRUD.Selectarea(Convert.ToInt32(ddlcity.SelectedValue));
        ddlarea.DataSource = dt;
        ddlarea.DataTextField = "AreaName";
        ddlarea.DataValueField = "AreaId";
        ddlarea.DataBind();
        ddlarea.Items.Insert(0, new ListItem("-- Select Area --", "0"));
        ddlarea.SelectedIndex = 0;
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
                CRUD.InsertAddress(DateTime.Now, Convert.ToInt32(Request.QueryString["Id"]), txtCompanyName.Text, txtFlatNo.Text, txtAppartmentName.Text, txtaddress.Text, Convert.ToInt32(ddlcity.SelectedValue), Convert.ToInt32(ddlarea.SelectedValue));
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Address Added Successfully !.');", true);
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateAddress(DateTime.Now, Convert.ToInt32(Request.QueryString["Id"]), txtCompanyName.Text, txtFlatNo.Text, txtAppartmentName.Text, txtaddress.Text, Convert.ToInt32(ddlcity.SelectedValue), Convert.ToInt32(ddlarea.SelectedValue));
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Address Updated Successfully !.');", true);
        }
       
        txtaddress.Text = "";
        txtAppartmentName.Text = "";
        txtCompanyName.Text = "";
        txtFlatNo.Text = "";
        ddlarea.SelectedValue = "0";
        ddlcity.SelectedValue = "0";

    }
}