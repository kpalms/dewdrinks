﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Address.aspx.cs" Inherits="dd_Address" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <link href="dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script src="bower_components/JavaScript.js" type="text/javascript"></script>
    <script>

        function closingCode() {
            opener.location.reload(true);
            self.close();
        }
    </script>


</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div class="container-fluid">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-center text-uppercase ">
                                Customer Address
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlcity" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <asp:DropDownList ID="ddlarea" class="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtCompanyName" placeholder="Enter Company Name " class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtAppartmentName" placeholder="Enter Appartment Name" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtFlatNo" placeholder="Enter FlatNo" class="form-control" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <asp:TextBox ID="txtaddress" TextMode="MultiLine" placeholder="Enter Address" class="form-control" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <asp:RequiredFieldValidator ID="rfv1" ValidationGroup="g1" runat="server" ControlToValidate="ddlcity" InitialValue="0" ForeColor="Red" ErrorMessage="please select City"></asp:RequiredFieldValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="g1" runat="server" ControlToValidate="ddlarea" InitialValue="0" ForeColor="Red" ErrorMessage="please select Area"></asp:RequiredFieldValidator>
                            </div>
                            <div class="row">
                                <div class="col-lg-4 col-lg-offset-2 col-md-4 col-md-offset-2">
                                    <div class="form-group">
                                        <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" OnClick="btnadd_Click" Text="Save" />

                                    </div>
                                    <div class="col-lg-4 col-md-4 ">
                                    <div class="form-group">
                                        <a href="CustomerAddress.aspx?Id=<%=Request.QueryString["Id"] %>" class="btn  btn-block btn-info">GO TO CUSTOMER ADDRESS BOOK</a>
                                    </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
