﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class dd_ReportPurchaseDetails : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        DataTable dtt = CRUD.SelectPurchaseDetailsById(Convert.ToInt32(Request.QueryString["PurchaseId"]));
        dvaddress.DataSource = dtt;
        dvaddress.DataBind();
    }
}