﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="ReportPurchase.aspx.cs" Inherits="dd_ReportPurchase" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Report Purchase
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4  col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtfromdate" placeholder="Select From Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txtfromdate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-lg-4  col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txttodate" placeholder="Select To Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender2" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txttodate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Load" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive" id="purchasediv">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                            <asp:BoundField DataField="InVoiceNo" HeaderText="InVoiceNo" />
                                            <asp:BoundField DataField="FullName" HeaderText="Supplier FullName" />
                                            <asp:BoundField DataField="Rate" HeaderText="Total Amount" />
                                            <asp:TemplateField HeaderText="Complete Order">
                                                <ItemTemplate>
                                                    <a href="#" onclick="popup =window.open('ReportPurchaseDetails.aspx?PurchaseId=<%# Eval("PurchaseId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false" class="btn btn-primary btn-xs">View Item List</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 alert alert-info">
                                <b>Total Amount :
                                    <asp:Label ID="lbltotalamt" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

