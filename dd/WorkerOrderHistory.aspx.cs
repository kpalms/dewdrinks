﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.Services;
using MySql.Data.MySqlClient;
using System.Text;
public partial class dd_WorkerOrderHistory : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    decimal TotalPrice = 0, TotalPendingAmount = 0;
    int TotalPendingCan = 0, TotalCan = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.selectWorkerName();
            ddlworker.DataSource = dt;
            ddlworker.DataTextField = "FullName";
            ddlworker.DataValueField = "WorkerId";
            ddlworker.DataBind();
            ddlworker.Items.Insert(0, new ListItem("-- Select Worker --", "0"));
            ddlworker.SelectedIndex = 0;

            txtfrmdate.Text = CRUD.ReturnIndianDate();
            txttodate.Text = CRUD.ReturnIndianDate();

            lbltotalcan.Text = "0";
            lbltotalamt.Text = "0.00";

            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
        }
    }

    private void selectOrderHistoryByWorker()
    {
        DataSet dt = CRUD.selectOrderHistoryByWorker(txtfrmdate.Text, txttodate.Text, ddlstatus.Text,Convert.ToInt32(ddlworker.SelectedValue));
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
                lbltotalcan.Text = "0";
                lbltotalamt.Text = "0.00";

                lbltotcan.Text = "0";
                lbltotamt.Text = "0.00";
                GridView1.Columns.Clear();
            }
            //if (dt.Tables[1].Rows.Count > 0)
            //{
            //    lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
            //    lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
            //}
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            lbltotalcan.Text = "0";
            lbltotalamt.Text = "0.00";

            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            GridView1.Columns.Clear();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        selectOrderHistoryByWorker();
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectOrderHistoryByWorker();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);
            DataTable dt = CRUD.selectPeningCanAmt(Convert.ToInt32(lblcustomerid.Text));
            lblPendingAmt.Text = dt.Rows[0]["PendingAmt"].ToString();
            lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();

            TotalPendingCan += Convert.ToInt32(dt.Rows[0]["CollectedCan"]);
            TotalPendingAmount += Convert.ToDecimal(dt.Rows[0]["PendingAmt"].ToString());
            TotalCan += Convert.ToInt32(DataBinder.Eval(e.Row.DataItem, "qty"));
            TotalPrice += Convert.ToDecimal(DataBinder.Eval(e.Row.DataItem, "TotalAmount"));

            lbltotalcan.Text = TotalCan.ToString();
            lbltotalamt.Text = TotalPrice.ToString();
            lbltotcan.Text = TotalPendingCan.ToString();
            lbltotamt.Text = TotalPendingAmount.ToString();
        }
    }
}