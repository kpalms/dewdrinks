﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Area : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.SelectCity();
            ddlcity.DataSource = dt;
            ddlcity.DataTextField = "CityName";
            ddlcity.DataValueField = "CityId";
            ddlcity.DataBind();
            ddlcity.Items.Insert(0, new ListItem("-- Select City --", "0"));
            ddlcity.SelectedIndex = 0;
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select AreaName from Area where AreaName='" + txtarea.Text + "'");
            if (str != txtarea.Text)
            {
                CRUD.Insertarea(txtarea.Text, Convert.ToInt32(ddlcity.SelectedValue));
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Area Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Area is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.Updatearea(txtarea.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Area Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtarea.Text = "";
        SelectArea();
    }

    private void SelectArea()
    {
        DataTable dt = CRUD.Selectarea(Convert.ToInt32(ddlcity.SelectedValue));
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }
    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtarea.Text = ((Label)e.Item.FindControl("lblAreaName")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.Deletearea(0,id);
            SelectArea();
            btnadd.Text = "Save";
        }
    }
    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectArea();
    }
}