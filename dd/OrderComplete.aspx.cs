﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_OrderComplete : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        CRUD.ChangeStatus(Convert.ToInt32(Request.QueryString["Id"]), "Delivered");
        CRUD.UpdatePenCanAmt(Convert.ToInt32(Request.QueryString["Id"]), Convert.ToInt32(txtpencan.Text), Convert.ToInt32(txtpenamt.Text), txtdeliveryNote.Text, Convert.ToInt32(txtDeliveredCan.Text));
        CRUD.InsertPaymentHistory(Convert.ToInt32(Request.QueryString["Id"]),Convert.ToDecimal(txtpenamt.Text));
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification :Delivery Successfully !.');", true);
      
        System.Web.UI.ScriptManager.RegisterClientScriptBlock(btnadd, typeof(Button), "Script", "refreshAndClose();", true); 
    }
}