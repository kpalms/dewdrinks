﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_VideoGallary : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectVideoGallary();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select Title from VideoGallary where Title='" + txttitle.Text + "'");
            if (str != txttitle.Text)
            {
                CRUD.InsertVideoGallary(txttitle.Text,txtdescripation.Text,txturl.Text);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Video Gallary Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Video Gallary is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateVideoGallary(txttitle.Text, txtdescripation.Text, txturl.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Video Gallary Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txttitle.Text = "";
        txturl.Text = "";
        txtdescripation.Text = "";
        SelectVideoGallary();
    }

    private void SelectVideoGallary()
    {
        DataTable dt = CRUD.SelectVideoGallary();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txttitle.Text = ((Label)e.Item.FindControl("lblTitle")).Text;
            txturl.Text = ((Label)e.Item.FindControl("lblURL")).Text;
            txtdescripation.Text = ((Label)e.Item.FindControl("lblDescrption")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeleteVideoGallary(id);
            SelectVideoGallary();
            btnadd.Text = "Save";
        }
    }
}