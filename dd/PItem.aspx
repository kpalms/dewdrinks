﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="PItem.aspx.cs" Inherits="dd_PItem" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                          Purchase  Item
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtitemname" placeholder="Enter Item Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtrate" placeholder="Enter Rate"  class="form-control" runat="server" required></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="revDecimals" ValidationGroup="g1" runat="server" ErrorMessage="Enter Rate Like 100.00"
    ControlToValidate="txtrate" ValidationExpression="^\d{1,9}\.\d{1,2}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                     </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" ValidationGroup="g1" Text="Save" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Item Name</th>
                                                    <th>Rate</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td>
                                                        <asp:Label ID="lblItemNames" runat="server" Text='<%# Eval("ItemNames") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblRate" runat="server" Text='<%# Eval("Rate") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("ItemId") %>' CssClass="btn btn-block btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("ItemId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr id="trEmpty" runat="server" visible="false">
                                                    <td colspan="5" align="center">No records found.
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
