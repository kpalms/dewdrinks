﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="DashBoard.aspx.cs" Inherits="dd_DashBoard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <br />
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-location-arrow fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="City.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">City</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-map-marker fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="Area.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Area</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-tint fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="Item.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Item</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-credit-card fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="PaymentMode.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Payment Mode</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-image fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="ImageGallary.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Image Gallary</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-video-camera   fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="VideoGallary.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Video Gallary</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-edit    fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="PlaceOrder.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Place Order</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-truck fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="DeliveryCard.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Delivery Card</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-check-square-o   fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="CompleteOrder.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Complete Order</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-calendar   fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="OrderHistory.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Order History</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
          
              <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-spinner fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="PendingOrders.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Pending Order</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-spinner fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="PendingDetails.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Pending Details</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-location-arrow fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="WorkerDeliverOrders.aspx">
                        <div class="panel-footer">
                            <span class="pull-left"> Order Assign to Worker</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-map-marker fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="WorkerDeliveryStatus.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Worker Delivery Status</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-tint fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="PendingDetailsByCustomer.aspx?Id=0">
                        <div class="panel-footer">
                            <span class="pull-left">Pending Details Cust</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-credit-card fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="WorkerOrderHistory.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Worker Order History</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-image fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="CustomerOrderHistory.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Customer Order History</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-video-camera   fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="CustomerCanHistory.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">CustomerCanHistory</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>


    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-user fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="Customer.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Create Customer</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-users fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="CustomerList.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Customer List</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-green">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-trash-o fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="DeletedCustomerList.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Deleted Customer List</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa  fa-file-text fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="Subscription.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Subscripation</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-2 col-md-3">
                <div class="panel panel-red">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-file-text fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="subcustmer.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Subscription Customer</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


              <div class="col-lg-2 col-md-3">
                <div class="panel panel-yellow">
                    <div class="panel-heading">
                        <div class="row">
                            <div class="col-xs-12">
                                <i class="fa fa-list-alt   fa-5x"></i>
                            </div>
                        </div>
                    </div>
                    <a href="SubOrderHistory.aspx">
                        <div class="panel-footer">
                            <span class="pull-left">Subscripation History</span>
                            <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                            <div class="clearfix"></div>
                        </div>
                    </a>
                </div>
            </div>


          
        </div>
    </div>
</asp:Content>

