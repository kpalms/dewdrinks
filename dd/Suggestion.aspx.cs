﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Suggestion : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectSuggestion();
           txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        string dd, tt, add="";
        if(txtdate.Text.Length  ==10)
        {
        dd = txtdate.Text;
        tt = DateTime.Now.ToString("hh:mm:ss tt");
        add = dd + " " + tt;
        }
        else
        {
            add = txtdate.Text;
        }
       
        if (btnadd.Text == "Save")
        {
            CRUD.InsertSuggestion(Convert.ToDateTime(add), txtfullname.Text, txtmobileno.Text, txtdescripation.Text);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Suggestion Added Successfully !.');", true);
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateSuggestion(Convert.ToDateTime(add), txtfullname.Text, txtmobileno.Text, txtdescripation.Text, id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Suggestion Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtdate.Text = DateTime.Now.ToString("dd/MM/yyyy");
        txtfullname.Text = "";
        txtdescripation.Text = "";
        txtmobileno.Text = "";
        SelectSuggestion();
    }

    private void SelectSuggestion()
    {
        DataTable dt = CRUD.SelectSuggestion();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }

    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtdate.Text = ((Label)e.Item.FindControl("lblDate")).Text;
            txtfullname.Text = ((Label)e.Item.FindControl("lblFullName")).Text;
            txtmobileno.Text = ((Label)e.Item.FindControl("lblMobileNo")).Text;
            txtdescripation.Text = ((Label)e.Item.FindControl("lblDescrption")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.Deletesuggestion(id);
            SelectSuggestion();
            btnadd.Text = "Save";
        }
    }
   
}