﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_PendingDetails : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id = 0;
    decimal TotalPrice = 0, TotalPendingAmount = 0;
    int TotalPendingCan = 0, TotalCan = 0,TotalSPendingCan = 0, TotalSCan = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            
            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            lblStotcan.Text = "0";
            selectPendingDetails();
        }
    }

    private void selectPendingDetails()
    {
        DataSet dt = CRUD.selectPendingDetails();
        if (dt != null)
        {
            if (dt.Tables[0].Rows.Count > 0)
            {
                GridView1.DataSource = dt.Tables[0];
                GridView1.DataBind();
            }
            else
            {
                GridView1.DataSource = null;
                GridView1.DataBind();
                lbltotcan.Text = "0";
                lbltotamt.Text = "0.00";
                GridView1.Columns.Clear();
            }

            if (dt.Tables[1].Rows.Count > 0)
            {
                GridView2.DataSource = dt.Tables[1];
                GridView2.DataBind();
            }
            else
            {
                GridView2.DataSource = null;
                GridView2.DataBind();
                lbltotcan.Text = "0";
                lbltotamt.Text = "0.00";
                GridView2.Columns.Clear();
            }
            //if (dt.Tables[1].Rows.Count > 0)
            //{
            //    lbltotamt.Text = dt.Tables[1].Rows[0]["PendingAmt"].ToString();
            //    lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
            //}
        }
        else
        {
            GridView1.DataSource = null;
            GridView1.DataBind();
            lbltotcan.Text = "0";
            lbltotamt.Text = "0.00";
            GridView1.Columns.Clear();

            GridView2.DataSource = null;
            GridView2.DataBind();
            lblStotcan.Text = "0";
            GridView2.Columns.Clear();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectPendingDetails();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
    }

    protected void GridView2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView2.PageIndex = e.NewPageIndex;
        selectPendingDetails();
    }

    protected void GridView2_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);
            //DataTable dt = CRUD.selectPeningCanAmt(Convert.ToInt32(lblcustomerid.Text));
            //lblPendingAmt.Text = dt.Rows[0]["PendingAmt"].ToString();
            //lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();

            TotalPendingCan += Convert.ToInt32(lblPendingCan.Text);
            TotalPendingAmount += Convert.ToDecimal(lblPendingAmt.Text);
           
            lbltotcan.Text = TotalPendingCan.ToString();
            lbltotamt.Text = TotalPendingAmount.ToString();
        }
    }


    protected void GridView2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblSPendingCan = (e.Row.FindControl("lblSPendingCan") as Label);
            TotalSPendingCan += Convert.ToInt32(lblSPendingCan.Text);
            lblStotcan.Text = TotalSPendingCan.ToString();
        }
    }
}