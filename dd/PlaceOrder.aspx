﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="PlaceOrder.aspx.cs" Inherits="dd_PlaceOrder" %>


<%@ Register TagPrefix="Ajaxified" Assembly="Ajaxified" Namespace="Ajaxified" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">

        function calculate() {
            var myBox1 = document.getElementById("<%=txtqty.ClientID %>").value;
            var myBox2 = document.getElementById("<%=txtrate.ClientID %>").value;
            if (myBox1 != null && myBox2 != null) {
                document.getElementById("<%=txttotal.ClientID %>").value = myBox1 * myBox2;
            }
        }
    </script>


    <script type="text/javascript">
        function HideLabel() {
            var seconds = 5;
            setTimeout(function () {
                document.getElementById("<%=lblMessage.ClientID %>").style.display = "none";
            }, seconds * 1000);
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Place Order
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">


                            <strong style="text-align: center">
                                <asp:Label ID="lblMessage" ForeColor="red" Visible="false" Font-Size="Medium" Font-Bold="true" runat="server" /></strong>

                        </div>
                        <div class="row">
                            <center>  <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                        ControlToValidate="txtmobileno" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                        ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator></center>
                        </div>
                        <div class="row">
                            <center>  <asp:RequiredFieldValidator ID="rfv1" ValidationGroup="g1" runat="server" ControlToValidate="ddladdress" InitialValue="0" ForeColor="Red" ErrorMessage="please select Address" />
                </center>
                        </div>
                        <div class="row">
                            <center> <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="g1" runat="server" ControlToValidate="ddlitem" InitialValue="0" ForeColor="Red" ErrorMessage="please select Item" />
              </center>
                        </div>
                        <%-- <div class="row">
                            <center> <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="g1" runat="server" ControlToValidate="ddlcustomer" InitialValue="0" ForeColor="Red" ErrorMessage="please select Customer Name" />
              </center>
                        </div>--%>
                        <div class="row">
                            <center>  <asp:CompareValidator ID="CompareValidator1" runat="server"
                        ControlToValidate="txtqty"
                        Operator="GreaterThanEqual"
                        ValueToCompare="1"
                        Type="Integer"
                        ForeColor="Red"
                        ValidationGroup="g1"
                        ErrorMessage="The integer cannot be less than zero"
                        Display="Dynamic"
                        Text="The integer cannot be less than one" /> </center>
                        </div>
                        <div class="row">
                            <center> <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="g1" runat="server" ControlToValidate="ddlpaymentmode" InitialValue="00" ForeColor="Red" ErrorMessage="please select Payment Mode" />
                 <asp:RegularExpressionValidator ID="revDecimals" ValidationGroup="g1" runat="server" ErrorMessage="Enter Rate Like 100.00"
    ControlToValidate="txtrate" ValidationExpression="^\d{1,9}\.\d{1,2}$" ForeColor="Red"></asp:RegularExpressionValidator>
                                 </center>
                        </div>

                        <asp:LinkButton ID="lnkaddcustomer" runat="server"  PostBackUrl="~/dd/Customer.aspx">Add Customer</asp:LinkButton>




                        <div class="row">
                            <div class="form-group">

<div class="col-lg-4 col-md-offset-8">                                
<asp:LinkButton ID="lnksubcustmer" Enabled="false" class="btn  btn-block btn-info" PostBackUrl="~/dd/subcustmer.aspx" runat="server">GO TO CUSTOMER SUBSCRIPTION</asp:LinkButton>
</div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtcustomer" runat="server" AutoPostBack="true" placeholder="Enter CustomerName" class="form-control" OnTextChanged="txtcustomer_TextChanged"></asp:TextBox>
                                    <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetCustomer" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtcustomer"
                                        ID="AutoCompleteExtender2" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>




                                  <%--  <asp:DropDownList ID="ddlcustomer" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlcustomer_SelectedIndexChanged"></asp:DropDownList>--%>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno" onkeypress="return isNumber(event)" AutoPostBack="true" OnTextChanged="txtmobileno_TextChanged" placeholder="Enter Mobile No" class="form-control" runat="server" MaxLength="10" required></asp:TextBox>
                                    <asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetMobileNo" MinimumPrefixLength="1"
                                        CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtmobileno"
                                        ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
                                    </asp:AutoCompleteExtender>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddladdress" AutoPostBack="true" class="form-control" runat="server"></asp:DropDownList>
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlitem" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlitem_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtrate" ValidationGroup="g1" placeholder="Enter Rate" class="form-control" runat="server" required></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">

                                    <asp:TextBox ID="txtqty" onchange="calculate()" onkeypress="return isNumber(event)" placeholder="Enter Quntity" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txttotal" onkeypress="return isNumber(event)" placeholder="Enter Total" class="form-control" runat="server" ReadOnly="true" required></asp:TextBox>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlpaymentmode" class="form-control" runat="server">
                                      
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtdate" placeholder="Select Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txtdate">
                                    </ajaxToolkit:CalendarExtender>
                                </div>
                            </div>


                        </div>
                        <div class="row">
                            <div class="col-lg-2 col-md-2">
                                <div class="form-group">
                                    <asp:TextBox ID="txttime" ReadOnly="true" placeholder="Select Time" class=" form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <Ajaxified:TimePicker ID="TimePicker1" runat="server" TargetControlID="txttime" MinuteStep="10" CloseOnSelection="true"></Ajaxified:TimePicker>

                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtnote" placeholder="Enter Note" class=" form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" Text="PlaceOrder" OnClick="btnadd_Click" />

                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


