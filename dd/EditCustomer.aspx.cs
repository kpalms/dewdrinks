﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_EditCustomer : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.SelectCustomerById(Convert.ToInt32(Request.QueryString["Id"]));
            if (dt.Rows.Count > 0)
            {
                txtFirstName.Text = dt.Rows[0]["FirstName"].ToString();
                txtLastName.Text = dt.Rows[0]["LastName"].ToString();
                txtmobileno.Text = dt.Rows[0]["MobileNo"].ToString();
                txtemailid.Text = dt.Rows[0]["Email"].ToString();
                txthindt.Text = dt.Rows[0]["Hint"].ToString();
                if ("Male" == dt.Rows[0]["Gender"].ToString())
                {
                    rdgender.SelectedValue = "0";
                }
                else
                {
                    rdgender.SelectedValue = "1";
                }
            }
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        CRUD.UpdateCustomer(txtFirstName.Text, txtLastName.Text, txtmobileno.Text, txtemailid.Text, txthindt.Text, Convert.ToInt32(rdgender.SelectedValue), Convert.ToInt32(Request.QueryString["Id"]));
        Response.Redirect("CustomerList.aspx");
    }
}