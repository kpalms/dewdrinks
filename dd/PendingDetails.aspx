﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="PendingDetails.aspx.cs" Inherits="dd_PendingDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Pending Details
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#UnSubscriber" data-toggle="tab" aria-expanded="true">UnSubscriber</a>
                                    </li>
                                    <li class=""><a href="#Subscriber" data-toggle="tab" aria-expanded="false">Subscriber</a>
                                    </li>
                                </ul>

                                </ul>
                                <div class="table-responsive">
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="UnSubscriber">
                                            <asp:GridView ID="GridView1" runat="server" EmptyDataText="No records Found" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr.No">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <%--<asp:BoundField DataField="Date" HeaderText="Date" />
                                           <asp:TemplateField HeaderText="Delivery DateTime">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                    <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>--%>

                                                    <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" />
                                                    <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                                    <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                                    <asp:TemplateField HeaderText="Hint & Address">
                                                        <ItemTemplate>
                                                            <b>
                                                                <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                            <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PendingAmt">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="PendingCan">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Complete Order">
                                                        <ItemTemplate>
                                                            <a href="#" onclick="popup =window.open('UpdatePendingDetails.aspx?Id=<%#Eval("CustomerId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false" class="btn btn-info">Save</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                            <div class="row">
                                                <div class="col-lg-4 alert col-lg-offset-2 alert-success">
                                                    <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                                                </div>

                                                <div class="col-lg-4 col-lg-offset-1 alert alert-info">
                                                    <b>Total Collected Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade" id="Subscriber">
                                            <asp:GridView ID="GridView2" runat="server" EmptyDataText="No records Found" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView2_PageIndexChanging" OnRowDataBound="GridView2_RowDataBound" OnRowCommand="GridView2_RowCommand">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr.No">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CustomerId" HeaderText="CustomerId" />
                                                    <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                                    <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                                    <asp:TemplateField HeaderText="Hint & Address">
                                                        <ItemTemplate>
                                                            <b>
                                                                <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                            <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="PendingCan">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblSPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblScustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Complete Order">
                                                        <ItemTemplate>
                                                            <a href="#" onclick="popup =window.open('UpdatePendingDetails.aspx?Id=<%#Eval("CustomerId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false" class="btn btn-info">Save</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>

                                             <div class="row">
                                               
                                                <div class="col-lg-4 col-lg-offset-1 alert alert-info">
                                                    <b>Total Collected Can :
                                    <asp:Label ID="lblStotcan" runat="server" Text="Label"></asp:Label></b>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>


