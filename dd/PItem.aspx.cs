﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_PItem : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            SelectItem();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select ItemNames from pitem where ItemNames='" + txtitemname.Text + "'");
            if (str != txtitemname.Text)
            {
                CRUD.InsertPItem(txtitemname.Text,Convert.ToDecimal( txtrate.Text));
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Item Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Item is Already Exists !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdatPItem(txtitemname.Text, Convert.ToDecimal(txtrate.Text), id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Item Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtitemname.Text = "";
        txtrate.Text = "";
        SelectItem();
    }

    private void SelectItem()
    {
        DataTable dt = CRUD.SelectPItem();
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }
    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtitemname.Text = ((Label)e.Item.FindControl("lblItemNames")).Text;
            txtrate.Text = ((Label)e.Item.FindControl("lblRate")).Text;
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeletPItem(0, id);
            SelectItem();
            btnadd.Text = "Save";
        }
    }
}