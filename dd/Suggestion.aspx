﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="Suggestion.aspx.cs" Inherits="dd_Suggestion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Suggestion
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-lg-offset-8 col-md-offset-8">
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                    ControlToValidate="txtmobileno" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                    ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtdate" placeholder="Select Date" class="form-control" runat="server" required></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                        TargetControlID="txtdate"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-md-4 col-lg-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtfullname" placeholder="Enter Full Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno" placeholder="Enter Mobile No" onkeypress="return isNumber(event)" MaxLength="10" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <asp:TextBox ID="txtdescripation" Rows="5" placeholder="Enter Descrption" TextMode="MultiLine" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Save" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <asp:Repeater ID="rptcourse" runat="server" OnItemCommand="rptcourse_ItemCommand">
                                            <HeaderTemplate>
                                                <tr>
                                                    <th>Sr.No</th>
                                                    <th>Date</th>
                                                    <th>FullName</th>
                                                    <th>MobileNo</th>
                                                    <th>Comments</th>
                                                    <th>Edit</th>
                                                    <th>Delete</th>
                                                </tr>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <tr>
                                                    <td><%# Container.ItemIndex + 1 %></td>
                                                    <td>
                                                        <asp:Label ID="lblDate" runat="server" Text='<%# Eval("Date") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblFullName" runat="server" Text='<%# Eval("FullName") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblMobileNo" runat="server" Text='<%# Eval("MobileNo") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:Label ID="lblDescrption" runat="server" Text='<%# Eval("Comments") %>'></asp:Label>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkEdit" runat="server" CommandArgument='<%#Eval("SuggestionId") %>' CssClass="btn btn-block btn-info" CommandName="Edit">Edit</asp:LinkButton>
                                                    </td>
                                                    <td>
                                                        <asp:LinkButton ID="lnkdelete" runat="server" CommandArgument='<%#Eval("SuggestionId") %>' CssClass="btn btn-block btn-danger" CommandName="Delete" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to delete?');">Delete</asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <tr id="trEmpty" runat="server" visible="false">
                                                    <td colspan="7" align="center">No records found.
                                                    </td>
                                                </tr>
                                                </table>
                                            </FooterTemplate>
                                        </asp:Repeater>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

