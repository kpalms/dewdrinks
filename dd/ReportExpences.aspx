﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="ReportExpences.aspx.cs" Inherits="dd_ReportExpences" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                           Report Expences
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-lg-4  col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtfromdate" placeholder="Select From Date" class="form-control" runat="server" required></asp:TextBox>
                                <ajaxToolkit:CalendarExtender ID="CalendarExtender1" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                TargetControlID="txtfromdate"></ajaxToolkit:CalendarExtender>
                                     </div>
                            </div>
                            <div class="col-lg-4  col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txttodate" placeholder="Select To Date" class="form-control" runat="server" required></asp:TextBox>
                                 <ajaxToolkit:CalendarExtender ID="CalendarExtender2" DefaultView="Days" runat="server" Format="dd/MM/yyyy" PopupButtonID="txtto"
                                TargetControlID="txttodate"></ajaxToolkit:CalendarExtender>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Load" OnClick="btnadd_Click" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12 col-md-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                            <asp:BoundField DataField="ItemName" HeaderText="ItemName" />
                                            <asp:BoundField DataField="Qty" HeaderText="Qty" />
                                            <asp:BoundField DataField="Amount" HeaderText="Amount" />
                                             <asp:BoundField DataField="Notes" HeaderText="Notes" />
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 alert alert-info">
                            <b>Total Amount :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

