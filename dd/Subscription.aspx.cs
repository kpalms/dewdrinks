﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Subscription : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            txtdate.Text = CRUD.ReturnIndianDate();
            DataTable dt = CRUD.selectCustomerName();
            ddlcustomer.DataSource = dt;
            ddlcustomer.DataTextField = "FullName";
            ddlcustomer.DataValueField = "CustomerId";
            ddlcustomer.DataBind();
            ddlcustomer.Items.Insert(0, new ListItem("-- Select Customer --", "0"));
            ddlcustomer.SelectedIndex = 0;
            SelectSubscription();
        }
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        if (btnadd.Text == "Save")
        {
            string str = CRUD.ReturnOneValue("select CustId from Subscription where CustId='" + Convert.ToInt32(ddlcustomer.SelectedValue) + "' and Active=1");
            if (str == "")
            {
                CRUD.InsertSubscription(txtdate.Text,Convert.ToInt32(ddlcustomer.SelectedValue), Convert.ToInt32(txtqty.Text));
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Subscription Added Successfully !.');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : This Customer is Active !.');", true);
            }
        }
        if (btnadd.Text == "Update")
        {
            CRUD.UpdateSubscription(txtdate.Text, Convert.ToInt32(ddlcustomer.SelectedValue), Convert.ToInt32(txtqty.Text), id);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Subscription Updated Successfully !.');", true);
        }
        btnadd.Text = "Save";
        txtqty.Text = "";
        txtdate.Text = CRUD.ReturnIndianDate();
        ddlcustomer.SelectedValue = "0";
        SelectSubscription();
        ddlcustomer.Enabled = true;
    }

    private void SelectSubscription()
    {
        DataTable dt = CRUD.SelectSubscription(1);
        rptcourse.DataSource = dt;
        rptcourse.DataBind();
        if (dt.Rows.Count == 0)
        {
            Control FooterTemplate = rptcourse.Controls[rptcourse.Controls.Count - 1].Controls[0];
            FooterTemplate.FindControl("trEmpty").Visible = true;
        }
    }
    protected void rptcourse_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        id = Convert.ToInt32(e.CommandArgument.ToString());
        if (e.CommandName == "Edit")
        {
            btnadd.Text = "Update";
            txtdate.Text = ((Label)e.Item.FindControl("lbldate")).Text;
            
            ddlcustomer.SelectedValue =((Label)e.Item.FindControl("lblcustid")).Text;
            txtqty.Text = ((Label)e.Item.FindControl("lblqty")).Text;
            ddlcustomer.Enabled = false;
            ddlcustomer.CssClass = "form-control";
        }
        if (e.CommandName == "Delete")
        {
            CRUD.DeleteSubscription(id,0);
            SelectSubscription();
            btnadd.Text = "Save";
        }
    }
    protected void ddlcustomer_SelectedIndexChanged(object sender, EventArgs e)
    {
        int id = Convert.ToInt32(ddlcustomer.SelectedValue.ToString());
        DataTable dt = CRUD.selectCustomerDetailsById(id);
        if (dt.Rows.Count > 0)
        {

            lblCustomerDetails.Text = dt.Rows[0]["FirstName"].ToString() + "  " + dt.Rows[0]["LastName"].ToString() + "  " + dt.Rows[0]["Gender"].ToString() + "  " + dt.Rows[0]["MobileNo"].ToString() + "  " + dt.Rows[0]["Hint"].ToString() + "  " + dt.Rows[0]["Company"].ToString() + "  " + dt.Rows[0]["Appartment"].ToString() + "  " + dt.Rows[0]["Flatno"].ToString() + "  " + dt.Rows[0]["Address"].ToString() + "  " + dt.Rows[0]["AreaName"].ToString() + "  " + dt.Rows[0]["CityName"].ToString();
        }

    }


}