﻿<%@ Page Title="" Language="C#" MasterPageFile="~/dd/MasterPage.master" AutoEventWireup="true" CodeFile="Customer.aspx.cs" Inherits="dd_Customer" %>
<%@ Register TagPrefix="Ajaxified" Assembly="Ajaxified" Namespace="Ajaxified" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Customer Registration
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">


<div class="col-lg-3 col-md-3">
<div class="form-group">      
                      
<asp:TextBox ID="txtmobileno" onkeypress="return isNumber(event)" AutoPostBack="true" placeholder="Enter Mobile No" class="form-control" runat="server" MaxLength="10" required></asp:TextBox>

<asp:AutoCompleteExtender ServicePath="WebService.asmx" ServiceMethod="GetMobileNo" MinimumPrefixLength="1"
CompletionInterval="10" EnableCaching="false" CompletionSetCount="1" TargetControlID="txtmobileno"
ID="AutoCompleteExtender1" runat="server" FirstRowSelected="false">
</asp:AutoCompleteExtender>
</div>
</div>


                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtFirstName" placeholder="Enter First Name" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtLastName" placeholder="Enter Last Name" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>



                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txtemailid" placeholder="Enter Email Id" TextMode="Email" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:RadioButtonList ID="rdgender" CssClass="RadioButtonWidth" runat="server" RepeatColumns="2">
                                        <asp:ListItem Value="0">Male</asp:ListItem>
                                        <asp:ListItem Value="1">Female</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:TextBox ID="txthindt" placeholder="Enter Hint" class="form-control" runat="server"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlcity" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlcity_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3">
                                <div class="form-group">
                                    <asp:DropDownList ID="ddlarea" class="form-control" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlarea_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                            </div>

                        </div>
                        <div class="row">

                            <div class="col-lg-4 col-md-4">
                                <asp:TextBox ID="txtCompanyName" placeholder="Enter Company Name " class="form-control" runat="server"></asp:TextBox>

                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtAppartmentName" placeholder="Enter Appartment Name" class="form-control" runat="server" ></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtFlatNo"   placeholder="Enter FlatNo" class="form-control" runat="server" ></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtaddress" TextMode="MultiLine" placeholder="Enter Address" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtpassword" placeholder="Enter Password" class="form-control" runat="server" TextMode="Password" required></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                    <asp:TextBox ID="txtretypepassword" TextMode="Password" placeholder="Enter ReType Password" class="form-control" runat="server" required></asp:TextBox>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                                ControlToValidate="txtmobileno" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                                ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>
                            <asp:RequiredFieldValidator ID="ReqiredFieldValidator1" runat="server" ControlToValidate="rdgender" ForeColor="Red" ErrorMessage="Select Gender!" ValidationGroup="g1"> </asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="comparePasswords" runat="server" ForeColor="Red" ControlToCompare="txtpassword" ControlToValidate="txtretypepassword"  ValidationGroup="g1" ErrorMessage="Both passwords do not match !" Display="Dynamic" />
                            <asp:RequiredFieldValidator ID="rfv1" ValidationGroup="g1" runat="server" ControlToValidate="ddlcity" InitialValue="0" ForeColor="Red" ErrorMessage="please select City"></asp:RequiredFieldValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="g1" runat="server" ControlToValidate="ddlarea" InitialValue="0" ForeColor="Red" ErrorMessage="please select Area"></asp:RequiredFieldValidator>
                        </div>

                        <div class="col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4">
                            <div class="form-group">
                                <asp:Button ID="btnadd" runat="server" ValidationGroup="g1" class="btn  btn-block btn-primary" OnClick="btnadd_Click" Text="Registration" />

                            </div>
                        </div>
                    </div>

                </div>
            </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

   <%-- <asp:TextBox ID="txtmo" runat="server" ></asp:TextBox>--%>

</asp:Content>

