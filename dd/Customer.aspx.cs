﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_Customer : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            DataTable dt = CRUD.SelectCity();
            ddlcity.DataSource = dt;
            ddlcity.DataTextField = "CityName";
            ddlcity.DataValueField = "CityId";
            ddlcity.DataBind();
            ddlcity.Items.Insert(0, new ListItem("-- Select City --", "0"));
            ddlcity.SelectedIndex = 0;
            if (Request.QueryString["mn"] != null)
            {
                txtmobileno.Text = Request.QueryString["mn"];
            }

        }
    }

    protected void ddlcity_SelectedIndexChanged(object sender, EventArgs e)
    {
        SelectArea();
        ddlarea.Focus();
    }

    private void SelectArea()
    {
        DataTable dt = CRUD.Selectarea(Convert.ToInt32(ddlcity.SelectedValue));
        ddlarea.DataSource = dt;
        ddlarea.DataTextField = "AreaName";
        ddlarea.DataValueField = "AreaId";
        ddlarea.DataBind();
        ddlarea.Items.Insert(0, new ListItem("-- Select Area --", "0"));
        ddlarea.SelectedIndex = 0;
       
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {

        string str = CRUD.ReturnOneValue("select MobileNo from Customer where MobileNo='" + txtmobileno.Text + "'");
        if (str != txtmobileno.Text)
        {
            CRUD.Insertcustomer(Convert.ToDateTime(DateTime.Now), txtFirstName.Text, txtLastName.Text, Convert.ToInt32(rdgender.SelectedValue), txtmobileno.Text, txtemailid.Text, txtpassword.Text, txthindt.Text, txtCompanyName.Text, txtFlatNo.Text, txtAppartmentName.Text, txtaddress.Text, Convert.ToInt32(ddlcity.SelectedValue), Convert.ToInt32(ddlarea.SelectedValue));
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Customer Added Successfully !.');", true);
            txtmobileno.Focus();
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Customer is Already Exists !.');", true);
        }

        txtaddress.Text = "";
        txtAppartmentName.Text = "";
        txtCompanyName.Text = "";
        txtemailid.Text = "";
        txtFirstName.Text = "";
        txtFlatNo.Text = "";
        txthindt.Text = "";
        txtLastName.Text = "";
        txtmobileno.Text = "";
        txtpassword.Text = "";
        txtretypepassword.Text = "";
        ddlarea.SelectedValue = "0";
        ddlcity.SelectedValue = "0";
        rdgender.SelectedValue = "1";
    }




    protected void ddlarea_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtCompanyName.Focus();
    }

  
}