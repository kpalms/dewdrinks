﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_UpdatePendingDetails : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        txtdate.Text = CRUD.ReturnIndianDate();
    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
        
        string subid = CRUD.ReturnOneValue("select SubscriptionId from Subscription where CustId=" + Convert.ToInt32(Request.QueryString["Id"]));

        if (subid=="")
        {
            CRUD.InsertPendingDetails(1, 0, Convert.ToDouble("60.00"), Convert.ToInt32(Request.QueryString["Id"]), 1, "Delivered",0,"",0,Convert.ToInt32(txtremaingincan.Text),Convert.ToDecimal(txtremaingamt.Text),txtdate.Text);
        }
        else
        {
            CRUD.InsertPendingDetails(1, 0, Convert.ToDouble("60.00"), Convert.ToInt32(Request.QueryString["Id"]), 1, "Delivered", 0, "", Convert.ToInt32(subid), Convert.ToInt32(txtremaingincan.Text), Convert.ToDecimal(txtremaingamt.Text), txtdate.Text);
        }
       
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification :Pending Details Updated Successfully !.');", true);

        System.Web.UI.ScriptManager.RegisterClientScriptBlock(btnadd, typeof(Button), "Script", "refreshAndClose();", true);
    }
}