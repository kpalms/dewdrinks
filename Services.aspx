﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Services.aspx.cs" Inherits="Services" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-12 col-sm-12">
                <div class="courseArchive_content">
                    <div class="row">
                        <div class="col-lg-12 col-12 col-sm-12">
                            <div class="single_blog_archive wow fadeInUp">
                                <br />
                                <br />
                                <h3 class="title_two" style="color: #056ee3">Services</h3>
                                <h3 class="title_two" style="color: #056ee3">Mineral Water supplier</h3>
                                <p style="text-align: justify; font-size: large;">
                                    Dew Drinks is a leading supplier of Mineral Water in pune city.
Portable water being one of the most Critical needs of mankind, next to oxyen,
we have triggered our journey to deliver pure and hyginic drinking water at the doorstep 
of every pune resident at affordable price, quality and timely service. Though water supply from 
local government is available in certain parts of pune, it is generally not considered safe enough 
to drink until it undergoes several filtering and processing steps. Indeed there are multiple filtered
water suppliers who deliver unknown labels, charge hefty price with poor quality and delivery service. 
There are also few suppliers who deliver branded mineral water at a very high price thus making
it unaffordable for daily household usage and even for business. We are piloting our service to 
individual and business in pune localities . Stay tuned and get ready to quench your thrist for a safe, great-tasting and pure-water
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

