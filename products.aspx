﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="products.aspx.cs" Inherits="products" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="demo/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="demo/css/style.css" rel="stylesheet" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <section id="courseArchive">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 col-sm-12">
                    <div class="courseArchive_content">
                        <div class="row">
                            <div class="col-lg-12 col-12 col-sm-12">
                                <div class="single_blog_archive wow fadeInUp">
                                    <div style="color: Green; text-align: center">Our Products</div>
                                    <div class="wrap">
                                        <div class="main">
                                            <div class="content">
                                                <div class="section group">
                                                    <div class="grid_1_of_4 images_1_of_4">
                                                        <a href="#">
                                                            <img src="demo/images/dew_drinks.jpg" alt="" /></a>
                                                        <h2>Dew Drinks </h2>
                                                        <p>Dew Drinks Mineral water at doorsteps</p>
                                                        <p><span class="strike">Rs 80</span><span class="price">Rs 60</span></p>
                                                        <div class="button"><span>
                                                            <img src="demo/images/cart.jpg" alt="" /><a href="CheckOut.aspx" class="cart-button">Place Order</a></span> </div>
                                                        <div class="button"><span><a href="ProductDetails.aspx" class="details">Details</a></span></div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

