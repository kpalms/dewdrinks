﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script>
        function OrderOnline() {
            alert("Few Days Remaining call 77 35 35 35 30");
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <br />
    <br />
    <br />
    <br />
    <h3 class="title_two" style="color: #056ee3">IT'S NOT BOTTLED...<br />
        IT'S BETTER!</h3>
    <h3 class="title_two" style="color: #056ee3">FIRST TIME IN PUNE PACKAGED DRINKING WATER DELIVERED TO YOUR DOORSTEP .</h3>
    <h3 class="title_two" style="color: #056ee3">Never guess what you are drinking again</h3>
    <section class="download" id="download2" style="background-image: url(img/dew-drinks-ordernow.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center wp4">
                    <a href="" class="download-btn" onclick="OrderOnline();">Order Online</a>
                </div>
            </div>
        </div>
    </section>
    <a href="#" class="blog_img">
        <img src="img/Dew Drinks water jars.png" />
    </a>
    <section class="download" id="download" style="background-image: url(img/dew-drinks-background.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center wp4">
                    <h1>Call Now</h1>
                    <a href="tel:7735353530" class="download-btn">77 35 35 35 30
                    </a>
                </div>
            </div>
        </div>
    </section>
    <section class="download" id="Section1">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center wp4">
                    <h1>How it works</h1>
                    <a href="#" class="download-btn">Select Product
                    </a>
                </div>
                <div class="col-md-12 text-center wp4">

                    <a href="#" class="download-btn">Place Your Order
                    </a>
                </div>
                <div class="col-md-12 text-center wp4">
                    <a href="#" class="download-btn">Make Payment
                    </a>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

