﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class Login : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
  
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnlogin_Click(object sender, EventArgs e)
    {
       DataTable dt= CRUD.CheckLogin(txtmobileno.Text, txtpass.Text);
        if (dt.Rows.Count > 0)
        {
            if (dt.Rows[0]["Role"].ToString() == "Admin")
            {
                Session["id"] = dt.Rows[0]["WorkerId"].ToString();
                Response.Redirect("dd/DashBoard.aspx");
            }
            else
            {
                Session["id"] = dt.Rows[0]["WorkerId"].ToString();
                Session["Name"] = dt.Rows[0]["Name"].ToString();
                Response.Redirect("worker/DeliveryCard.aspx");
            }
            
        }
        else
        {
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : User Name and Password is Wrong !.');", true);
            txtmobileno.Text = "";
            txtpass.Text = "";
        }
    }
}