﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class suggestion : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnadd_Click(object sender, EventArgs e)
    {
            CRUD.InsertSuggestion(Convert.ToDateTime(DateTime.Now), txtfullname.Text, txtmobileno.Text, txtdescripation.Text);
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Thanky You for Your Suggestion !.');", true);
        txtfullname.Text = "";
        txtdescripation.Text = "";
        txtmobileno.Text = "";
    }
}