﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;
using System.Data;
   public class DewDrinks_CRUD
    {

       public String ReturnIndianDate()
       {
         return  DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy");
       }

       public String ReturnIndianTime()
       {
           return DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("hh:mm tt");
       }

       public String ReturnIndianDateTime()
       {
           return DateTime.UtcNow.AddHours(5).AddMinutes(30).ToString("dd/MM/yyyy hh:mm:ss");
       }

       public string ReturnOneValue(string str)
       {
           MySql.Data.MySqlClient.MySqlConnection conn = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon);
           MySql.Data.MySqlClient.MySqlCommand command = new MySql.Data.MySqlClient.MySqlCommand(str);
           command.Connection = conn;
           conn.Open();
           string strr = Convert.ToString(command.ExecuteScalar());
           conn.Close();
           return strr;
       }

       public DataTable CheckLogin(string Mb, string Pass)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "CALL CheckLogin('" + Pass + "','" + Mb + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
            }
        }

       /// <summary>
       /// City CRUD
       /// </summary>
       /// <param name="CityName"></param>

        public void InsertCity(string CityName)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertCity(@CityName)";
                        cmd.Parameters.AddWithValue("@CityName", CityName);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                       
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateCity(string CityName, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateCity(@CityName,@Id)";
                        cmd.Parameters.AddWithValue("@CityName", CityName);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteCity(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteCity(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectCity()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectCity";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        /// <summary>
        /// AREA CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void Insertarea(string AreaName, int CityId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call Insertarea(@CityId,@AreaName)";
                        cmd.Parameters.AddWithValue("@CityId", CityId);
                        cmd.Parameters.AddWithValue("@AreaName", AreaName);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void Updatearea(string AreaName, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call Updatearea(@AreaName,@Id)";
                        cmd.Parameters.AddWithValue("@AreaName", AreaName);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void Deletearea(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call Deletearea(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable Selectarea(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call Selectarea('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText,con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Item CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertItem(string ItemNames, string Rate)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertItem(@Rate,@ItemNames)";
                        cmd.Parameters.AddWithValue("@ItemNames", ItemNames);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdatItem(string ItemNames, string Rate, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatItem(@ItemNames,@Rate,@Id)";
                        cmd.Parameters.AddWithValue("@ItemNames", ItemNames);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeletItem(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeletItem(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectItem()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectItem";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectItemName()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectItemName";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }
        /// <summary>
        /// PaymentMode CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertPaymentMode(string PaymentMode)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPaymentMode(@PaymentMode)";
                        cmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdatePaymentMode(string PaymentMode, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatePaymentMode(@PaymentMode,@Id)";
                        cmd.Parameters.AddWithValue("@PaymentMode", PaymentMode);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeletePaymentMode(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeletePaymentMode(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectPaymentMode()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectPaymentMode()";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        /// <summary>
        /// VideoGallary CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertVideoGallary(string Title, string Descrption, string URL)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertVideoGallary(@Title,@Descrption,@URL)";
                        cmd.Parameters.AddWithValue("@Title", Title);
                        cmd.Parameters.AddWithValue("@Descrption", Descrption);
                        cmd.Parameters.AddWithValue("@URL", URL);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateVideoGallary(string Title, string Descrption, string URL, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateVideoGallary(@Title,@Descrption,@URL,@Id)";
                        cmd.Parameters.AddWithValue("@Title", Title);
                        cmd.Parameters.AddWithValue("@Descrption", Descrption);
                        cmd.Parameters.AddWithValue("@URL", URL);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteVideoGallary(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteVideoGallary(@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectVideoGallary()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectVideoGallary";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Suggestion CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertSuggestion(DateTime Date, string FullName, string MobileNo, string Comments)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertSuggestion(@Date,@FullName,@MobileNo,@Comments)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@FullName", FullName);
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@Comments", Comments);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateSuggestion(DateTime Date, string FullName, string MobileNo, string Comments, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateSuggestion(@Date,@FullName,@MobileNo,@Comments,@Id)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@FullName", FullName);
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@Comments", Comments);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void Deletesuggestion(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call Deletesuggestion(@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSuggestion()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSuggestion";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        con.Close();
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        /// <summary>
        /// ImageGallary CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertImageGallary(string Title, string Descrption, string ImageName)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertImageGallary(@Title,@Descrption,@ImageName)";
                        cmd.Parameters.AddWithValue("@Title", Title);
                        cmd.Parameters.AddWithValue("@Descrption", Descrption);
                        cmd.Parameters.AddWithValue("@ImageName", ImageName);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateImageGallary(string Title, string Descrption, string ImageName, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateImageGallary(@Title,@Descrption,@ImageName,@Id)";
                        cmd.Parameters.AddWithValue("@Title", Title);
                        cmd.Parameters.AddWithValue("@Descrption", Descrption);
                        cmd.Parameters.AddWithValue("@ImageName", ImageName);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteImageGallary(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteImageGallary(@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectImageGallary()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectImageGallary";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public string GetImageName()
        {
            int length = 4;
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return (res + DateTime.Now.ToString("ddMMyyyyHHmmss")).ToString();
        }

        /// <summary>
        /// Customer CRUD
        /// </summary>
        /// <param name="CityName"></param>
        public void Insertcustomer(DateTime Date, string FirstName, string LastName, int Gender, string MobileNo, string Email, string Password, string Hint, string Company, string FlatNo, string Appartment, string Address, int CityId, int AreaId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call Insertcustomer(@Date,@FirstName,@LastName,@Gender,@MobileNo,@Email,@Password,@Hint,@Company,@FlatNo,@Appartment,@Address,@CityId,@AreaId)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@FirstName", FirstName);
                        cmd.Parameters.AddWithValue("@LastName", LastName);
                        cmd.Parameters.AddWithValue("@Gender", Gender);
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@Email", Email);
                        cmd.Parameters.AddWithValue("@Password", Password);
                        cmd.Parameters.AddWithValue("@Hint", Hint);
                        cmd.Parameters.AddWithValue("@Company", Company);
                        cmd.Parameters.AddWithValue("@FlatNo", FlatNo);
                        cmd.Parameters.AddWithValue("@Appartment", Appartment);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@CityId", CityId);
                        cmd.Parameters.AddWithValue("@AreaId", AreaId);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }
   
        public DataTable SelectCustomer(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectCustomer('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectCustomerByMb(string MNo)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectCustomerByMb('" + MNo + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectCustomerByFirstName(string FName)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectCustomerByFirstName('" + FName + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectCustomerByLastName(string LName)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectCustomerByLastName('" + LName + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataTable SelectCustomerById(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectCustomerById('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void ActiveCustomer(int Id, int AId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call ActiveCustomer(@AId,@Id)";
                        cmd.Parameters.AddWithValue("@AId", AId);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateCustomer(string FirstName, string LastName, string MobileNo, string Email, string Hint, int Gender, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateCustomer(@Id,@FirstName,@LastName,@MobileNo,@Email,@Hint,@Gender)";
                        cmd.Parameters.AddWithValue("@FirstName", FirstName);
                        cmd.Parameters.AddWithValue("@LastName", LastName);
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@Email", Email);
                        cmd.Parameters.AddWithValue("@Hint", Hint);
                        cmd.Parameters.AddWithValue("@Gender", Gender);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectCustomerName()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectCustomerName";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectWorkerName()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectWorkerName";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSubscription(int IsActive)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSubscription('" + IsActive + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSubCustomer()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSubCustomer";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSubCustomerByMb(string MNo)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSubCustomerByMb('" + MNo + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSubCustomerByFirstName(string FName)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSubCustomerByFirstName('" + FName + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSubCustomerByLastName(string LName)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSubCustomerByLastName('" + LName + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteSubscription(int Id, int IsActive)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteSubscription(@Id,@IsActive)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsActive", IsActive);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateSubscription(string Date, int CustId, int Qty, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateSubscription(@Date,@Id,@Qty,@CustId)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@CustId", CustId);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void InsertSubscription(string Date, int Id, int Qty)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertSubscription(@Date,@Id,@Qty)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataSet selectDeliveryCard(string stuts, string ddate)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectDeliveryCard('" + stuts + "','" + ddate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectNewDeliveryCard(string stuts, string ddate)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectNewDeliveryCard('" + stuts + "','" + ddate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectWorkerDeliveryStatus(string stuts, string ddate, int workerid)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectWorkerDeliveryStatus('" + stuts + "','" + ddate + "','" + workerid + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataTable selectOrderById(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectOrderById('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectDeliveryCardByMb(string stuts, string Mb, string ddate)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectDeliveryCardByMb('" + stuts + "','" + Mb + "','" + ddate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectDeliveryCardByFName(string stuts, string FName, string ddate)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectDeliveryCardByFName('" + stuts + "','" + FName + "','" + ddate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectDeliveryCardByLName(string stuts, string LName, string ddate)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectDeliveryCardByLName('" + stuts + "','" + LName + "','" + ddate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataSet selectPendingOrder(string stuts, string ddate)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectPendingOrder('" + stuts + "','" + ddate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectPeningCanAmt(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectPeningCanAmt('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataTable selectPeningCanAmtByOderId(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectPeningCanAmtByOderId('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdatePenCanAmt(int Id, int CollCan, int PenAmt, string deliverynote,int DeliveredCan)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatePenCanAmt(@CollCan,@PenAmt,@Id,@deliverynote,@DeliveredCan)";
                        cmd.Parameters.AddWithValue("@CollCan", CollCan);
                        cmd.Parameters.AddWithValue("@PenAmt", PenAmt);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@deliverynote", deliverynote);
                        cmd.Parameters.AddWithValue("@DeliveredCan", DeliveredCan);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void ChangeStatus(int Id, string stuts)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call ChangeStatus(@stuts,@Id)";
                        cmd.Parameters.AddWithValue("@stuts", stuts);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectOrderHistory(string frmdate, string todate, string stat)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                         cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectOrderHistory('" + frmdate + "','" + todate + "','" + stat + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectOrderHistoryByWorker(string frmdate, string todate, string stat, int wid)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectOrderHistoryByWorker('" + frmdate + "','" + todate + "','" + stat + "','" + wid + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataSet selectOrderHistoryByCustomer(string frmdate, string todate, string stat, int cid)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectOrderHistoryByCustomer('" + frmdate + "','" + todate + "','" + stat + "','" + cid + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataSet selectPendingDetails()
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectPendingDetails";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataSet selectPendingDetailsByCustomer(int CustId)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectPendingDetailsByCustomer('" + CustId + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataTable selectSubscriptionByCustomer(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectSubscriptionByCustomer('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable selectSubscriptionCustomer()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectSubscriptionCustomer";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectSubOrderHistory(int CutId, int SubId)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectSubOrderHistory('" + CutId + "','" + SubId + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectCustomerCanHistory(int CutId)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectCustomerCanHistory('" + CutId + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataSet selectAddressByMb(string MB)
        {
            DataSet dt = new DataSet();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectAddressByMb('" + MB + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        // DateTime Date,
        public void InsertPlaceOrder(int ItemId, int Qty, double Rate, string DeDate, string Time, int CustomerId, int PaymentModeId, string Status, int DeliveryId, string Note, int SubscriptionId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPlaceOrder(@ItemId,@Qty,@Rate,@CDate,@DeDate,@Time,@CustomerId,@PaymentModeId,@Status,@DeliveryId,@Note,@SubscriptionId)";
                        cmd.Parameters.AddWithValue("@ItemId", ItemId);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@CDate", ReturnIndianDateTime());
                        cmd.Parameters.AddWithValue("@DeDate", DeDate);
                        cmd.Parameters.AddWithValue("@Time", Time);
                        cmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        cmd.Parameters.AddWithValue("@PaymentModeId", PaymentModeId);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@DeliveryId", DeliveryId);
                        cmd.Parameters.AddWithValue("@Note", Note);
                        cmd.Parameters.AddWithValue("@SubscriptionId", SubscriptionId);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }
        //DateTime Date,
        public void UpdatePlaceOrder(int ItemId, int Qty, double Rate, string DeDate, string Time, int CustomerId, int PaymentModeId, string Status, int DeliveryId, string Note, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatePlaceOrder(@ItemId,@Qty,@Rate,@CDate,@DeDate,@Time,@CustomerId,@PaymentModeId,@Status,@DeliveryId,@Note,@Id)";
                        cmd.Parameters.AddWithValue("@ItemId", ItemId);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@CDate", ReturnIndianDateTime());
                        cmd.Parameters.AddWithValue("@DeDate", DeDate);
                        cmd.Parameters.AddWithValue("@Time", Time);
                        cmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        cmd.Parameters.AddWithValue("@PaymentModeId", PaymentModeId);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@DeliveryId", DeliveryId);
                        cmd.Parameters.AddWithValue("@Note", Note);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        /// <summary>
        /// Worker CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertWorker(string Name, string MiddleName, string LastName, string PhoneNo, string Mobile, string Email, string Country, string State, string City, string Address, string Password, decimal Salary, string Role)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertWorker(@Name,@MiddleName,@LastName,@PhoneNo,@Mobile,@Email,@Salary,@Country,@State,@City,@Address,@Role,@Password)";
                        cmd.Parameters.AddWithValue("@Name", Name);
                        cmd.Parameters.AddWithValue("@MiddleName", MiddleName);
                        cmd.Parameters.AddWithValue("@LastName", LastName);
                        cmd.Parameters.AddWithValue("@PhoneNo", PhoneNo);
                        cmd.Parameters.AddWithValue("@Mobile", Mobile);
                        cmd.Parameters.AddWithValue("@Email", Email);
                        cmd.Parameters.AddWithValue("@Country", Country);
                        cmd.Parameters.AddWithValue("@State", State);
                        cmd.Parameters.AddWithValue("@City", City);
                        cmd.Parameters.AddWithValue("@Password", Password);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@Salary", Salary);
                        cmd.Parameters.AddWithValue("@Role", Role);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateWorker(string Name, string MiddleName, string LastName, string PhoneNo, string Mobile, string Email, string Country, string State, string City, string Address, string Password, decimal Salary, string Role, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateWorker(@Name,@MiddleName,@LastName,@PhoneNo,@Mobile,@Email,@Salary,@Country,@State,@City,@Address,@Role,@Password,@Id)";
                                                                                            
                                                                                            cmd.Parameters.AddWithValue("@Name", Name);
                                                                                            cmd.Parameters.AddWithValue("@MiddleName", MiddleName);
                                                                                            cmd.Parameters.AddWithValue("@LastName", LastName);
                                                                                            cmd.Parameters.AddWithValue("@PhoneNo", PhoneNo);
                                                                                            cmd.Parameters.AddWithValue("@Mobile", Mobile);
                                                                                            cmd.Parameters.AddWithValue("@Email", Email);
                                                                                            cmd.Parameters.AddWithValue("@Country", Country);
                                                                                            cmd.Parameters.AddWithValue("@State", State);
                                                                                            cmd.Parameters.AddWithValue("@City", City);
                                                                                            cmd.Parameters.AddWithValue("@Password", Password);
                                                                                            cmd.Parameters.AddWithValue("@Address", Address);
                                                                                            cmd.Parameters.AddWithValue("@Salary", Salary);
                                                                                            cmd.Parameters.AddWithValue("@Role", Role);
                                                                                            cmd.Parameters.AddWithValue("@Id", Id);
                                                                                            con.Open();
                                                                                            cmd.ExecuteNonQuery();
                                                                                            con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteWorker(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteWorker('" + IsDelete + "','" + Id + "')";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectWorker()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectWorker";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


       


        public void InsertContactUs(DateTime DateTime, string FullName, string MobileNo,string EmailId, string Description)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertContactUs(@DateTime,@FullName,@MobileNo,@EmailId,@Description)";
                        cmd.Parameters.AddWithValue("@DateTime", DateTime);
                        cmd.Parameters.AddWithValue("@FullName", FullName);
                        cmd.Parameters.AddWithValue("@MobileNo", MobileNo);
                        cmd.Parameters.AddWithValue("@EmailId", EmailId);
                        cmd.Parameters.AddWithValue("@Description", Description);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteContactUs(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteContactUs(@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectContactUs()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectContactUs";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        /// <summary>
        /// Item CRUD
        /// </summary>
        /// <param name="CityName"></param>

        public void InsertPItem(string ItemNames, decimal Rate)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPItem(@Rate,@ItemNames)";
                        cmd.Parameters.AddWithValue("@ItemNames", ItemNames);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdatPItem(string ItemNames, decimal Rate, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatPItem(@ItemNames,@Rate,@Id)";
                        cmd.Parameters.AddWithValue("@ItemNames", ItemNames);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeletPItem(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeletPItem(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectPItem()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectPItem";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectPItemName()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectPItemName";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }







        public void InsertExpences(string Date, string ItemName, int Qty, decimal Amount, string Notes)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertExpences(@Date,@ItemName,@Qty,@Amount,@Notes)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@ItemName", ItemName);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@Amount", Amount);
                        cmd.Parameters.AddWithValue("@Notes", Notes);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateExpences(string Date, string ItemName, int Qty, decimal Amount, string Notes, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateExpences(@Date,@ItemName,@Qty,@Amount,@Notes,@Id)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@ItemName", ItemName);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@Amount", Amount);
                        cmd.Parameters.AddWithValue("@Notes", Notes);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteExpences(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteExpences(@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectExpences(string dDate)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectExpences('" + dDate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable ReportExpences(string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call ReportExpences('" + FromDate + "','" + ToDate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        public void InsertUnit(string UnitName)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertUnit(@UnitName)";
                        cmd.Parameters.AddWithValue("@UnitName", UnitName);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateUnit(string UnitName, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateUnit(@UnitName,@Id)";
                        cmd.Parameters.AddWithValue("@UnitName", UnitName);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteUnit(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteUnit(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectUnit()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectUnit()";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }




        public void InsertSupplier(string FullName, string ContactPerson, string EmailId, string MobileNo1, string MobileNo2, string City, string Note, string Address)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertSupplier(@FullName,@ContactPerson,@EmailId,@MobileNo1,@MobileNo2,@City,@Note,@Address)";
                        cmd.Parameters.AddWithValue("@FullName", FullName);
                        cmd.Parameters.AddWithValue("@ContactPerson", ContactPerson);
                        cmd.Parameters.AddWithValue("@EmailId", EmailId);
                        cmd.Parameters.AddWithValue("@MobileNo1", MobileNo1);
                        cmd.Parameters.AddWithValue("@MobileNo2", MobileNo2);
                        cmd.Parameters.AddWithValue("@City", City);
                        cmd.Parameters.AddWithValue("@Note", Note);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateSupplier(string FullName, string ContactPerson, string EmailId, string MobileNo1, string MobileNo2, string City, string Note, string Address, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateSupplier(@FullName,@ContactPerson,@EmailId,@MobileNo1,@MobileNo2,@City,@Note,@Address,@Id)";
                        cmd.Parameters.AddWithValue("@FullName", FullName);
                        cmd.Parameters.AddWithValue("@ContactPerson", ContactPerson);
                        cmd.Parameters.AddWithValue("@EmailId", EmailId);
                        cmd.Parameters.AddWithValue("@MobileNo1", MobileNo1);
                        cmd.Parameters.AddWithValue("@MobileNo2", MobileNo2);
                        cmd.Parameters.AddWithValue("@City", City);
                        cmd.Parameters.AddWithValue("@Note", Note);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeletSupplier(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeletSupplier(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSupplier()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSupplier()";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectSupplierName()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectSupplierName()";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public void InsertPurchaseDetails(int ItemId, int Qty, int UnitId, decimal Rate, string Notes)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPurchaseDetails(@ItemId,@Qty,@UnitId,@Rate,@Notes)";
                        cmd.Parameters.AddWithValue("@ItemId", ItemId);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@UnitId", UnitId);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@Notes", Notes);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdatePurchaseDetails(int ItemId, int Qty, int UnitId, decimal Rate, string Notes, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatePurchaseDetails(@ItemId,@Qty,@UnitId,@Rate,@Notes,@Id)";
                        cmd.Parameters.AddWithValue("@ItemId", ItemId);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@UnitId", UnitId);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@Notes", Notes);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeletPurchaseDetails(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeletPurchaseDetails(@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectPurchaseDetails()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectPurchaseDetails";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public void InsertPurchase(string Date, int InVoiceNo, int SupplierId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPurchase(@Date,@InVoiceNo,@SupplierId)";
                        cmd.Parameters.AddWithValue("@Date", ReturnIndianDateTime());
                        cmd.Parameters.AddWithValue("@InVoiceNo", InVoiceNo);
                        cmd.Parameters.AddWithValue("@SupplierId", SupplierId);
                       
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }
       
       public DataTable SelectPurchase()
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectPurchase";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdatePurchase(string Date, int InVoiceNo, int SupplierId, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdatePurchase(@Date,@InVoiceNo,@SupplierId,@Id)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@InVoiceNo", InVoiceNo);
                        cmd.Parameters.AddWithValue("@SupplierId", SupplierId);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void EditPurchase(int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call EditPurchase(@Id)";
                        
                        cmd.Parameters.AddWithValue("@Id", Id);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public DataTable ReportPurchase(string FromDate, string ToDate)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call ReportPurchase('" + FromDate + "','" + ToDate + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectPurchaseDetailsById(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectPurchaseDetailsById('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public void InsertOrderDeliveredDetail(int WorkerId, int OrderId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertOrderDeliveredDetail(@WorkerId,@OrderId,@DDeliveredTime)";
                        cmd.Parameters.AddWithValue("@WorkerId", WorkerId);
                        cmd.Parameters.AddWithValue("@OrderId", OrderId);
                        string str = ReturnIndianDateTime();
                        cmd.Parameters.AddWithValue("@DDeliveredTime", ReturnIndianDateTime());
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }


        public void InsertPaymentHistory(int OrderId, decimal PendingAmount)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPaymentHistory(@OrderId,@SDeliveryDateTime,@PendingAmount)";
                        cmd.Parameters.AddWithValue("@OrderId", OrderId);
                        cmd.Parameters.AddWithValue("@SDeliveryDateTime", ReturnIndianDateTime());
                        cmd.Parameters.AddWithValue("@PendingAmount", PendingAmount);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void InsertUpdatePaymentHistory(int Id, decimal PaidAmount, int CollCan)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertUpdatePaymentHistory(@Id,@SDeliveryDateTime,@PaidAmount,@CollCan)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@SDeliveryDateTime", ReturnIndianDateTime());
                        cmd.Parameters.AddWithValue("@PaidAmount", PaidAmount);
                        cmd.Parameters.AddWithValue("@CollCan", CollCan);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        public DataTable SelectAddress(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectAddress('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void InsertAddress(DateTime Date, int CustomerId, string Company, string FlatNo, string Appartment, string Address, int CityId, int AreaId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertAddress(@Date,@CustomerId,@Company,@FlatNo,@Appartment,@Address,@CityId,@AreaId)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        cmd.Parameters.AddWithValue("@Company", Company);
                        cmd.Parameters.AddWithValue("@FlatNo", FlatNo);
                        cmd.Parameters.AddWithValue("@Appartment", Appartment);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@CityId", CityId);
                        cmd.Parameters.AddWithValue("@AreaId", AreaId);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void UpdateAddress(DateTime Date, int Id, string Company, string FlatNo, string Appartment, string Address, int CityId, int AreaId)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call UpdateAddress(@Date,@Id,@Company,@FlatNo,@Appartment,@Address,@CityId,@AreaId)";
                        cmd.Parameters.AddWithValue("@Date", Date);
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@Company", Company);
                        cmd.Parameters.AddWithValue("@FlatNo", FlatNo);
                        cmd.Parameters.AddWithValue("@Appartment", Appartment);
                        cmd.Parameters.AddWithValue("@Address", Address);
                        cmd.Parameters.AddWithValue("@CityId", CityId);
                        cmd.Parameters.AddWithValue("@AreaId", AreaId);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {

                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public DataTable SelectAddressById(int Id)
        {
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call SelectAddressById('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        public void DeleteAddress(int IsDelete, int Id)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call DeleteAddress(@IsDelete,@Id)";
                        cmd.Parameters.AddWithValue("@Id", Id);
                        cmd.Parameters.AddWithValue("@IsDelete", IsDelete);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }






       ////+++++++++++++++++++++++++++++++++++++++++++by kundan++++++++++++++++++++++++++++++++++++++++++++++++++++++//



        public DataTable selectCustomerDetailsById(int Id)
        {
      
            DataTable dt = new DataTable();
            using (MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySqlCommand cmd = new MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandType = System.Data.CommandType.StoredProcedure;
                        cmd.CommandText = "call selectCustomerDetailsById('" + Id + "')";
                        MySqlDataAdapter sda = new MySqlDataAdapter(cmd.CommandText, con);
                        sda.Fill(dt);
                        return dt;
                    }
                    catch (Exception ex)
                    {
                        return dt;
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }



        public void UseExcuteNonQuery(string query)
        {
            try
            {
                MySqlConnection con = new MySqlConnection(SqlHelper.SqlCon);
                con.Open();
                MySqlCommand cmd = new MySqlCommand(query, con);
                cmd.ExecuteNonQuery();
                con.Close();
            }
            catch
            {
            }
        }



        public void InsertPendingDetails(int ItemId, int Qty, double Rate, int CustomerId, int PaymentModeId, string Status, int DeliveryId, string Note, int SubscriptionId, int CollCan, decimal PendingAmount,string date)
        {
            using (MySql.Data.MySqlClient.MySqlConnection con = new MySql.Data.MySqlClient.MySqlConnection(SqlHelper.SqlCon))
            {
                using (MySql.Data.MySqlClient.MySqlCommand cmd = new MySql.Data.MySqlClient.MySqlCommand())
                {
                    try
                    {
                        cmd.Connection = con;
                        cmd.CommandText = "call InsertPendingDetails(@ItemId,@Qty,@Rate,@CDate,@Time,@CustomerId,@PaymentModeId,@Status,@DeliveryId,@Note,@SubscriptionId,@CollCan,@PendingAmount)";
                        cmd.Parameters.AddWithValue("@ItemId", ItemId);
                        cmd.Parameters.AddWithValue("@Qty", Qty);
                        cmd.Parameters.AddWithValue("@Rate", Rate);
                        cmd.Parameters.AddWithValue("@CDate", date);
                        cmd.Parameters.AddWithValue("@Time", ReturnIndianTime());
                        cmd.Parameters.AddWithValue("@CustomerId", CustomerId);
                        cmd.Parameters.AddWithValue("@PaymentModeId", PaymentModeId);
                        cmd.Parameters.AddWithValue("@Status", Status);
                        cmd.Parameters.AddWithValue("@DeliveryId", DeliveryId);
                        cmd.Parameters.AddWithValue("@Note", Note);
                        cmd.Parameters.AddWithValue("@SubscriptionId", SubscriptionId);
                        cmd.Parameters.AddWithValue("@CollCan", CollCan);
                        cmd.Parameters.AddWithValue("@PendingAmount", PendingAmount);
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                    catch (Exception ex)
                    {
                    }
                    finally
                    {
                        if (con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }
                }
            }
        }

        

   }






