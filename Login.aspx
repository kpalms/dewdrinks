﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Login.aspx.cs" Inherits="Login" %>

<!DOCTYPE html lang="en">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dew Drinks | Log In</title>
    <link rel="shortcut icon" href="dd/dist/images/ddicon.jpg" type="image/png">
    <link href="dd/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="dd/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <link href="dd/dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="dd/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script src="dd/bower_components/JavaScript.js" type="text/javascript"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">

                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                        ControlToValidate="txtmobileno" ErrorMessage="10 Digit Mobile No" ForeColor="Red" ValidationGroup="g1"
                        ValidationExpression="[0-9]{10}"></asp:RegularExpressionValidator>

                    <div class="login-panel panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title">Dew Drinks Log In</h3>
                        </div>
                        <div class="panel-body">
                            <fieldset>
                                <div class="form-group">
                                    <asp:TextBox ID="txtmobileno" onkeypress="return isNumber(event)" MaxLength="10" class="form-control" placeholder="Enter Mobile No" required runat="server" autofocus></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <asp:TextBox ID="txtpass" class="form-control" TextMode="Password" placeholder="Enter Password" name="password" runat="server" required></asp:TextBox>
                                </div>
                                <asp:Button ID="btnlogin" class="btn btn-lg btn-success btn-block" runat="server" Text="Log In" OnClick="btnlogin_Click" />
                            </fieldset>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
<script src="dd/bower_components/jquery/dist/jquery.min.js"></script>
<script src="dd/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="dd/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="dd/dist/js/sb-admin-2.js"></script>
</html>

