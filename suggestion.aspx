﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="suggestion.aspx.cs" Inherits="suggestion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script src="dd/bower_components/JavaScript.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <section id="courseArchive">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 col-sm-12">
                    <div class="courseArchive_content">
                        <div class="row">
                            <div class="col-lg-12 col-12 col-sm-12">
                                <div class="single_blog_archive wow fadeInUp">
                                    <div style="color: Green; text-align: center">Write us Your Valuable Suggestion</div>
                                    <br />
                                    <div class="form-horizontal">
                                        <asp:TextBox ID="txtfullname" placeholder="Enter Full Name" class="wp-form-control wpcf7-text" runat="server" required></asp:TextBox>
                                        <asp:TextBox ID="txtmobileno" placeholder="Enter Mobile No" onkeypress="return isNumber(event)" MaxLength="10" class="wp-form-control wpcf7-text" runat="server" required></asp:TextBox>

                                        <asp:TextBox ID="txtdescripation" placeholder="What would you like to tell us" TextMode="MultiLine" class="wp-form-control wpcf7-textarea" runat="server" required></asp:TextBox>

                                        <asp:Button ID="btnadd" runat="server" class="btn btn-primary" Text="Submit" OnClick="btnadd_Click" />

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

