﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class dd_CompleteOrder : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            selectDeliveryCard();
        }
    }

    private void selectDeliveryCard()
    {
        DataSet dt = CRUD.selectDeliveryCard("Delivered", CRUD.ReturnIndianDate());
        if (dt.Tables[0].Rows.Count > 0)
        {
            GridView1.DataSource = dt.Tables[0];
            GridView1.DataBind();
        }
        if (dt.Tables[1].Rows.Count > 0)
        {
            lbltotamt.Text = dt.Tables[1].Rows[0]["CollectedAmount"].ToString();
            lbltotcan.Text = dt.Tables[1].Rows[0]["CollectedCan"].ToString();
        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        selectDeliveryCard();
    }

    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "DeleteRow")
        {
            GridViewRow row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
            Label lblqty = (Label)row.FindControl("lblqty");

            
        CRUD.ChangeStatus(Convert.ToInt32(e.CommandArgument.ToString()), "PlaceOrder");
        CRUD.UpdatePenCanAmt(Convert.ToInt32(e.CommandArgument.ToString()), 0, 0, "", Convert.ToInt32(lblqty.Text));
        CRUD.UseExcuteNonQuery("delete from PaymentHistory where OrderId="+Convert.ToInt32(e.CommandArgument.ToString()));
        selectDeliveryCard();
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification : Undo Order Successfully !.');", true);

        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblcustomerid = (e.Row.FindControl("lblcustomerid") as Label);
            Label lblPendingAmt = (e.Row.FindControl("lblPendingAmt") as Label);
            Label lblPendingCan = (e.Row.FindControl("lblPendingCan") as Label);
            DataTable dt = CRUD.selectPeningCanAmt(Convert.ToInt32(lblcustomerid.Text));
            lblPendingAmt.Text = dt.Rows[0]["PendingAmt"].ToString();
            lblPendingCan.Text = dt.Rows[0]["CollectedCan"].ToString();
        }
    }



    protected void btndefault_Click(object sender, EventArgs e)
    {
        selectDeliveryCard();
    }
}