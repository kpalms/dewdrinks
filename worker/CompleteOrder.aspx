﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeFile="CompleteOrder.aspx.cs" Inherits="dd_CompleteOrder" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content=""/>
<meta name="author" content=""/>
<title>Dew Drinks | Worker Delivery Card</title>
<link rel="shortcut icon" href="dd/dist/images/ddicon.jpg" type="image/png"/>
<link href="../dd/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../dd/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
<link href="../dd/dist/css/sb-admin-2.css" rel="stylesheet" />
<link href="../dd/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script src="../dd/bower_components/JavaScript.js" type="text/javascript"></script>
        </head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>




            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                            Complete Order
                        </div>
                    </div>
                    <div class="panel-body">
    

                            <div class="row">
                                <div class="col-lg-2 col-md-2">
                                    <asp:LinkButton ID="btndefault" class="btn btn-info btn-block" runat="server" OnClick="btndefault_Click">Refresh</asp:LinkButton>
                                </div>

                               <div class="col-lg-2 col-md-2">
                                   <a href ="DeliveryCard.aspx" >Delivery Card</a>
                                </div>



                                <div class="col-lg-2 col-md-2">
                                   <b> <asp:Label ID="lblWorkerName" runat="server"></asp:Label></b>
                                   
                                </div>
                                <div class="col-lg-2 col-md-2 col-md-offset-6  col-lg-offset-6">
                                    <a href="../logout.aspx" class="btn btn-primary">Log Out</a>
                                </div>
                            </div>
                            <br />


                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowCommand="GridView1_RowCommand" EmptyDataText="No records Found" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                          <%--  <asp:BoundField DataField="Date" HeaderText="Date" />--%>
<%--                                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                            <asp:TemplateField HeaderText="Qty & ItemName">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblqty" runat="server" Text=' <%#Eval("qty")%>'></asp:Label></b>
                                                    <asp:Label ID="lblItemNames" runat="server" Text=' <%#Eval("ItemNames")%>'></asp:Label>

                                                </ItemTemplate>
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Hint & Address">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                    <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>

                                                    <asp:Label ID="lblqty" runat="server" Text=' <%#Eval("qty")%>'></asp:Label></b>
                                                    <asp:Label ID="lblItemNames" runat="server" Text=' <%#Eval("ItemNames")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                            <asp:TemplateField HeaderText="Delivery DateTime" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                    <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Time Ago">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbltimeago" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="note" HeaderText="PlaceOrderNote" />
                                            
                                            <asp:BoundField DataField="TransactionId" HeaderText="TransactionId" />
                                              <asp:TemplateField HeaderText="TotalAmount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblTotalAmount" runat="server" Text=' <%#Eval("TotalAmount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                              <asp:TemplateField HeaderText="CollectedAmount">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCollectedAmount" runat="server" Text=' <%#Eval("CollectedAmount")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>


                                            <asp:TemplateField HeaderText="PendingAmt">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            

                                            <asp:TemplateField HeaderText="CollectedCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblCollectedCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="PendingCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>




                                            <asp:TemplateField HeaderText="Complete Order">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkordercomp" CommandName="DeleteRow" runat="server" CommandArgument='<%#Eval("OrderId") %>' class="btn btn-primary btn-sm" Text="Raise Confirm" OnClientClick="return confirm('are you sure want to Undo?');">Undo</asp:LinkButton>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 alert col-lg-offset-2 alert-success">
                                <b>Total Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-4 col-lg-offset-1 alert alert-info">
                                <b>Total Collected Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>




 </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
<script src="../dd/bower_components/JavaScript.js"></script>
<script src="../dd/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../dd/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../dd/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../dd/dist/js/sb-admin-2.js"></script>
</html>

