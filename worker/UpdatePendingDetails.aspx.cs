﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
public partial class worker_UpdatePendingDetails : System.Web.UI.Page
{
    DewDrinks_CRUD CRUD = new DewDrinks_CRUD();
    static int id = 0;
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void btnadd_Click(object sender, EventArgs e)
    {
        CRUD.InsertUpdatePaymentHistory(Convert.ToInt32(Request.QueryString["Id"]), Convert.ToDecimal(txtpaidamt.Text), Convert.ToInt32(txtremaingincan.Text));
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Notify", "alert('Notification :Pending Details Updated Successfully !.');", true);

        System.Web.UI.ScriptManager.RegisterClientScriptBlock(btnadd, typeof(Button), "Script", "refreshAndClose();", true);
    }
}