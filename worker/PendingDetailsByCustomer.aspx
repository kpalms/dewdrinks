﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PendingDetailsByCustomer.aspx.cs" Inherits="worker_PendingDetailsByCustomer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content=""/>
<meta name="author" content=""/>
<title>Dew Drinks | Worker Delivery Card</title>
<link rel="shortcut icon" href="dd/dist/images/ddicon.jpg" type="image/png"/>
<link href="../dd/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../dd/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
<link href="../dd/dist/css/sb-admin-2.css" rel="stylesheet" />
<link href="../dd/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script src="../dd/bower_components/JavaScript.js" type="text/javascript"></script>
        </head>
<body>
    <form id="form1" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>

         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <br />
            <div class="container-fluid">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <div class="text-center text-uppercase ">
                           Pending Details By Customer
                        </div>
                    </div>
                    <div class="panel-body">
                         <div class="row">
                              <div class="col-lg-4 col-md-4">
                                <div class="form-group">
                                  <a href="DeliveryCard.aspx" class="btn  btn-block btn-info">Go to Delivery Card </a>
                                </div>
                            </div>
                             </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="table-responsive">
                                    <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="30" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Date" HeaderText="Date" />
                                           <asp:TemplateField HeaderText="Delivery DateTime">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                    <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="FullName" HeaderText="FullName" />
                                            <asp:BoundField DataField="MobileNo" HeaderText="MobileNo" />
                                            <asp:TemplateField HeaderText="Hint & Address">
                                                <ItemTemplate>
                                                    <b>
                                                        <asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></b>
                                                    <asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                           <asp:TemplateField HeaderText="PendingAmt">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="PendingCan">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Complete Order">
                                                <ItemTemplate>
                                                    <a href="#" onclick="popup =window.open('UpdatePendingDetails.aspx?Id=<%#Eval("OrderId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false" class="btn btn-info">Save</a>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-4 alert col-lg-offset-2 alert-success">
                                <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                            </div>

                            <div class="col-lg-4 col-lg-offset-1 alert alert-info">
                                <b>Total Pending Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    </form>
</body>
    <script src="../dd/bower_components/JavaScript.js"></script>
<script src="../dd/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../dd/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../dd/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../dd/dist/js/sb-admin-2.js"></script>
</html>
