﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DeliveryCard.aspx.cs" Inherits="worker_DeliveryCard" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
<meta charset="utf-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
<meta name="viewport" content="width=device-width, initial-scale=1"/>
<meta name="description" content=""/>
<meta name="author" content=""/>
<title>Dew Drinks | Worker Delivery Card</title>
<link rel="shortcut icon" href="dd/dist/images/ddicon.jpg" type="image/png"/>
<link href="../dd/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
<link href="../dd/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
<link href="../dd/dist/css/sb-admin-2.css" rel="stylesheet" />
<link href="../dd/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<script src="../dd/bower_components/JavaScript.js" type="text/javascript"></script>

<style>
    .address
    {
        font-size:9px;
        font-weight:bold;
    }
</style>


</head>
<body>



    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div class="container-fluid">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-center text-uppercase ">
                                Worker  Delivery Orders
                            </div>
                        </div>

                        <div class="panel-body">
                            <div class="row">



                                <div>  
   <div id="google_translate_element"></div>  
   <script type="text/javascript">
       function googleTranslateElementInit() {
           new google.translate.TranslateElement
           ({
               pageLanguage: 'en',
               layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL.SIMPLE
               //layout: google.translate.TranslateElement.InlineLayout.SIMPLE
           },
           'google_translate_element');
       }
</script><script type="text/javascript"   
src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit">  
</script>  
    </div> 



                                <div class="col-lg-2 col-md-2">
                                    <asp:LinkButton ID="btndefault" class="btn btn-info btn-block" runat="server" OnClick="btndefault_Click">Refresh</asp:LinkButton>
                                </div>
                                <div class="col-lg-2 col-md-2">
                                   <b> <asp:Label ID="lblWorkerName" runat="server"></asp:Label></b>
                                    <a href="CompleteOrder.aspx">Complate order</a>
                                </div>
                                <div class="col-lg-2 col-md-2 col-md-offset-6  col-lg-offset-6">
                                    <a href="../logout.aspx" class="btn btn-primary">Log Out</a>
                                </div>
                            </div>
                            <br />
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="table-responsive">
                                        <asp:GridView ID="GridView1" EmptyDataText="No records Found" runat="server" AllowPaging="true" PageSize="50" class="table table-striped table-bordered table-hover" AutoGenerateColumns="False" OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" OnPageIndexChanging="GridView1_PageIndexChanging">
                                            <Columns>
                 
                                                <asp:TemplateField HeaderText="OrderId" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblOrderId" runat="server" Text=' <%#Eval("OrderId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sr.No" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblcustomerid" runat="server" Text=' <%#Eval("CustomerId")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>

<asp:TemplateField HeaderText="Address">
<ItemTemplate>
<div class="address">
<%# Container.DataItemIndex + 1 %>)
<asp:Label ID="lblHint" runat="server" Text=' <%#Eval("Hint")%>'></asp:Label></br>
<asp:Label ID="lblAddress" runat="server" Text=' <%#Eval("Address")%>'></asp:Label>
<asp:Label ID="lblqty" runat="server" Text=' <%#Eval("qty")%>'></asp:Label></b>
<asp:Label ID="lblItemNames" runat="server" Text=' <%#Eval("ItemNames")%>'></asp:Label>
<asp:Label ID="lblnote" runat="server" Text=' <%#Eval("note")%>'></asp:Label></br>
Time Ago &nbsp:<asp:Label ID="lbltimeago" runat="server" Text=' <%#Eval("Time")%>'></asp:Label></br>
</div>
</ItemTemplate>
</asp:TemplateField>
                                           

<asp:TemplateField HeaderText="Details">
<ItemTemplate>
<div class="address">
 <a href="tel:15555551212">555-555-1212</a>


<asp:Label ID="Label1" runat="server" Text=' <%#Eval("MobileNo")%>'></asp:Label></br>
Total Amount:<asp:Label ID="lblTotalAmount" runat="server" Text=' <%#Eval("TotalAmount")%>'></asp:Label></br>
Pending Amt:<asp:Label ID="lblPendingAmt" runat="server" Text=' <%#Eval("PendingAmt")%>'></asp:Label></br>
Pending bottle: <asp:Label ID="lblPendingCan" runat="server" Text=' <%#Eval("CollectedCan")%>'></asp:Label>
Transaction Id: <asp:Label ID="lblTransactionId" runat="server" Text=' <%#Eval("TransactionId")%>'></asp:Label>
</div>
</ItemTemplate>
</asp:TemplateField>



                                                <asp:TemplateField HeaderText="Delivery DateTime" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblDeDate" runat="server" Text=' <%#Eval("DeDate")%>'></asp:Label>
                                                        <asp:Label ID="lbltime" runat="server" Text=' <%#Eval("Time")%>'></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <div class="dropdown">
                                                        <a href="#" class="btn btn-primary" onclick="popup =window.open('OrderComplete.aspx?Id=<%#Eval("OrderId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false">Ok</a>

                                                             <button class="btn btn-sm dropdown-toggle" type="button" data-toggle="dropdown">
                                                                Action
  <span class="caret"></span>
                                                            </button>
<%--<ul class="dropdown-menu">
<li><a href="#" onclick="popup =window.open('OrderComplete.aspx?Id=<%#Eval("OrderId") %>', 'PopupPage', 'resizable,scrollbars=yes,menubar=no,status=yes,toolbar=no,location=no,width=700, height=300, top=250, left=350'); return false">Ok</a></li>
<li><a href="PendingDetailsByCustomer.aspx?Id=<%#Eval("CustomerId") %>">Pending Details</a></li>
<li>
<asp:LinkButton ID="lnkordercomp" CommandName="DeleteRow" runat="server" OnClientClick="return confirm('are you sure want to Cancel Order?');" CommandArgument='<%#Eval("OrderId") %>'>Cancel</asp:LinkButton>
</li>
</ul>--%>

<ul class="dropdown-menu">

<li><a href="PendingDetailsByCustomer.aspx?Id=<%#Eval("CustomerId") %>">Pending Details</a></li>
<li>
<%--<asp:LinkButton ID="lnkordercomp" CommandName="DeleteRow" runat="server" OnClientClick="return confirm('are you sure want to Cancel Order?');" CommandArgument='<%#Eval("OrderId") %>'>Cancel</asp:LinkButton>--%>

</li>
</ul>



                                                        </div>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-3 alert alert-success">
                                    <b>Total Can :
                                    <asp:Label ID="lbltotalcan" runat="server" Text="Label"></asp:Label></b>
                                </div>

                                <div class="col-lg-3 alert alert-info">
                                    <b>Total Amount:
                                    <asp:Label ID="lbltotalamt" runat="server" Text="Label"></asp:Label></b>
                                </div>
                                <div class="col-lg-3 alert alert-danger">
                                    <b>Total Pending Amount :
                                    <asp:Label ID="lbltotamt" runat="server" Text="Label"></asp:Label></b>
                                </div>

                                <div class="col-lg-3 alert alert-warning">
                                    <b>Total Pending Can :
                                    <asp:Label ID="lbltotcan" runat="server" Text="Label"></asp:Label></b>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
<script src="../dd/bower_components/JavaScript.js"></script>
<script src="../dd/bower_components/jquery/dist/jquery.min.js"></script>
<script src="../dd/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../dd/bower_components/metisMenu/dist/metisMenu.min.js"></script>
<script src="../dd/dist/js/sb-admin-2.js"></script>
</html>
