﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="OrderComplete.aspx.cs" Inherits="dd_OrderComplete" %>

<%@ Reference VirtualPath="~/dd/DeliveryCard.aspx" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <link href="../dd/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../dd/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet" />
    <link href="../dd/dist/css/sb-admin-2.css" rel="stylesheet" />
    <link href="../dd/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <script src="../dd/bower_components/JavaScript.js" type="text/javascript"></script>
    <script type="text/javascript">
        function refreshAndClose() {
            window.opener.location.reload(true);
             window.close();
        }
</script>
<style>
    .textboxsize
    {
  width: 100%;
  padding: 12px 20px;
  margin: 8px 0;
  box-sizing: border-box;
 border: 2px solid red;
  border-radius: 4px;
    }
</style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                <div class="container-fluid">
                    <div class="panel panel-info">
                        <div class="panel-heading">
                            <div class="text-center text-uppercase ">
                                Order Complete
                            </div>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                
                                  <div class="col-lg-4">
                                    <div class="form-group">
                                        Delivered Bottle :
                                        <asp:TextBox ID="txtDeliveredCan" onkeypress="return isNumber(event)"  placeholder="Enter Delivered Bottle" class="textboxsize" runat="server" required></asp:TextBox>
                                    </div>
                                </div>                              
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        Enter Collected Bottle :
                                        <asp:TextBox ID="txtpencan" onkeypress="return isNumber(event)"  placeholder="Enter Collected Bottle" class="textboxsize" runat="server" required></asp:TextBox>
                                    </div>
                                </div>


                                <div class="col-lg-4">
                                    <div class="form-group">
                                        Enter Collected Amount :
                                        <asp:TextBox ID="txtpenamt" onkeypress="return isNumber(event)"  placeholder="Enter Pending Amount" class="textboxsize" runat="server" required></asp:TextBox>
                                    </div>
                                </div>
                              
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        Enter Delivery Note :
                                        <asp:TextBox ID="txtdeliveryNote"  placeholder="Enter Delivery Note" class="textboxsize" runat="server"></asp:TextBox>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <br />
                                        <asp:Button ID="btnadd" runat="server" class="btn  btn-block btn-primary" Text="Confirm" OnClick="btnadd_Click" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
