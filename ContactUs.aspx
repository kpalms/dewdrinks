﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ContactUs.aspx.cs" Inherits="ContactUs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
     <script src="dd/bower_components/JavaScript.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="courseArchive">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 col-sm-12">
                    <div class="courseArchive_content">
                        <div class="row">
                            <div class="col-lg-12 col-12 col-sm-12">
                                <div class="single_blog_archive wow fadeInUp">
                                    <section id="contact">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12">
                                                    <div class="title_area">
                                                        <h2 class="title_two">Contact Us</h2>
                                                        <span></span>
                                                        <p>
                                                            Thank you for contacting us through our website. We will reply to your question or request as so on as possible.
                <br />
                                                            For urgent enquiries please call us on one of the telephone numbers below..
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-8 col-md-8 col-sm-8">
                                                    <div class="contact_form wow fadeInLeft">

                                                        <asp:TextBox ID="txtfullname" placeholder="Enter Full Name" class="wp-form-control wpcf7-text" runat="server" required></asp:TextBox>
                                                        <asp:TextBox ID="txtmobileno" placeholder="Enter Mobile No" onkeypress="return isNumber(event)" MaxLength="10" class="wp-form-control wpcf7-text" runat="server" required></asp:TextBox>
                                                        <asp:TextBox ID="txtemail" placeholder="Enter Email" TextMode="Email" class="wp-form-control wpcf7-text" runat="server"></asp:TextBox>

                                                        <asp:TextBox ID="txtdescripation" placeholder="What would you like to tell us" TextMode="MultiLine" class="wp-form-control wpcf7-textarea" runat="server" required></asp:TextBox>

                                                        <asp:Button ID="btnadd" runat="server" class="btn btn-primary" Text="Submit" OnClick="btnadd_Click" />
                                                    </div>
                                                </div>
                                                <div class="col-lg-4 col-md-4 col-sm-4">
                                                    <div class="contact_address wow fadeInRight">
                                                        <h3>Address</h3>
                                                        <div class="address_group">
                                                            <p>K-Palms(Dew Drinks)</p>
                                                            <p>Survey.No.76/89/449.</p>
                                                            <p>Navjivan Sociery,</p>
                                                            <p>Sahakar Nagar,</p>
                                                            <p>Parvati Pune-09</p>

                                                            <h3>Customer Supports</h3>
                                                            web:-www.dewdrinks.com
                    <br />
                                                            <p>email:-support@dewdrinks.com</p>
                                                            <p>dew2dewdrinks@gmail.com</p>
                                                            <p>call <a href="tel:7735353530">77 35 35 35 30</a></p>
                                                        </div>
                                                        <div class="address_group">
                                                            <ul class="footer_social">
                                                                <li><a href="www.facebook.com/DewDrinksMineralWater" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Facebook"><i class="fa fa-facebook"></i></a></li>
                                                                <!--                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Twitter"><i class="fa fa-twitter"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Google+"><i class="fa fa-google-plus"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Linkedin"><i class="fa fa-linkedin"></i></a></li>
                <li><a href="#" class="soc_tooltip" title="" data-placement="top" data-toggle="tooltip" data-original-title="Youtube"><i class="fa fa-youtube"></i></a></li>-->
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </section>
                                    <section id="googleMap">
                                        <iframe src="https://www.google.com/maps/d/embed?mid=1FbPYb0akktB6p95aVl4ObXp4gqo&hl=en" width="100%" height="480"></iframe>
                                    </section>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

