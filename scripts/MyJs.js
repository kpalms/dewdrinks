﻿                                                                                                                                                                      

var ConnectionString = "http://localhost:2883/Location/WebService.asmx";
//var ConnectionString = "http://dewdrinks.com/WebService.asmx";


function AddUserInformation(fn, ln, mb, email, pass) {

    var otp = Math.floor((Math.random() * 1000000) + 1);

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/AddUserInformation",
        data: '{"firstname":"' + fn + '","lastname":"' + ln + '","mbo":"' + mb + '","email":"' + email + '","pass":"' + pass + '","otp":"' + otp + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;

            //alert("Register successfully");
            localStorage.setItem("CustomerId", result);
            localStorage.setItem("MobileNo", mb);
            localStorage.setItem("Email", email);
            localStorage.setItem("Name", fn + " " + ln);
            localStorage.setItem("Password", pass);

            SendSMS("your OTP is  " + otp, mb);
            window.location.href = 'OTP.html';
        
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });



}


function GetCityList(mode, cityid) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/GetCityList",
        data: '{"mode":' + mode + ',"cityid":' + cityid + '}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            $("#DataCity").empty();
            var subtotal = 0
            $("#DataCity").append('<th align="left">City Name</th><th align="left">Action</th>');
            for (var i = 0; i < result.length; i++) {
                $("#DataCity").append('<tr><td>' + result[i].CityName + '</td><td> <img src="dewdrinks.com/img/edit.png" width=30px height=30px onclick=updatedata(' + result[i].CityId + ',"' + result[i].CityName + '") >  <img src="dewdrinks.com/img/delete.png" width=30px height=30px onclick=deleterecords(' + result[i].CityId + ')></td></tr>');
            }
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}

function SelectCityDropDown() {
    // alert("drop down");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/GetCityList",
        data: '{"mode":"Select","cityid":"0"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            var subtotal = 0
            for (var i = 0; i < result.length; i++) {

                $("#cbocity").append('<option value=' + result[i].CityName + ' id=' + result[i].CityId + '>' + result[i].CityName + '</option>');

            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}


function UserLogIn(mobile, pass) {
  // alert("dd");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/UserLogIn",
        data: '{"mobile":"' + mobile + '","pass":"' + pass + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;

            if (result != "") {

           
               // alert(result[0].Verify);
                if (result[0].Verify == 0) {
                    alert("Mobile number not verify");

                } else {
                    localStorage.setItem("CustomerId", result[0].UserId);
                    localStorage.setItem("MobileNo", result[0].MobileNo);
                    localStorage.setItem("Email", result[0].Email);
                    localStorage.setItem("Name", result[0].FirstName + " " + result[0].LastName);
                    localStorage.setItem("Password", pass);

                    window.location.href = 'Home.html';
                }

            } else {
                alert("Invalid credentials");
            }
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}


function SelectDeliveryAddress() {
    var userid = localStorage.getItem("CustomerId");

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectDeliveryAddress",
        data: '{"userid":"' + userid + '"}',
        dataType: "json",
        success: function (data) {

            var result = data.d;
            $("#DataDeliveryAddress").empty();
            if (result != '') {
                var table = "<div >";

                for (var i = 0; i < result.length; i++) {
                    table += '<div style="padding:5px" ><div class="e" style="background-color:#e5edf9;padding:2px;width:70%;float:left;height:50px">' + result[i].Company + ' ' + result[i].FlatNo + ' ' + result[i].Appartment + ' ' + result[i].AreaName + ' ' + result[i].Address + '</div> <div  style="background-color:#e5edf9;padding:2px;width:15%;float:left;height:50px"><button class="btn btn-primary btn-xs" id=' + result[i].DeliveryId + ' onclick="ModelPops(' + result[i].DeliveryId + ',\'' + result[i].Company + '\',\'' + result[i].FlatNo + '\',\'' + result[i].Appartment + '\',\'' + result[i].Address + '\',\'' + result[i].AreaName + '\')">Edit</button></div> <div style="background-color:#e5edf9;padding:2px;width:15%;float:left;height:50px"><button class="btn btn-danger btn-xs" onclick=DeleteDeliveryAdd(' + result[i].DeliveryId + ')>Delete</button> </div> </div><div >__</div>'

                }

                table += "</div>";
                $("#DataDeliveryAddress").append(table);
            } else {
                alert("Please Add delivery address");
                window.location.href = "AddAddress.html";
            }
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}







function SelectDeliveryAddressDropDown() {
  //  alert("drop down");
    var userid = localStorage.getItem("CustomerId");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectDeliveryAddress",
        data: '{"userid":"' + userid + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            var subtotal = 0
            if (result.length > 0) {
                for (var i = 0; i < result.length; i++) {


                    $("#cboaddress").append('<option value=' + result[i].Address + ' id=' + result[i].DeliveryId + '>' + result[i].Address + ' ' + result[i].Company + ' ' + result[i].FlatNo + ' ' + result[i].Appartment + '</option>');

                }
            } else {
                
                alert("Address not found please add new address");
                location.href = "AddAddress.html";
            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}

function SelectPaymentMode() {
     // alert("drop down");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectPaymentMode",
        data: '{"mode":"Select","paymentmodeid":"0"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            var subtotal = 0
            for (var i = 0; i < result.length; i++) {

                // $("#cboaddress").append("<option>" + result[i].DeliveryId + "</option>");
                $("#cbopaymentmode").append('<option value=' + result[i].PaymentModes + ' id=' + result[i].PayModeId + '>' + result[i].PaymentModes + '</option>');

            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}

function SelectItemName() {
   //  alert("drop down");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectItemNames",
        data: '{"mode":"Select","itemid":"0"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            var subtotal = 0
            for (var i = 0; i < result.length; i++) {

                // $("#cboaddress").append("<option>" + result[i].DeliveryId + "</option>");
                $("#cbojar").append('<option value=' + result[i].ItemNames + ' id=' + result[i].ItemId + '>' + result[i].ItemNames + '</option>');

            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}






function DeleteDeliveryAdd(deliveryid) {
   
    var r = confirm("Are Your Sure You Want to Delete This Record !");
    if (r == true) {
        var userid = localStorage.getItem("CustomerId");
        AddDeliveryAddress('', '', '', '', 'Delete', deliveryid,'','')

    }
}


function AddDeliveryAddress(company, flatno, apparment, add, mode, deliveryid,cityid, cboareaid) {
    //alert(cboareaid);
    var userid = localStorage.getItem("CustomerId");

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/AddDeliveryAddress",
        data: '{"userid":"' + userid + '","company":"' + company + '","flatno":"' + flatno + '","apparment":"' + apparment + '","add":"' + add + '","mode":"' + mode + '","deliveryid":"' + deliveryid + '","cityid":"' + cityid + '","cboareaid":"' + cboareaid + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            if (mode == "Insert") {
                alert("Address added successfully !");
            }
            if (mode == "Update") {
                alert("Address is Updated !");
            }
            if (mode == "Delete") {
                alert("Address is Delete !");
            }
            window.location.href = "Home.html";
            //SelectDeliveryAddress();
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert("errorMessage");
        }
    });

}

function UpdateDeliveryAdd(deliveryid, userid, Company, FlatNo, Appartment, Address) {

    document.getElementById('txtbuildingname').value = FlatNo;
    document.getElementById('txapartment').value = Appartment;
    document.getElementById('txtaddress').value = Address;
    document.getElementById('txtcompany').value = Company;

}

//++++++++++++++++++++++++++++++++++++++city start+++++++++++++++++++++++++++++++++++++++++++

function GetCityList(mode,cityid) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/GetCityList",
        data: '{"mode":' + mode + ',"cityid":' + cityid + '}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            $("#DataCity").empty();
            var subtotal = 0
            $("#DataCity").append('<th align="left">City Name</th><th align="left">Action</th>');
            for (var i = 0; i < result.length; i++) {
                $("#DataCity").append('<tr><td>' + result[i].CityName + '</td><td> <img src="dewdrinks.com/img/edit.png" width=30px height=30px onclick=updatedata(' + result[i].CityId + ',"' + result[i].CityName + '") >  <img src="dewdrinks.com/img/delete.png" width=30px height=30px onclick=deleterecords(' + result[i].CityId + ')></td></tr>');
            }
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });


}










function SelectUserDetails() {

    var customerid = localStorage.getItem("CustomerId");

    $.ajax({
        type: "POST",
        url: ConnectionString + '/GetUserDetailsList',
        data: '{"userid":' + customerid + '}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data)
        {
            var result = data.d;
            document.getElementById('txtfirstname').value = result[0].FirstName;
            document.getElementById('txtlastname').value = result[0].LastName;

            if (result[0].Gender=="Male")
            {
                document.getElementById('gmale').checked = true;

            }
            if (result[0].Gender == "Female") 
            {
                document.getElementById('gfemale').checked = true;
            }
            document.getElementById('txtmobileno').value = result[0].MobileNo;
            document.getElementById('txtemail').value = result[0].Email;

            //localStorage.setItem("CustomerId", result);
            localStorage.setItem("MobileNo", result[0].MobileNo);
            localStorage.setItem("email", result[0].Email);
            localStorage.setItem("Name", result[0].FirstName + " " + result[0].LastName);

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert("errorMessage");
        }
    });
}





function updateUserInformation(fn,ln,g,mb,email,add) {
    var customerid = localStorage.getItem("CustomerId");

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/UpdateUserInformation",
        data: '{"firstname":"' + fn + '","lastname":"' + ln + '","userid":"' + customerid + '","mbo":"' + mb + '","email":"' + email + '","gender":"' + g + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            alert("Profile Updated");
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert("errorMessage");
        }
    });
}




function CheckUserPassword(newpass, oldpass)
{
    var customerid = localStorage.getItem("CustomerId");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/CheckUserPassword",
        data: '{"userid":"' + customerid + '"}',
        dataType: "json",
        success: function (data) {

            var result = data.d;
           // alert(result);
            if (result[0].Password == oldpass) {
                ChangePassword(newpass, customerid);
                
            }
            else {
                alert("Old Password is Incorrect !");
            }
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert("errorMessage");
        }
    });

}


function ChangePassword(newpass, customerid) {
  //  var uid = 6;
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/ChangePassword",
        data: '{"userid":"' + customerid + '","newpass":"' + newpass + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
                alert("Password Change Successfully.");
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert("errorMessage");
        }
    });

}




function AddSuggestion(fullname, mobileno, commetns) {
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/AddSuggestion",
        data: '{"fullname":"' + fullname + '","mobileno":"' + mobileno + '","commetns":"' + commetns + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;

            SendEmailSmsSuggestion(fullname, mobileno, commetns);
            suggestionClear();
            alert("Thank you for your suggestion !");
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert("errorMessage");
        }
    });
}


function SendEmailSmsSuggestion(fullname, mobileno, commetns) {





    //user message
    if (mobileno != '') {
        var msg = "Thank you " + fullname + " Your Suggestion very important to us.";
        var mobilenos = mobileno;
        SendSMS(msg, mobilenos);
    }
    //end user message


    //my message
    var msgmy = "Suggestion:-name:-" + fullname + " Mobile:-" + mobileno + " commetns:-" + commetns + "";
    var mobilenosmy = "9096234202";
    SendSMS(msgmy, mobilenosmy);
    //my user message




}

function suggestionClear() {
    document.getElementById("txtfullname").value = "";
    document.getElementById("txtmobileno").value = "";
    document.getElementById("txtcomment").value = "";
}


function PlaceOrder() {

   // alert("adf");
var cbojar = document.getElementById("cbojar");
var cbojarid=cbojar.options[cbojar.selectedIndex].id;



var cboqty = document.getElementById("cboqty");
var cboqtyid = cboqty.options[cboqty.selectedIndex].id;



var cbopaymentmode = document.getElementById("cbopaymentmode");
var cbopaymentmodeid = cbopaymentmode.options[cbopaymentmode.selectedIndex].id;



var cboaddress = document.getElementById("cboaddress");
var cboaddressid = cboaddress.options[cboaddress.selectedIndex].id;

var customerid = localStorage.getItem("CustomerId");
var MobileNo = localStorage.getItem("MobileNo");

var Latitude = localStorage.getItem("Latitude");
var Longitude = localStorage.getItem("Longitude");



$.ajax({
    type: "POST",
    contentType: "application/json; charset=utf-8",
    url: ConnectionString + "/PlaceOrder",
    data: '{"customerid":"' + customerid + '","paymentmodeid":"' + cbopaymentmodeid + '","latitude":"' + Latitude + '","longitude":"' + Longitude + '","itemid":"' + cbojarid + '","qty":"' + cboqtyid + '","rate":"50","deliveryid":"' + cboaddressid + '"}',
    dataType: "json",
    success: function (data) {
        var result = data.d;
        SendSmsEmailPlaceOrder();
        alert("Order Place Successfully we will Contact you.!");
        location.href = "ThankYou.html";
    },
    error: function (data) {
        var r = data.responseText;
        var errorMessage = r.Message;
        alert("errorMessage");
    }
});

}

function SendSmsEmailPlaceOrder() {
    var MobileNo = localStorage.getItem("MobileNo");
    var email = localStorage.getItem("Email");
    var Name = localStorage.getItem("Name");
    var cboaddress = document.getElementById("cboaddress");
    var cboaddresstext = cboaddress.options[cboaddress.selectedIndex].text;

    //user email
   
    if (email != "") {
        var subject = "Order Confirm";
        var body = "<style>.btn {	text-decoration:none;color: #FFF;background-color: #666;padding:10px 16px;font-weight:bold;margin-right:10px;text-align:center;cursor:pointer;display: inline-block;}p.callout {padding:15px;background-color:#ECF8FF;margin-bottom: 15px;}.callout a {font-weight:bold;color: #2BA6CB;}table.social {background-color: #ebebeb;}.social .soc-btn {padding: 3px 7px;font-size:12px;margin-bottom:10px;text-decoration:none;color: #FFF;font-weight:bold;display:block;text-align:center;}a.fb { background-color: #3B5998!important; }a.tw { background-color: #1daced!important; }a.gp { background-color: #DB4A39!important; }a.ms { background-color: #000!important; }.sidebar.soc-btn { display:block;width:100%;}table.head-wrap { width: 100%;}.header.container table td.logo { padding: 15px; }.header.container tabletd.label { padding: 15px; padding-left:0px;}table.body-wrap { width: 100%;}table.footer-wrap { width: 100%;	clear:both!important;}.footer-wrap .container td.content p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}.footer-wrap .container td.contentp {font-size:10px;font-weight: bold;}h1,h2,h3,h4,h5,h6{font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;}h1 small, h2 small, h3 small, h4 small, h5 small,h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }h1 { font-weight:200; font-size: 44px;}h2 { font-weight:200; font-size: 37px;}h3 { font-weight:500; font-size: 27px;}h4 { font-weight:500; font-size: 23px;}h5 { font-weight:900; font-size: 17px;}h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}.collapse { margin:0!important;}p, ul { margin-bottom: 10px; font-weight: normal; font-size:14px; line-height:1.6;}p.lead { font-size:17px; }p.last { margin-bottom:0px;}ul li {	margin-left:5px;list-style-position: inside;}ul.sidebar {background:#ebebeb;display:block;list-style-type: none;}ul.sidebar li { display: block; margin:0;}ul.sidebar li a {text-decoration:none;color: #666;padding:10px 16px;margin-right:10px;cursor:pointer;border-bottom: 1px solid #777777;border-top: 1px solid #FFFFFF;display:block;margin:0;}ul.sidebar li a.last { border-bottom-width:0px;}ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}.container {display:block!important;max-width:600px!important;margin:0 auto!important; clear:both!important;}.content {padding:15px;max-width:600px;margin:0 auto;display:block; }.content table { width: 100%; }.column {	width: 300px;float:left;}.column tr td { padding: 15px; }.column-wrap { padding:0!important; margin:0 auto; max-width:600px!important;}.column table{ width:100%;}.social .column {	width: 280px;min-width: 279px;float:left;}.clear { display: block; clear: both; }@media only screen and (max-width: 100%) {a[class='btn'] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}div[class='column'] { width: auto!important; float:none!important;}table.social div[class='column'] {width:auto!important;	}}</style><table class='head-wrap' bgcolor='#0187aa'><tr><td></td><td class='header container'><div class='content'><table bgcolor='#0187aa'><tr><td><a href='dewdrinks.com' target='_blank'><img src='dewdrinks.com/img/DewDrinksLogo.png' /></a></td><td align='right'><h6 class='collapse' style='color:White'>Dew Drinks</h6></td></tr></table></div></td><td></td></tr></table><table class='body-wrap'><tr><td></td><td class='container' bgcolor='#FFFFFF'><div class='content'><table><tr><td><h3>Thank You, " + Name + "</h3><p class='lead'>We appreciate the recent order for the Dew Drinks</p><p><img src='dewdrinks.com/img/banner.jpg' /></p><p>Thank you for placing an order with us.We really appreciate for choosing our product and assure that you will never any complaint. <br />Your order will be delivered as soon as posible.Our delivery team shall get in touch with you and reconfirm the delivery location.<br />It has been great doing business with you and we hope to continue the acquaintance.</p><p class='callout'>If you have any questions, please don't hesitate to contact us at 9595 16 35 94. Thank You for choosing Dew Drinks. </p><table class='social' width='100%'><tr><td><table align='left' class='column'><tr><td><h5 class=''>Connect with Us:</h5><p class=''><a href='www.facebook.com/DewDrinksMineralWater' class='soc-btn fb'>Facebook</a></p></td></tr></table><table align='left' class='column'><tr><td><h5 class=''>Contact Info:</h5><p>Phone: <strong>9595 1635 94 </strong><br/><p>  web: <strong><a href='dewdrinks.com' target='_blank'>www.dewdrinks.com</a></strong></p><p>  Email: <strong>support@dewdrinks.com</strong></p></td></tr></table><span class='clear'></span></td></tr></table></td></tr></table></div></td><td></td></tr></table><table class='footer-wrap'><tr><td></td><td class='container'><div class='content'><table><tr><td align='center'></td></tr></table></div></td><td></td></tr></table>";
        var useremail = email;
        SendEmailContactUs(subject, body, useremail);
    }
    //end user email

    //for me email
    var subjectmy = "Place Order";
    var bodymy = "<table><tr><td>Name</td><td>'" + Name + "'</td></tr><tr><td>Emails</td><td>'" + email + "'</td></tr><tr><td>Mobile No</td><td>'" + MobileNo + "'</td></tr><tr><td>Address</td><td>'" + cboaddresstext + "'</td></tr></table>";
    var emailmy = "support@dewdrinks.com";
    SendEmailContactUs(subjectmy, bodymy, emailmy);
    //end for me email


    //user message
    var msg = "Thank you " + Name + " for placing an order with us.We really appreciate for choosing our product and assure that you will never any complaint.";
    var mobilenos = MobileNo;
    SendSMS(msg, mobilenos);
    //end user message


    //my message
    var msgmy = "Place Order:-name:-" + Name + " Mobile:-" + MobileNo + " Address:-" + cboaddresstext + "";
    var mobilenosmy = "9096234202";
    SendSMS(msgmy, mobilenosmy);
    //my user message
}


function SelectMyOrder() {
    var userid = localStorage.getItem("CustomerId");

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectMyOrder",
        data: '{"userid":"' + userid + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            $("#MyOrder").empty();
            var subtotal = 0
            debugger
            var table = "<table border=1 id='newspaper-a'><tr><td>Date</td><td>Qty</td><td>ItemName</td><td>Status</td><td>Action</td></tr>";
            for (var i = 0; i < result.length; i++) {
               //alert(result[i].Status);
                if ((result[i].Status).trim() == "Cancel") {
                    table += '<tr> <td>' + result[i].Date + '</td><td>' + result[i].Qty + '</td><td>' + result[i].ItemNames + '</td><td>' + result[i].Status + '</td><td></td></tr>';
                } else if ((result[i].Status).trim() == "Delivered")
                {
                    table += '<tr> <td>' + result[i].Date + '</td><td>' + result[i].Qty + '</td><td>' + result[i].ItemNames + '</td><td>' + result[i].Status + '</td><td></td></tr>';
                } 
                else {
                    table += '<tr> <td>' + result[i].Date + '</td><td>' + result[i].Qty + '</td><td>' + result[i].ItemNames + '</td><td>' + result[i].Status + '</td><td><button class="btn btn-danger btn-xs" onclick=CancelOrder(' + result[i].OrderId + ')>Cancel</button></td></tr>';
                }
            }
            table += "</table>";
            $("#MyOrder").append(table);

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}


function CancelOrder(orderid) {
    var MobileNo = localStorage.getItem("MobileNo");

    var r = confirm("Are Your Sure You Want to Cancel This Order !");
    if (r == true) {

        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: ConnectionString + "/CancelOrder",
            data: '{"orderid":"' + orderid + '"}',
            dataType: "json",
            success: function (data) {
                var result = data.d;
                
                SendSmsEmailCancelOrder();
                SelectMyOrder();
                alert("Cancel Order Successfully");

            },
            error: function (data) {
                var r = data.responseText;
                var errorMessage = r.Message;
                alert("errorMessage");
            }
        });

    }
}


function SendSmsEmailCancelOrder() {
    var MobileNo = localStorage.getItem("MobileNo");
    var email = localStorage.getItem("Email");
    var Name = localStorage.getItem("Name");
    var cboaddress = document.getElementById("cboaddress");
    var cboaddresstext = cboaddress.options[cboaddress.selectedIndex].text;

    //user email

    if (email != "") {
        var subject = "Cancel Order";
        var body = "<style>.btn {	text-decoration:none;color: #FFF;background-color: #666;padding:10px 16px;font-weight:bold;margin-right:10px;text-align:center;cursor:pointer;display: inline-block;}p.callout {padding:15px;background-color:#ECF8FF;margin-bottom: 15px;}.callout a {font-weight:bold;color: #2BA6CB;}table.social {background-color: #ebebeb;}.social .soc-btn {padding: 3px 7px;font-size:12px;margin-bottom:10px;text-decoration:none;color: #FFF;font-weight:bold;display:block;text-align:center;}a.fb { background-color: #3B5998!important; }a.tw { background-color: #1daced!important; }a.gp { background-color: #DB4A39!important; }a.ms { background-color: #000!important; }.sidebar.soc-btn { display:block;width:100%;}table.head-wrap { width: 100%;}.header.container table td.logo { padding: 15px; }.header.container tabletd.label { padding: 15px; padding-left:0px;}table.body-wrap { width: 100%;}table.footer-wrap { width: 100%;	clear:both!important;}.footer-wrap .container td.content p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}.footer-wrap .container td.contentp {font-size:10px;font-weight: bold;}h1,h2,h3,h4,h5,h6{font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;}h1 small, h2 small, h3 small, h4 small, h5 small,h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }h1 { font-weight:200; font-size: 44px;}h2 { font-weight:200; font-size: 37px;}h3 { font-weight:500; font-size: 27px;}h4 { font-weight:500; font-size: 23px;}h5 { font-weight:900; font-size: 17px;}h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}.collapse { margin:0!important;}p, ul { margin-bottom: 10px; font-weight: normal; font-size:14px; line-height:1.6;}p.lead { font-size:17px; }p.last { margin-bottom:0px;}ul li {	margin-left:5px;list-style-position: inside;}ul.sidebar {background:#ebebeb;display:block;list-style-type: none;}ul.sidebar li { display: block; margin:0;}ul.sidebar li a {text-decoration:none;color: #666;padding:10px 16px;margin-right:10px;cursor:pointer;border-bottom: 1px solid #777777;border-top: 1px solid #FFFFFF;display:block;margin:0;}ul.sidebar li a.last { border-bottom-width:0px;}ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}.container {display:block!important;max-width:600px!important;margin:0 auto!important; clear:both!important;}.content {padding:15px;max-width:600px;margin:0 auto;display:block; }.content table { width: 100%; }.column {	width: 300px;float:left;}.column tr td { padding: 15px; }.column-wrap { padding:0!important; margin:0 auto; max-width:600px!important;}.column table{ width:100%;}.social .column {	width: 280px;min-width: 279px;float:left;}.clear { display: block; clear: both; }@media only screen and (max-width: 100%) {a[class='btn'] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}div[class='column'] { width: auto!important; float:none!important;}table.social div[class='column'] {width:auto!important;	}}</style><table class='head-wrap' bgcolor='#0187aa'><tr><td></td><td class='header container'><div class='content'><table bgcolor='#0187aa'><tr><td><a href='dewdrinks.com' target='_blank'><img src='dewdrinks.com/img/DewDrinksLogo.png' /></a></td><td align='right'><h6 class='collapse' style='color:White'>Dew Drinks</h6></td></tr></table></div></td><td></td></tr></table><table class='body-wrap'><tr><td></td><td class='container' bgcolor='#FFFFFF'><div class='content'><table><tr><td><h3>Hi " + Name + "</h3><p><img src='dewdrinks.com/img/banner.jpg' /></p><h3>Cancel Order-Confirmation</h3><p>Thank you for your interest! If you have experienced any difficulties placing your order, or have any questions, feel free to contact us directly at 9595 1635 94.We welcome your comments and opinions so that we may be able to improve our level of customer service.</p><p class='callout'>If you have any questions, please don't hesitate to contact us at 9595 16 35 94.Thank You for choosing Dew Drinks. </p><table class='social' width='100%'><tr><td><table align='left' class='column'><tr><td><h5 class=''>Connect with Us:</h5><p class=''><a href='www.facebook.com/DewDrinksMineralWater' class='soc-btn fb'>Facebook</a></p></td></tr></table><table align='left' class='column'><tr><td><h5 class=''>Contact Info:</h5><p>Phone: <strong>9595 1635 94 </strong><br/><p>  web: <strong><a href='dewdrinks.com' target='_blank'>www.dewdrinks.com</a></strong></p><p>  Email: <strong>support@dewdrinks.com</strong></p></td></tr></table><span class='clear'></span></td></tr></table></td></tr></table></div></td><td></td></tr></table><table class='footer-wrap'><tr><td></td><td class='container'><div class='content'><table><tr><td align='center'></td></tr></table></div></td><td></td></tr></table>";
        var useremail = email;
        SendEmailContactUs(subject, body, useremail);
    }
    //end user email

    //for me email
    var subjectmy = "Cancel Order";
    var bodymy = "<table><tr><td>Name</td><td>'" + Name + "'</td></tr><tr><td>Emails</td><td>'" + email + "'</td></tr><tr><td>Mobile No</td><td>'" + MobileNo + "'</td></tr><tr><td>Address</td><td>'" + cboaddresstext + "'</td></tr></table>";
    var emailmy = "support@dewdrinks.com";
    SendEmailContactUs(subjectmy, bodymy, emailmy);
    //end for me email


    //user message
    var msg = "Cancel Order-Confirmation " + Name + " If you have experienced any difficulties placing your order, or have any questions, feel free to contact us directly at 9595 1635 94. We welcome your comments and opinions so that we may be able to improve our level of customer service.";
    var mobilenos = MobileNo;
    SendSMS(msg, mobilenos);
    //end user message


    //my message
    var msgmy = "Cancel Order:-name:-" + Name + " Mobile:-" + MobileNo + " Address:-" + cboaddresstext + "";
    var mobilenosmy = "9096234202";
    SendSMS(msgmy, mobilenosmy);
    //my user message
}



function LogOut() {
    localStorage.clear();
    window.location.href = "Index.html";
}


function SelectArea(cityid) {
    // alert("area");
    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectArea",
        data: '{"cityid":"'+cityid+'"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
            var subtotal = 0
            for (var i = 0; i < result.length; i++) {

                // $("#cboaddress").append("<option>" + result[i].DeliveryId + "</option>");
                $("#cboarea").append('<option value=' + result[i].AreaName + ' id=' + result[i].AreaId + '>' + result[i].AreaName + '</option>');

            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });
}




function ResendOTP() {

    var userid = localStorage.getItem("CustomerId");
    var MobileNo = localStorage.getItem("MobileNo");

    var otp = Math.floor((Math.random() * 1000000) + 1);
    

    

   
  

    $.ajax({


                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: ConnectionString + "/ResendOTP",
                data: '{"userid":"' + userid + '","otp":"' + otp + '"}',
               
                dataType: "json",
                success: function (data) {
                    var result = data.d;
                    SendSMS("your OTP is" + otp, MobileNo);
                    alert("OTP Send Successfully");
                },
                error: function (data) {
                    var r = data.responseText;
                    var errorMessage = r.Message;
                    alert(errorMessage);
                }


        });


}




function SelectOTP() {

    var userid = localStorage.getItem("CustomerId");
    var otppost = document.getElementById("txtotp").value;

    $.ajax({


        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SelectOTP",
        data: '{"userid":"' + userid + '"}',

        dataType: "json",
        success: function (data) {
            var result = data.d;

            //alert(result);
            if (result == otppost) {
                VerifyOTP();
            } else {
                alert("OTP Does not match");
            }


        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }


    });


}



function VerifyOTP() {

    var MobileNo = localStorage.getItem("MobileNo");
    var userid = localStorage.getItem("CustomerId");

    $.ajax({


        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/VerifyOTP",
        data: '{"userid":"' + userid + '"}',

        dataType: "json",
        success: function (data) {
            var result = data.d;
//            alert("Congratulation");
//            document.getElementById("txtotp").value = "";
//            SendSMS("Welcome to Dew Drinks", MobileNo);
//            document.location.href = "Home.html";
              SendSmsEmailRegisterSuccess();
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }


    });


}

function SendSmsEmailRegisterSuccess()
{

//    SendSMS("Welcome to Dew Drinks", MobileNo);

//    alert("Congratulation");
//    document.getElementById("txtotp").value = "";
//   
  


    var MobileNo = localStorage.getItem("MobileNo");
    var email = localStorage.getItem("Email");
    var Name = localStorage.getItem("Name");
    var Password = localStorage.getItem("Password");

            //user email
            if (email != "") {
                var subject = "Welcome To Dew Drinks";
                var body = "<style>.btn {	text-decoration:none;color: #FFF;background-color: #666;padding:10px 16px;font-weight:bold;margin-right:10px;text-align:center;cursor:pointer;display: inline-block;}p.callout {padding:15px;background-color:#ECF8FF;margin-bottom: 15px;}.callout a {font-weight:bold;color: #2BA6CB;}table.social {background-color: #ebebeb;}.social .soc-btn {padding: 3px 7px;font-size:12px;margin-bottom:10px;text-decoration:none;color: #FFF;font-weight:bold;display:block;text-align:center;}a.fb { background-color: #3B5998!important; }a.tw { background-color: #1daced!important; }a.gp { background-color: #DB4A39!important; }a.ms { background-color: #000!important; }.sidebar.soc-btn { display:block;width:100%;}table.head-wrap { width: 100%;}.header.container table td.logo { padding: 15px; }.header.container tabletd.label { padding: 15px; padding-left:0px;}table.body-wrap { width: 100%;}table.footer-wrap { width: 100%;	clear:both!important;}.footer-wrap .container td.content p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}.footer-wrap .container td.contentp {font-size:10px;font-weight: bold;}h1,h2,h3,h4,h5,h6{font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;}h1 small, h2 small, h3 small, h4 small, h5 small,h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }h1 { font-weight:200; font-size: 44px;}h2 { font-weight:200; font-size: 37px;}h3 { font-weight:500; font-size: 27px;}h4 { font-weight:500; font-size: 23px;}h5 { font-weight:900; font-size: 17px;}h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}.collapse { margin:0!important;}p, ul { margin-bottom: 10px; font-weight: normal; font-size:14px; line-height:1.6;}p.lead { font-size:17px; }p.last { margin-bottom:0px;}ul li {	margin-left:5px;list-style-position: inside;}ul.sidebar {background:#ebebeb;display:block;list-style-type: none;}ul.sidebar li { display: block; margin:0;}ul.sidebar li a {text-decoration:none;color: #666;padding:10px 16px;margin-right:10px;cursor:pointer;border-bottom: 1px solid #777777;border-top: 1px solid #FFFFFF;display:block;margin:0;}ul.sidebar li a.last { border-bottom-width:0px;}ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}.container {display:block!important;max-width:600px!important;margin:0 auto!important; clear:both!important;}.content {padding:15px;max-width:600px;margin:0 auto;display:block; }.content table { width: 100%; }.column {	width: 300px;float:left;}.column tr td { padding: 15px; }.column-wrap { padding:0!important; margin:0 auto; max-width:600px!important;}.column table{ width:100%;}.social .column {	width: 280px;min-width: 279px;float:left;}.clear { display: block; clear: both; }@media only screen and (max-width: 100%) {a[class='btn'] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}div[class='column'] { width: auto!important; float:none!important;}table.social div[class='column'] {width:auto!important;	}}</style> <table class='head-wrap' bgcolor='#0187aa'><tr><td></td><td class='header container'><div class='content'><table bgcolor='#0187aa'><tr><td><a href='dewdrinks.com' target='_blank'><img src='dewdrinks.com/img/DewDrinksLogo.png' /></a></td><td align='right'><h6 class='collapse' style='color:White'>Dew Drinks</h6></td></tr></table></div></td><td></td></tr></table><table class='body-wrap'><tr><td></td><td class='container' bgcolor='#FFFFFF'><div class='content'><table><tr><td><h3>Welcome, " + Name + "</h3><p class='lead'>Thank you for registering for the Dew Drinks</p><p><img src='dewdrinks.com/img/banner.jpg' /></p><h3>Login Details</h3><p>Your account has been created and can be accessed via www.dewdrinks.com using the below mentioned login credentials. </p><h4>Mobile No:-" + MobileNo + "</h4><br /><h4>Password:-" + Password + "</h4><br/><br/><p class='callout'>If you have any questions, please don't hesitate to contact us at 9595 16 35 94.Thank You for choosing Dew Drinks.</p><table class='social' width='100%'><tr><td><table align='left' class='column'><tr><td><h5 class=''>Connect with Us:</h5><p class=''><a href='www.facebook.com/DewDrinksMineralWater' class='soc-btn fb'>Facebook</a></p></td></tr></table><table align='left' class='column'><tr><td><h5>Contact Info:</h5><p>Phone: <strong>9595 1635 94 </strong><br/><p>  web: <strong><a href='dewdrinks.com' target='_blank'>www.dewdrinks.com</a></strong></p><p>  Email: <strong>support@dewdrinks.com</strong></p></td></tr></table><span class='clear'></span></td></tr></table></td></tr></table></div></td><td></td></tr></table><table class='footer-wrap'><tr><td></td><td class='container'><div class='content'><table><tr><td align='center'></td></tr></table></div></td><td></td></tr></table>";
                var useremail = email;
                SendEmailContactUs(subject, body, useremail);
            }
           //end user email

            //for me email
            var subjectmy = "new Registration";
            var bodymy = "<table><tr><td>Name</td><td>'" + Name + "'</td></tr><tr><td>Emails</td><td>'" + email + "'</td></tr><tr><td>Mobile No</td><td>'" + MobileNo + "'</td></tr><tr><td>Message</td></tr></table>";
            var emailmy = "support@dewdrinks.com";
            SendEmailContactUs(subjectmy, bodymy, emailmy);
            //end for me email


            //user message
            var msg = "Your account has been created and can be accessed via www.dewdrinks.com using the below mentioned login credentials.Mobile No:-" + MobileNo + " Password:-" + Password + "";
            var mobilenos =MobileNo;
            SendSMS(msg, mobilenos);
            //end user message


            //my message
            var msgmy = "new Registration Mobile No:-"+MobileNo+" Name:-"+Name+"";
            var mobilenosmy = "9096234202";
            SendSMS(msgmy, mobilenosmy);
            //my user message

            document.location.href = "Home.html";
            
}



function CheckEmailExist() {

    var email = document.getElementById("txtemail").value;
    $.ajax({

        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/CheckEmailExist",
        data: '{"email":"' + email + '"}',

        dataType: "json",
        success: function (data) {
            var result = data.d;
            if (result.length > 0) {
                alert("Email id already exit");

                document.getElementById("txtemail").value = "";
                //document.getElementById("txtemail").focus();
                document.getElementById("txtemail").style.borderColor = "red";
            } else {
                document.getElementById("txtemail").style.borderColor = "#0187aa";
            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });

}


function CheckEmailForgetPassword() {

    var email = document.getElementById("txtemail").value;
   // alert(email);


    $.ajax({

        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/CheckEmailExist",
        data: '{"email":"' + email + '"}',

        dataType: "json",
        success: function (data) {
            var result = data.d;
            if (result.length > 0) {

                if (email != "") {
                    var subject = "Forgot Password";
                    var body = "<style>.btn {	text-decoration:none;color: #FFF;background-color: #666;padding:10px 16px;font-weight:bold;margin-right:10px;text-align:center;cursor:pointer;display: inline-block;}p.callout {padding:15px;background-color:#ECF8FF;margin-bottom: 15px;}.callout a {font-weight:bold;color: #2BA6CB;}table.social {background-color: #ebebeb;}.social .soc-btn {padding: 3px 7px;font-size:12px;margin-bottom:10px;text-decoration:none;color: #FFF;font-weight:bold;display:block;text-align:center;}a.fb { background-color: #3B5998!important; }a.tw { background-color: #1daced!important; }a.gp { background-color: #DB4A39!important; }a.ms { background-color: #000!important; }.sidebar.soc-btn { display:block;width:100%;}table.head-wrap { width: 100%;}.header.container table td.logo { padding: 15px; }.header.container tabletd.label { padding: 15px; padding-left:0px;}table.body-wrap { width: 100%;}table.footer-wrap { width: 100%;	clear:both!important;}.footer-wrap .container td.content p { border-top: 1px solid rgb(215,215,215); padding-top:15px;}.footer-wrap .container td.contentp {font-size:10px;font-weight: bold;}h1,h2,h3,h4,h5,h6{font-family: 'HelveticaNeue-Light', 'Helvetica Neue Light', 'Helvetica Neue', Helvetica, Arial, 'Lucida Grande', sans-serif; line-height: 1.1; margin-bottom:15px; color:#000;}h1 small, h2 small, h3 small, h4 small, h5 small,h6 small { font-size: 60%; color: #6f6f6f; line-height: 0; text-transform: none; }h1 { font-weight:200; font-size: 44px;}h2 { font-weight:200; font-size: 37px;}h3 { font-weight:500; font-size: 27px;}h4 { font-weight:500; font-size: 23px;}h5 { font-weight:900; font-size: 17px;}h6 { font-weight:900; font-size: 14px; text-transform: uppercase; color:#444;}.collapse { margin:0!important;}p, ul { margin-bottom: 10px; font-weight: normal; font-size:14px; line-height:1.6;}p.lead { font-size:17px; }p.last { margin-bottom:0px;}ul li {	margin-left:5px;list-style-position: inside;}ul.sidebar {background:#ebebeb;display:block;list-style-type: none;}ul.sidebar li { display: block; margin:0;}ul.sidebar li a {text-decoration:none;color: #666;padding:10px 16px;margin-right:10px;cursor:pointer;border-bottom: 1px solid #777777;border-top: 1px solid #FFFFFF;display:block;margin:0;}ul.sidebar li a.last { border-bottom-width:0px;}ul.sidebar li a h1,ul.sidebar li a h2,ul.sidebar li a h3,ul.sidebar li a h4,ul.sidebar li a h5,ul.sidebar li a h6,ul.sidebar li a p { margin-bottom:0!important;}.container {display:block!important;max-width:600px!important;margin:0 auto!important; clear:both!important;}.content {padding:15px;max-width:600px;margin:0 auto;display:block; }.content table { width: 100%; }.column {	width: 300px;float:left;}.column tr td { padding: 15px; }.column-wrap { padding:0!important; margin:0 auto; max-width:600px!important;}.column table{ width:100%;}.social .column {	width: 280px;min-width: 279px;float:left;}.clear { display: block; clear: both; }@media only screen and (max-width: 100%) {a[class='btn'] { display:block!important; margin-bottom:10px!important; background-image:none!important; margin-right:0!important;}div[class='column'] { width: auto!important; float:none!important;}table.social div[class='column'] {width:auto!important;	}}</style><table class='head-wrap' bgcolor='#0187aa'><tr><td></td><td class='header container'><div class='content'><table bgcolor='#0187aa'><tr><td><a href='dewdrinks.com' target='_blank'><img src='dewdrinks.com/img/DewDrinksLogo.png' /></a></td><td align='right'><h6 class='collapse' style='color:White'>Dew Drinks</h6></td></tr></table></div></td><td></td></tr></table><table class='body-wrap'><tr><td></td><td class='container' bgcolor='#FFFFFF'><div class='content'><table><tr><td><h3>hello, Customer Name</h3><p><img src='dewdrinks.com/img/banner.jpg' /></p><p>Hello " + Name + ",We have received new password request for your account.</p><h3>Login Details</h3><h4>Mobile No:-</h4><br /><h4>Password:-</h4><br/><br/><p class='callout'>If you have any questions, please don't hesitate to contact us at 9595 16 35 94.Thank You for choosing Dew Drinks. </p><table class='social' width='100%'><tr><td><table align='left' class='column'><tr><td><h5 class=''>Connect with Us:</h5><p class=''><a href='www.facebook.com/DewDrinksMineralWater' class='soc-btn fb'>Facebook</a> </p></td></tr></table><table align='left' class='column'><tr><td><h5 class=''>Contact Info:</h5><p>Phone: <strong>9595 1635 94 </strong><br/><p>  web: <strong><a href='dewdrinks.com' target='_blank'>www.dewdrinks.com</a></strong></p><p>Email: <strong>support@dewdrinks.com</strong></p></td></tr></table><span class='clear'></span></td></tr></table></td></tr></table></div></td><td></td></tr></table><table class='footer-wrap'><tr><td></td><td class='container'><div class='content'><table><tr><td align='center'></td></tr></table></div></td><td></td></tr></table>";
                    var useremail = email;
                    SendEmailContactUs(subject, body, useremail);
                } else {
                    alert("Emailid not found.Update your email id");

                }



            } else {
                alert("email id does not exist");
                document.getElementById("txtemail").style.borderColor = "red";
            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });

}


function CheckMobileNoExist() {

    var mobileno = document.getElementById("txtmobileno").value;
    $.ajax({

        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/CheckMobileNoExist",
        data: '{"mobileno":"' + mobileno + '"}',

        dataType: "json",
        success: function (data) {
            var result = data.d;
            if (result.length > 0) {
                alert("MobileNo already exit");
                document.getElementById("txtmobileno").value = "";
                document.getElementById("txtmobileno").focus();
                document.getElementById("txtmobileno").style.borderColor = "red";
                
                
            } else {
                document.getElementById("txtmobileno").style.borderColor = "#0187aa";
            }

        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });

}



//function CheckMobileEmail() {

//    
//    var mobileno = document.getElementById("txtmobileno").value;
//    var email = document.getElementById("txtemail").value;
//    alert(mobileno);



//    $.ajax({

//        type: "POST",
//        contentType: "application/json; charset=utf-8",
//        url: ConnectionString + "/CheckMobileNoExist",
//        data: '{"mobileno":"' + mobileno + '","email":"' + email + '"}',

//        dataType: "json",
//        success: function (data) {
//            var result = data.d;


//        },
//        error: function (data) {
//            var r = data.responseText;
//            var errorMessage = r.Message;
//            alert(errorMessage);
//        }
//    });

//}



function SendEmailContactUs(subject, body, useremail) {

    $.ajax({

        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SendMail",
        data: '{"sub":"' + subject + '","body1":"' + body + '","to":"' + useremail + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;
      
           // alert("Thank you for contacting us. We will be in touch with you very soon.");
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });

}



function SendSMS(msg,mobileno) {
   //alert("js");

    $.ajax({
        type: "POST",
        contentType: "application/json; charset=utf-8",
        url: ConnectionString + "/SendSms",
        data: '{"sNumber":"' + mobileno + '","sMessage":"' + msg + '"}',
        dataType: "json",
        success: function (data) {
            var result = data.d;

            //alert("Thank you for contacting us. We will be in touch with you very soon.");
        },
        error: function (data) {
            var r = data.responseText;
            var errorMessage = r.Message;
            alert(errorMessage);
        }
    });



}































