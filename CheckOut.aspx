﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="CheckOut.aspx.cs" Inherits="CheckOut" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link href="demo/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <link href="demo/css/style.css" rel="stylesheet" type="text/css" media="all" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <section id="courseArchive">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 col-12 col-sm-12">
                    <div class="courseArchive_content">
                        <div class="row">
                            <div class="col-lg-12 col-12 col-sm-12">
                                <div class="single_blog_archive wow fadeInUp">
                                    <div style="color: Green; text-align: center">Our Products</div>
                                    <div class="wrap">
                                        <div class="main">
                                            <div class="content">
                                                <div class="section group">
                                                    <div class="left_content">
                                                        <div class="span_1_of_left">
                                                            <div class="grid images_3_of_2">
                                                                <div class="flexslider">
                                                                    <ul class="slides">
                                                                        <li data-thumb="img/dew_drinks.jpg">
                                                                            <div class="thumb-image">
                                                                                <img src="img/dew_drinks.jpg" data-imagezoom="true" class="img-responsive">
                                                                            </div>
                                                                        </li>

                                                                    </ul>
                                                                    <div class="clearfix"></div>
                                                                </div>
                                                            </div>
                                                            <div class="span1_of_1_des">
                                                                <div class="desc1">
                                                                    <h3>Dew Drinks Mineral Water, 20 Ltr </h3>
                                                                    <p>When you are drinking Mineral Water, you are assured of water in its purest form, because Dew Drinks uses reverse osmosis technology to produce clean and safe drinking water. </p>
                                                                    <h5>Rs.60 <a href="#">click for offer</a></h5>
                                                                    <div class="available">
                                                                        <div class="btn_form">
                                                                            <form>
                                                                                <a href="https://play.google.com/store/apps/details?id=com.dewdrinks.dewdrinks" target="_blank">
   <input type="button" value="Check Out" />
</a>
                                                                                                         
                                                                            </form>
                                                                        </div>

                                                                        <div class="clearfix"></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="clearfix"></div>
                                                        </div>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</asp:Content>

