﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VideoGallery.aspx.cs" Inherits="VideoGallery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section id="imgBanner"></section>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12">
                    <h3 class="title_two" style="color: #056ee3">Video Gallery</h3>
            </div>
        </div>

        <div class="row">
            <asp:Repeater ID="rptcourse" runat="server">
                <ItemTemplate>

                    <div class="col-sm-6 col-md-4">

                        <div class="panel panel-info">
                            <div class="panel-heading text-uppercase text-center">
                                <asp:Label ID="lbltitle" runat="server" Text='<%# Eval("Title") %>'></asp:Label>
                            </div>
                            <div class="panel-body text-center">
                                <iframe src="https://www.youtube.com/embed/<%# Eval("URL") %>" frameborder="0" allow="autoplay; encrypted-media" width="100%" allowfullscreen></iframe>
                            
                                <p>
                                    <cite>
                                        <asp:Label ID="lblDescrption" runat="server" Text='<%# Eval("Descrption") %>'></asp:Label>
                                </p>
                                </footer>
                                </cite></p>
                               
                            </div>
                        </div>


                    </div>

                </ItemTemplate>
            </asp:Repeater>
        </div>

    </div>
</asp:Content>

